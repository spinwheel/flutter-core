import 'dart:convert';

class Error {
  int? _code;
  String? _desc;

  int? get code => _code != null ? _code : null;

  String? get desc => _desc != null ? _desc : null;

  Error({int? code, String? desc}) {
    _code = code;
    _desc = desc;
  }

  Error.fromJson(dynamic json) {
    _code = json['status']['code'];
    _desc = json['status']['desc'];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['code'] = _code;
    map['desc'] = _desc;
    return map;
  }
}
