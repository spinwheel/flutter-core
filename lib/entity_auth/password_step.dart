import 'package:sw_core/entity_auth/security_details.dart';
import 'next_auth_step_params.dart';

class SecurityStep {
  SecurityStep({
    String? loanServicerId,
    bool? securityDetailsRequired,
    String? userId,
    List<SecurityDetails>? securityDetails,
    String? nextAuthStep,
    NextAuthStepParams? nextAuthStepParams,
  }) {
    _loanServicerId = loanServicerId;
    _securityDetailsRequired = securityDetailsRequired;
    _userId = userId;
    _securityDetails = securityDetails;
    _nextAuthStep = nextAuthStep;
    _nextAuthStepParams = nextAuthStepParams;
  }

  SecurityStep.fromJson(dynamic json) {
    _loanServicerId = json['loanServicerId'];
    _securityDetailsRequired = json['securityDetailsRequired'];
    _userId = json['userId'];
    if (json['securityDetails'] != null) {
      _securityDetails = [];
      json['securityDetails'].forEach((v) {
        _securityDetails?.add(SecurityDetails.fromJson(v));
      });
    }
    _nextAuthStep = json['nextAuthStep'];
    _nextAuthStepParams = json['nextAuthStepParams'] != null
        ? NextAuthStepParams.fromJson(json['nextAuthStepParams'])
        : null;
  }

  String? _loanServicerId;
  bool? _securityDetailsRequired;
  String? _userId;
  List<SecurityDetails>? _securityDetails;
  String? _nextAuthStep;
  NextAuthStepParams? _nextAuthStepParams;

  String? get loanServicerId => _loanServicerId;

  bool? get securityDetailsRequired => _securityDetailsRequired;

  String? get userId => _userId;

  List<SecurityDetails> get securityDetails => _securityDetails ?? [];

  String? get nextAuthStep => _nextAuthStep;

  NextAuthStepParams get nextAuthStepParams => _nextAuthStepParams ?? NextAuthStepParams();

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['loanServicerId'] = _loanServicerId;
    map['securityDetailsRequired'] = _securityDetailsRequired;
    map['userId'] = _userId;
    if (_securityDetails != null) {
      map['securityDetails'] = _securityDetails?.map((v) => v.toJson()).toList();
    }
    map['nextAuthStep'] = _nextAuthStep;
    if (_nextAuthStepParams != null) {
      map['nextAuthStepParams'] = _nextAuthStepParams?.toJson();
    }
    return map;
  }
}
