class TokenData {
  TokenData({
      String? token, 
      String? refreshToken,}){
    _token = token;
    _refreshToken = refreshToken;
}

  TokenData.fromJson(dynamic json) {
    _token = json['token'];
    _refreshToken = json['refreshToken'];
  }
  String? _token;
  String? _refreshToken;

  String get token => _token ?? '';
  String get refreshToken => _refreshToken ?? '';

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['token'] = _token;
    map['refreshToken'] = _refreshToken;
    return map;
  }

}