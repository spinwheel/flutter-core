class AuthStepParams {
  AuthStepParams({
    String? accountUserName,
  }) {
    _accountUserName = accountUserName;
  }

  AuthStepParams.fromJson(dynamic json) {
    _accountUserName = json['accountUserName'];
  }

  String? _accountUserName;

  String? get accountUserName => _accountUserName;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['accountUserName'] = _accountUserName;
    return map;
  }
}
