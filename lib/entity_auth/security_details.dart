class SecurityDetails {
  SecurityDetails({
    String? question,
    String? questionId,
    String? answer,
  }) {
    _question = question;
    _questionId = questionId;
    _answer = answer;
  }

  SecurityDetails.fromJson(dynamic json) {
    _question = json['question'];
    _questionId = json['questionId'];
    _answer = json['answer'];
  }

  String? _question;
  String? _questionId;
  String? _answer;

  String? get question => _question;

  String? get questionId => _questionId;

  String get answer => _answer ?? '';

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['question'] = _question;
    map['questionId'] = _questionId;
    map['answer'] = _answer;
    return map;
  }
}
