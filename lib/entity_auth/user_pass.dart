class UserPass {
  UserPass({
    String? firstName,
    String? lastName,
    String? emailID,
    String? loanServicerName,
    String? username,
    String? userId,
    String? password,
    String? pin,
    String? dob,
    String? accountNumber,
    String? ssn,
  }) {
    _firstName = firstName;
    _lastName = lastName;
    _emailID = emailID;
    _loanServicerName = loanServicerName;
    _username = username;
    _userId = userId;
    _password = password;
    _pin = pin;
    _dob = dob;
    _accountNumber = accountNumber;
    _ssn = ssn;
  }

  UserPass.fromJson(dynamic json) {
    _firstName = json['firstName'];
    _lastName = json['lastName'];
    _emailID = json['emailID'];
    _loanServicerName = json['loanServicerName'];
    _username = json['username'];
    _userId = json['userId'];
    _password = json['password'];
    _pin = json['pin'];
    _dob = json['dob'];
    _accountNumber = json['accountNumber'];
    _ssn = json['ssn'];
  }

  String? _firstName;
  String? _lastName;
  String? _emailID;
  String? _loanServicerName;
  String? _username;
  String? _userId;
  String? _password;
  String? _pin;
  String? _dob;
  String? _accountNumber;
  String? _ssn;

  String? get firstName => _firstName;

  String? get lastName => _lastName;

  String? get loanServicerName => _loanServicerName;

  String? get emailID => _emailID;

  String? get username => _username;

  String? get userId => _userId;

  String? get password => _password;

  String? get pin => _pin;

  String? get dob => _dob;

  String? get accountNumber => _accountNumber;

  String? get ssn => _ssn;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['firstName'] = _firstName;
    map['lastName'] = _lastName;
    map['emailID'] = _emailID;
    map['loanServicerName'] = _loanServicerName;
    map['username'] = _username;
    map['userId'] = _userId;
    map['password'] = _password;
    map['pin'] = _pin;
    map['dob'] = _dob;
    map['accountNumber'] = _accountNumber;
    map['ssn'] = _ssn;
    return map;
  }
}
