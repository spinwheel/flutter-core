class CommunicationDetails {
  CommunicationDetails({String? email}) : _email = email;

  String? _email;

  CommunicationDetails.fromJson(Map<String, dynamic> json) {
    _email = json['email'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> map = Map<String, dynamic>();
    map['email'] = _email;

    return map;
  }
}
