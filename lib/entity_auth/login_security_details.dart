import 'package:sw_core/entity_auth/auth_step_params.dart';

import 'security_details.dart';

class LoginSecurityDetails {
  LoginSecurityDetails({
    String? loanServicerName,
    String? userId,
    List<SecurityDetails>? securityDetails,
    String? authStep,
    AuthStepParams? authStepParams,
    int? time,
    String? liabilityType,
  }) {
    _loanServicerName = loanServicerName;
    _userId = userId;
    _securityDetails = securityDetails;
    _authStep = authStep;
    _authStepParams = authStepParams;
    _time = time;
    _liabilityType = liabilityType;
  }

  LoginSecurityDetails.fromJson(dynamic json) {
    _loanServicerName = json['loanServicerName'];
    _userId = json['userId'];
    _authStep = json['authStep'];
    if (json['authStepParams'] != null) {
      _authStepParams = AuthStepParams.fromJson(json['authStepParams']);
    }
    _time = json['time'];
    _liabilityType = json['liabilityType'];
    if (json['securityDetails'] != null) {
      _securityDetails = [];
      json['securityDetails'].forEach((v) {
        _securityDetails?.add(SecurityDetails.fromJson(v));
      });
    }
  }

  String? _loanServicerName;
  String? _userId;
  List<SecurityDetails>? _securityDetails;
  String? _authStep;
  AuthStepParams? _authStepParams;
  int? _time;
  String? _liabilityType;

  String? get loanServicerName => _loanServicerName;
  String? get userId => _userId;
  List<SecurityDetails>? get securityDetails => _securityDetails;
  String? get authStep => _authStep;
  AuthStepParams? get authStepParams => _authStepParams;
  int? get time => _time;
  String? get liabilityType => _liabilityType;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['loanServicerName'] = _loanServicerName;
    map['userId'] = _userId;
    if (_securityDetails != null) {
      map['securityDetails'] = _securityDetails?.map((v) => v.toJson()).toList();
    }
    map['authStep'] = _authStep;
    if (_authStepParams != null) {
      map['authStepParams'] = _authStepParams?.toJson();
    }
    map['time'] = _time;
    map['liabilityType'] = _liabilityType;
    return map;
  }
}
