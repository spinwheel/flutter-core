class AppliedToLoans {
  String? _loanId;
  num? _percentage;

  String? get loanId => _loanId;

  num? get percentage => _percentage;

  AppliedToLoans({String? loanId, num? percentage}) {
    _loanId = loanId;
    _percentage = percentage;
  }

  AppliedToLoans.fromJson(dynamic json) {
    _loanId = json['loanId'];
    _percentage = json['percentage'];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['loanId'] = _loanId;
    map['percentage'] = _percentage;
    return map;
  }
}
