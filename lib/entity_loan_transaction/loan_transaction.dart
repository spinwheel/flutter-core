import 'payment_details.dart';

class LoanTransaction {
  String? _loanAccountId;
  String? _loanServicerId;
  String? _paymentDate;
  num? _paymentAmount;
  num? _appliedPrincipal;
  num? _appliedInterest;
  num? _appliedFees;
  String? _paymentType;
  List<PaymentDetails>? _paymentDetails;
  bool? _isInternalTxn;
  String? _status;
  String? _loanServicerName;

  String? get loanAccountId => _loanAccountId;

  String get loanServicerId => _loanServicerId ?? '';

  String get paymentDate => _paymentDate ?? '-- / -- / ----';

  num get paymentAmount => _paymentAmount ?? 0;

  num? get appliedPrincipal => _appliedPrincipal;

  num? get appliedInterest => _appliedInterest;

  num? get appliedFees => _appliedFees;

  String get paymentType => _paymentType ?? '';

  List<PaymentDetails>? get paymentDetails => _paymentDetails;

  bool? get isInternalTxn => _isInternalTxn;

  String get status => _status ?? '';

  String get loanServicerName => _loanServicerName ?? '';

  LoanTransaction(
      {String? loanAccountId,
      String? loanServicerId,
      String? paymentDate,
      num? paymentAmount,
      num? appliedPrincipal,
      num? appliedInterest,
      num? appliedFees,
      String? paymentType,
      List<PaymentDetails>? paymentDetails,
      bool? isInternalTxn,
      String? status,
      String? loanServicerName}) {
    _loanAccountId = loanAccountId;
    _loanServicerId = loanServicerId;
    _paymentDate = paymentDate;
    _paymentAmount = paymentAmount;
    _appliedPrincipal = appliedPrincipal;
    _appliedInterest = appliedInterest;
    _appliedFees = appliedFees;
    _paymentType = paymentType;
    _paymentDetails = paymentDetails;
    _isInternalTxn = isInternalTxn;
    _status = status;
    _loanServicerName = loanServicerName;
  }

  LoanTransaction.fromJson(dynamic json) {
    _loanAccountId = json['loanAccountId'];
    _loanServicerId = json['loanServicerId'];
    _paymentDate = json['paymentDate'];
    _paymentAmount = json['paymentAmount'];
    _appliedPrincipal = json['appliedPrincipal'];
    _appliedInterest = json['appliedInterest'];
    _appliedFees = json['appliedFees'];
    _paymentType = json['paymentType'];
    if (json['paymentDetails'] != null) {
      _paymentDetails = [];
      json['paymentDetails'].forEach((v) {
        _paymentDetails?.add(PaymentDetails.fromJson(v));
      });
    }
    _isInternalTxn = json['isInternalTxn'];
    _status = json['status'];
    _loanServicerName = json['loanServicerName'];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['loanAccountId'] = _loanAccountId;
    map['loanServicerId'] = _loanServicerId;
    map['paymentDate'] = _paymentDate;
    map['paymentAmount'] = _paymentAmount;
    map['appliedPrincipal'] = _appliedPrincipal;
    map['appliedInterest'] = _appliedInterest;
    map['appliedFees'] = _appliedFees;
    map['paymentType'] = _paymentType;
    if (_paymentDetails != null) {
      map['paymentDetails'] = _paymentDetails?.map((v) => v.toJson()).toList();
    }
    map['isInternalTxn'] = _isInternalTxn;
    map['status'] = _status;
    map['loanServicerName'] = _loanServicerName;
    return map;
  }
}
