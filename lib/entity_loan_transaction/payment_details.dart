import 'applied_to_loans.dart';

class PaymentDetails {
  String? _appliedTxnId;
  List<AppliedToLoans>? _appliedToLoans;

  String? get appliedTxnId => _appliedTxnId;

  List<AppliedToLoans>? get appliedToLoans => _appliedToLoans;

  PaymentDetails({String? appliedTxnId, List<AppliedToLoans>? appliedToLoans}) {
    _appliedTxnId = appliedTxnId;
    _appliedToLoans = appliedToLoans;
  }

  PaymentDetails.fromJson(dynamic json) {
    _appliedTxnId = json['appliedTxnId'];
    if (json['appliedToLoans'] != null) {
      _appliedToLoans = [];
      json['appliedToLoans'].forEach((v) {
        _appliedToLoans?.add(AppliedToLoans.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['appliedTxnId'] = _appliedTxnId;
    if (_appliedToLoans != null) {
      map['appliedToLoans'] = _appliedToLoans?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}
