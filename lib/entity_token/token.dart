import 'package:jwt_decode/jwt_decode.dart';
import 'package:sw_core/environment/sw_env.dart';

import '../tool/inject.dart';

class DecodedToken {
  DecodedToken({
    String? clientId,
    String? partnerId,
    String? extUserId,
    String? spinwheelUserId,
    int? tokenExpiryInSec,
    bool? isCdnRequest,
    String? env,
    bool? isDefaultExpiryTime,
    int? iat,
    int? exp,
  }) {
    _clientId = clientId;
    _partnerId = partnerId;
    _extUserId = extUserId;
    _spinwheelUserId = spinwheelUserId;
    _tokenExpiryInSec = tokenExpiryInSec;
    _isCdnRequest = isCdnRequest;
    _env = env;
    _isDefaultExpiryTime = isDefaultExpiryTime;
    _iat = iat;
    _exp = exp;
  }

  DecodedToken.fromJson(dynamic json) {
    _clientId = json['clientId'];
    _partnerId = json['partnerId'];
    _extUserId = json['extUserId'];
    _spinwheelUserId = json['spinwheelUserId'];
    _tokenExpiryInSec = json['tokenExpiryInSec'];
    _isCdnRequest = json['isCdnRequest'];
    _env = json['env'];
    _isDefaultExpiryTime = json['isDefaultExpiryTime'];
    _iat = json['iat'];
    _exp = json['exp'];

    switch (_env) {
      case 'sandbox':
        replace<SWEnvironment>(() => SWEnvironment.DEV);
        break;
      case 'prod':
        replace<SWEnvironment>(() => SWEnvironment.PROD);
        break;
    }
  }

  factory DecodedToken.fromToken(String token) => DecodedToken.fromJson(Jwt.parseJwt(token));

  String? _clientId;
  String? _partnerId;
  String? _extUserId;
  String? _spinwheelUserId;
  int? _tokenExpiryInSec;
  bool? _isCdnRequest;
  String? _env;
  bool? _isDefaultExpiryTime;
  int? _iat;
  int? _exp;

  String get clientId => _clientId ?? "";

  String get partnerId => _partnerId ?? "";

  String get extUserId => _extUserId ?? "";

  String get spinwheelUserId => _spinwheelUserId ?? "";

  int get tokenExpiryInSec => _tokenExpiryInSec ?? 3600;

  bool get isCdnRequest => _isCdnRequest ?? false;

  String get env => _env ?? "sandbox";

  bool get isDefaultExpiryTime => _isDefaultExpiryTime ?? false;

  int get iat => _iat ?? 0;

  int get exp => _exp ?? 0;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['clientId'] = _clientId;
    map['partnerId'] = _partnerId;
    map['extUserId'] = _extUserId;
    map['spinwheelUserId'] = _spinwheelUserId;
    map['tokenExpiryInSec'] = _tokenExpiryInSec;
    map['isCdnRequest'] = _isCdnRequest;
    map['env'] = _env;
    map['isDefaultExpiryTime'] = _isDefaultExpiryTime;
    map['iat'] = _iat;
    map['exp'] = _exp;
    return map;
  }
}
