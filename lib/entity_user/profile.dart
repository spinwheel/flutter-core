class Profile {
  Profile({
    String? name,
    String? email,
    String? phoneNumber,
    String? zipCode,
  }) {
    _name = name;
    _email = email;
    _phoneNumber = phoneNumber;
    _zipCode = zipCode;
  }

  Profile.fromJson(dynamic json) {
    _name = json['name'];
    _email = json['email'];
    _phoneNumber = json['phoneNumber'];
    _zipCode = json['zipCode'];
  }

  String? _name;
  String? _email;
  String? _phoneNumber;
  String? _zipCode;

  String? get name => _name;

  String? get email => _email;

  String? get phoneNumber => _phoneNumber;

  String? get zipCode => _zipCode;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['name'] = _name;
    map['email'] = _email;
    map['phoneNumber'] = _phoneNumber;
    map['zipCode'] = _zipCode;
    return map;
  }
}
