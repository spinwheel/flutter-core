class LoanAccountssummary {
  LoanAccountssummary({
    num? outstandingBalance,
    num? outstandingInterest,
    num? regularMonthlyPayment,
    num? pendingLoanTermInMonths,
    String? expectedPayoffDate,
    num? interestRate,
    num? originalPrincipalAmount,
    num? principalBalance,
  }) {
    _outstandingBalance = outstandingBalance;
    _outstandingInterest = outstandingInterest;
    _regularMonthlyPayment = regularMonthlyPayment;
    _pendingLoanTermInMonths = pendingLoanTermInMonths;
    _expectedPayoffDate = expectedPayoffDate;
    _interestRate = interestRate;
    _originalPrincipalAmount = originalPrincipalAmount;
    _principalBalance = principalBalance;
  }

  LoanAccountssummary.fromJson(dynamic json) {
    _outstandingBalance = json['outstandingBalance'];
    _outstandingInterest = json['outstandingInterest'];
    _regularMonthlyPayment = json['regularMonthlyPayment'];
    _pendingLoanTermInMonths = json['pendingLoanTermInMonths'];
    _expectedPayoffDate = json['expectedPayoffDate'];
    _interestRate = json['interestRate'];
    _originalPrincipalAmount = json['originalPrincipalAmount'];
    _principalBalance = json['principalBalance'];
  }

  num? _outstandingBalance;
  num? _outstandingInterest;
  num? _regularMonthlyPayment;
  num? _pendingLoanTermInMonths;
  String? _expectedPayoffDate;
  num? _interestRate;
  num? _originalPrincipalAmount;
  num? _principalBalance;

  num? get outstandingBalance => _outstandingBalance;

  num? get outstandingInterest => _outstandingInterest;

  num? get regularMonthlyPayment => _regularMonthlyPayment;

  num? get pendingLoanTermInMonths => _pendingLoanTermInMonths;

  String? get expectedPayoffDate => _expectedPayoffDate;

  num? get interestRate => _interestRate;

  num? get originalPrincipalAmount => _originalPrincipalAmount;

  num? get principalBalance => _principalBalance;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['outstandingBalance'] = _outstandingBalance;
    map['outstandingInterest'] = _outstandingInterest;
    map['regularMonthlyPayment'] = _regularMonthlyPayment;
    map['pendingLoanTermInMonths'] = _pendingLoanTermInMonths;
    map['expectedPayoffDate'] = _expectedPayoffDate;
    map['interestRate'] = _interestRate;
    map['originalPrincipalAmount'] = _originalPrincipalAmount;
    map['principalBalance'] = _principalBalance;
    return map;
  }
}
