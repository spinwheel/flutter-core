class LastConnectedLoanServicerDetails {
  LastConnectedLoanServicerDetails({
    String? loanServicerId,
    num? connectedAt,
    List<String>? loanAccountIds,
  }) {
    _loanServicerId = loanServicerId;
    _connectedAt = connectedAt;
    _loanAccountIds = loanAccountIds;
  }

  LastConnectedLoanServicerDetails.fromJson(dynamic json) {
    _loanServicerId = json['loanServicerId'];
    _connectedAt = json['connectedAt'];
    _loanAccountIds = json['loanAccountIds'] != null ? json['loanAccountIds'].cast<String>() : [];
  }

  String? _loanServicerId;
  num? _connectedAt;
  List<String>? _loanAccountIds;

  String? get loanServicerId => _loanServicerId;

  num? get connectedAt => _connectedAt;

  List<String>? get loanAccountIds => _loanAccountIds;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['loanServicerId'] = _loanServicerId;
    map['connectedAt'] = _connectedAt;
    map['loanAccountIds'] = _loanAccountIds;
    return map;
  }
}
