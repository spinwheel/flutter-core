import 'package:sw_core/entity_auth/user_pass.dart';

class UnintegratedUser extends UserPass {
  UnintegratedUser({
    String? firstName,
    String? lastName,
    String? emailID,
    String? loanServicerName,
    String? username,
    String? userId,
    String? password,
    String? pin,
    String? dob,
    String? accountNumber,
  }) : super(
    firstName: firstName,
    lastName: lastName,
    emailID: emailID,
    loanServicerName: loanServicerName,
    username: username,
    userId: userId,
    password: password,
    pin: pin,
    dob: dob,
    accountNumber: accountNumber
  );
}
