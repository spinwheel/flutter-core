class StudentLoanAccountSummary {
  StudentLoanAccountSummary({
    num? numberOfLoanAccounts,
    num? numberOfLoanServicers,
    String? recommendedLoanId,
    num? outstandingBalance,
    num? outstandingInterest,
    num? regularMonthlyPayment,
    num? pendingLoanTermInMonths,
    num? interestRate,
    num? originalPrincipalAmount,
    num? principalBalance,
    String? payoffDate,
  }) {
    _numberOfLoanAccounts = numberOfLoanAccounts;
    _numberOfLoanServicers = numberOfLoanServicers;
    _recommendedLoanId = recommendedLoanId;
    _outstandingBalance = outstandingBalance;
    _outstandingInterest = outstandingInterest;
    _regularMonthlyPayment = regularMonthlyPayment;
    _pendingLoanTermInMonths = pendingLoanTermInMonths;
    _interestRate = interestRate;
    _originalPrincipalAmount = originalPrincipalAmount;
    _principalBalance = principalBalance;
    _payoffDate = payoffDate;
  }

  StudentLoanAccountSummary.fromJson(dynamic json) {
    _numberOfLoanAccounts = json['numberOfLoanAccounts'];
    _numberOfLoanServicers = json['numberOfLoanServicers'];
    _recommendedLoanId = json['recommendedLoanId'];
    _outstandingBalance = json['outstandingBalance'];
    _outstandingInterest = json['outstandingInterest'];
    _regularMonthlyPayment = json['regularMonthlyPayment'];
    _pendingLoanTermInMonths = json['pendingLoanTermInMonths'];
    _interestRate = json['interestRate'];
    _originalPrincipalAmount = json['originalPrincipalAmount'];
    _principalBalance = json['principalBalance'];
    _payoffDate = json['payoffDate'];
  }

  num? _numberOfLoanAccounts;
  num? _numberOfLoanServicers;
  String? _recommendedLoanId;
  num? _outstandingBalance;
  num? _outstandingInterest;
  num? _regularMonthlyPayment;
  num? _pendingLoanTermInMonths;
  num? _interestRate;
  num? _originalPrincipalAmount;
  num? _principalBalance;
  String? _payoffDate;

  num? get numberOfLoanAccounts => _numberOfLoanAccounts;

  num? get numberOfLoanServicers => _numberOfLoanServicers;

  String? get recommendedLoanId => _recommendedLoanId;

  num? get outstandingBalance => _outstandingBalance;

  num? get outstandingInterest => _outstandingInterest;

  num? get regularMonthlyPayment => _regularMonthlyPayment;

  num? get pendingLoanTermInMonths => _pendingLoanTermInMonths;

  num? get interestRate => _interestRate;

  num? get originalPrincipalAmount => _originalPrincipalAmount;

  num? get principalBalance => _principalBalance;

  String? get payoffDate => _payoffDate;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['numberOfLoanAccounts'] = _numberOfLoanAccounts;
    map['numberOfLoanServicers'] = _numberOfLoanServicers;
    map['recommendedLoanId'] = _recommendedLoanId;
    map['outstandingBalance'] = _outstandingBalance;
    map['outstandingInterest'] = _outstandingInterest;
    map['regularMonthlyPayment'] = _regularMonthlyPayment;
    map['pendingLoanTermInMonths'] = _pendingLoanTermInMonths;
    map['interestRate'] = _interestRate;
    map['originalPrincipalAmount'] = _originalPrincipalAmount;
    map['principalBalance'] = _principalBalance;
    map['payoffDate'] = _payoffDate;
    return map;
  }
}
