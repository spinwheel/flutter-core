import 'profile.dart';
import 'student_loan_accountssummary.dart';
import 'loan_accounts.dart';
import 'connection_status.dart';

class StudentLoanAccounts {
  StudentLoanAccounts({
    Profile? profile,
    StudentLoanAccountsSummary? studentLoanAccountsSummary,
    List<LoanAccounts>? loanAccounts,
    num? createdOn,
    num? updatedOn,
    String? accountId,
    String? loanServicerId,
    bool? isLoanServicerIntegrated,
    ConnectionStatus? connectionStatus,
    String? accountUserName,
  }) {
    _profile = profile;
    _studentLoanAccountsSummary = studentLoanAccountsSummary;
    _loanAccounts = loanAccounts;
    _createdOn = createdOn;
    _updatedOn = updatedOn;
    _accountId = accountId;
    _loanServicerId = loanServicerId;
    _isLoanServicerIntegrated = isLoanServicerIntegrated;
    _connectionStatus = connectionStatus;
    _accountUserName = accountUserName;
  }

  StudentLoanAccounts.fromJson(dynamic json) {
    _profile = json['profile'] != null ? Profile.fromJson(json['profile']) : null;
    _studentLoanAccountsSummary =
        json['summary'] != null ? StudentLoanAccountsSummary.fromJson(json['summary']) : null;
    if (json['loanAccounts'] != null) {
      _loanAccounts = [];
      json['loanAccounts'].forEach((v) {
        _loanAccounts?.add(LoanAccounts.fromJson(v));
      });
    }
    _createdOn = json['createdOn'];
    _updatedOn = json['updatedOn'];
    _accountId = json['accountId'];
    _loanServicerId = json['loanServicerId'];
    _isLoanServicerIntegrated = json['isLoanServicerIntegrated'];
    _connectionStatus = json['connectionStatus'] != null
        ? ConnectionStatus.fromJson(json['connectionStatus'])
        : null;
    _accountUserName = json['accountUserName'];
  }

  Profile? _profile;
  StudentLoanAccountsSummary? _studentLoanAccountsSummary;
  List<LoanAccounts>? _loanAccounts;
  num? _createdOn;
  num? _updatedOn;
  String? _accountId;
  String? _loanServicerId;
  bool? _isLoanServicerIntegrated;
  ConnectionStatus? _connectionStatus;
  String? _accountUserName;

  Profile? get profile => _profile;

  StudentLoanAccountsSummary? get studentLoanAccountssummary => _studentLoanAccountsSummary;

  List<LoanAccounts> get loanAccounts => _loanAccounts ?? [];

  num? get createdOn => _createdOn;

  num? get updatedOn => _updatedOn;

  String? get accountId => _accountId;

  String? get loanServicerId => _loanServicerId;

  bool? get isLoanServicerIntegrated => _isLoanServicerIntegrated;

  ConnectionStatus? get connectionStatus => _connectionStatus;

  String? get accountUserName => _accountUserName;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_profile != null) {
      map['profile'] = _profile?.toJson();
    }
    if (_studentLoanAccountsSummary != null) {
      map['summary'] = _studentLoanAccountsSummary?.toJson();
    }
    if (_loanAccounts != null) {
      map['loanAccounts'] = _loanAccounts?.map((v) => v.toJson()).toList();
    }
    map['createdOn'] = _createdOn;
    map['updatedOn'] = _updatedOn;
    map['accountId'] = _accountId;
    map['loanServicerId'] = _loanServicerId;
    map['isLoanServicerIntegrated'] = _isLoanServicerIntegrated;
    if (_connectionStatus != null) {
      map['connectionStatus'] = _connectionStatus?.toJson();
    }
    map['accountUserName'] = _accountUserName;
    return map;
  }
}
