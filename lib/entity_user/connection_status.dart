import 'status_change_logs.dart';

class ConnectionStatus {
  ConnectionStatus({
    List<StatusChangeLogs>? statusChangeLogs,
    bool? isAuthSuccessful,
    num? lastAuthAttemptOn,
    num? lastSuccessfulAuthOn,
    num? statusCode,
    bool? isValid,
    bool? isSecurityStepPending,
    String? description,
    dynamic userAction,
    bool? isQuestionStepPending,
  }) {
    _statusChangeLogs = statusChangeLogs;
    _isAuthSuccessful = isAuthSuccessful;
    _lastAuthAttemptOn = lastAuthAttemptOn;
    _lastSuccessfulAuthOn = lastSuccessfulAuthOn;
    _statusCode = statusCode;
    _isValid = isValid;
    _isSecurityStepPending = isSecurityStepPending;
    _description = description;
    _userAction = userAction;
    _isQuestionStepPending = isQuestionStepPending;
  }

  ConnectionStatus.fromJson(dynamic json) {
    if (json['statusChangeLogs'] != null) {
      _statusChangeLogs = [];
      json['statusChangeLogs'].forEach((v) {
        _statusChangeLogs?.add(StatusChangeLogs.fromJson(v));
      });
    }
    _isAuthSuccessful = json['isAuthSuccessful'];
    _lastAuthAttemptOn = json['lastAuthAttemptOn'];
    _lastSuccessfulAuthOn = json['lastSuccessfulAuthOn'];
    _statusCode = json['statusCode'];
    _isValid = json['isValid'];
    _isSecurityStepPending = json['isSecurityStepPending'];
    _description = json['description'];
    _userAction = json['userAction'];
    _isQuestionStepPending = json['isQuestionStepPending'];
  }

  List<StatusChangeLogs>? _statusChangeLogs;
  bool? _isAuthSuccessful;
  num? _lastAuthAttemptOn;
  num? _lastSuccessfulAuthOn;
  num? _statusCode;
  bool? _isValid;
  bool? _isSecurityStepPending;
  String? _description;
  dynamic _userAction;
  bool? _isQuestionStepPending;

  List<StatusChangeLogs>? get statusChangeLogs => _statusChangeLogs;

  bool? get isAuthSuccessful => _isAuthSuccessful;

  num? get lastAuthAttemptOn => _lastAuthAttemptOn;

  num? get lastSuccessfulAuthOn => _lastSuccessfulAuthOn;

  num? get statusCode => _statusCode;

  bool? get isValid => _isValid;

  bool? get isSecurityStepPending => _isSecurityStepPending;

  String? get description => _description;

  dynamic get userAction => _userAction;

  bool? get isQuestionStepPending => _isQuestionStepPending;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_statusChangeLogs != null) {
      map['statusChangeLogs'] = _statusChangeLogs?.map((v) => v.toJson()).toList();
    }
    map['isAuthSuccessful'] = _isAuthSuccessful;
    map['lastAuthAttemptOn'] = _lastAuthAttemptOn;
    map['lastSuccessfulAuthOn'] = _lastSuccessfulAuthOn;
    map['statusCode'] = _statusCode;
    map['isValid'] = _isValid;
    map['isSecurityStepPending'] = _isSecurityStepPending;
    map['description'] = _description;
    map['userAction'] = _userAction;
    map['isQuestionStepPending'] = _isQuestionStepPending;
    return map;
  }
}
