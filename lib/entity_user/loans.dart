class Loans {
  Loans({
    String? loanName,
    String? dueDate,
    String? loanType,
    String? status,
    num? pendingLoanTermInMonths,
    num? origAmount,
    num? interestRate,
    num? principalBalance,
    num? outstandingInterest,
    String? repaymentPlan,
    num? outstandingBalance,
    num? calculatedPriorityScore,
    String? expectedPayoffDate,
    num? regularMonthlyPayment,
    String? loanId,
    num? createdOn,
  }) {
    _loanName = loanName;
    _dueDate = dueDate;
    _loanType = loanType;
    _status = status;
    _pendingLoanTermInMonths = pendingLoanTermInMonths;
    _origAmount = origAmount;
    _interestRate = interestRate;
    _principalBalance = principalBalance;
    _outstandingInterest = outstandingInterest;
    _repaymentPlan = repaymentPlan;
    _outstandingBalance = outstandingBalance;
    _calculatedPriorityScore = calculatedPriorityScore;
    _expectedPayoffDate = expectedPayoffDate;
    _regularMonthlyPayment = regularMonthlyPayment;
    _loanId = loanId;
    _createdOn = createdOn;
  }

  Loans.fromJson(dynamic json) {
    _loanName = json['loanName'];
    _dueDate = json['dueDate'];
    _loanType = json['loanType'];
    _status = json['status'];
    _pendingLoanTermInMonths = json['pendingLoanTermInMonths'];
    _origAmount = json['origAmount'];
    _interestRate = json['interestRate'];
    _principalBalance = json['principalBalance'];
    _outstandingInterest = json['outstandingInterest'];
    _repaymentPlan = json['repaymentPlan'];
    _outstandingBalance = json['outstandingBalance'];
    _calculatedPriorityScore = json['calculatedPriorityScore'];
    _expectedPayoffDate = json['expectedPayoffDate'];
    _regularMonthlyPayment = json['regularMonthlyPayment'];
    _loanId = json['loanId'];
    _createdOn = json['createdOn'];
  }

  String? _loanName;
  String? _dueDate;
  String? _loanType;
  String? _status;
  num? _pendingLoanTermInMonths;
  num? _origAmount;
  num? _interestRate;
  num? _principalBalance;
  num? _outstandingInterest;
  String? _repaymentPlan;
  num? _outstandingBalance;
  num? _calculatedPriorityScore;
  String? _expectedPayoffDate;
  num? _regularMonthlyPayment;
  String? _loanId;
  num? _createdOn;

  String? get loanName => _loanName;

  String? get dueDate => _dueDate;

  String? get loanType => _loanType;

  String? get status => _status;

  num? get pendingLoanTermInMonths => _pendingLoanTermInMonths;

  num? get origAmount => _origAmount;

  num? get interestRate => _interestRate;

  num? get principalBalance => _principalBalance;

  num? get outstandingInterest => _outstandingInterest;

  String? get repaymentPlan => _repaymentPlan;

  num? get outstandingBalance => _outstandingBalance;

  num? get calculatedPriorityScore => _calculatedPriorityScore;

  String? get expectedPayoffDate => _expectedPayoffDate;

  num? get regularMonthlyPayment => _regularMonthlyPayment;

  String? get loanId => _loanId;

  num? get createdOn => _createdOn;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['loanName'] = _loanName;
    map['dueDate'] = _dueDate;
    map['loanType'] = _loanType;
    map['status'] = _status;
    map['pendingLoanTermInMonths'] = _pendingLoanTermInMonths;
    map['origAmount'] = _origAmount;
    map['interestRate'] = _interestRate;
    map['principalBalance'] = _principalBalance;
    map['outstandingInterest'] = _outstandingInterest;
    map['repaymentPlan'] = _repaymentPlan;
    map['outstandingBalance'] = _outstandingBalance;
    map['calculatedPriorityScore'] = _calculatedPriorityScore;
    map['expectedPayoffDate'] = _expectedPayoffDate;
    map['regularMonthlyPayment'] = _regularMonthlyPayment;
    map['loanId'] = _loanId;
    map['createdOn'] = _createdOn;
    return map;
  }
}
