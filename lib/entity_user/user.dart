import 'student_loan_accounts.dart';
import 'student_loan_account_summary.dart';
import 'last_connected_loan_servicer_details.dart';

class User {
  User({
    bool? isChampion,
    bool? isBorrower,
    bool? isRegistered,
    String? id,
    String? partnerId,
    String? extUserId,
    List<StudentLoanAccounts>? studentLoanAccounts,
    num? createdOn,
    num? updatedOn,
    num? v,
    StudentLoanAccountSummary? studentLoanAccountSummary,
    LastConnectedLoanServicerDetails? lastConnectedLoanServicerDetails,
  }) {
    _isChampion = isChampion;
    _isBorrower = isBorrower;
    _isRegistered = isRegistered;
    _id = id;
    _partnerId = partnerId;
    _extUserId = extUserId;
    _studentLoanAccounts = studentLoanAccounts;
    _createdOn = createdOn;
    _updatedOn = updatedOn;
    _v = v;
    _studentLoanAccountSummary = studentLoanAccountSummary;
    _lastConnectedLoanServicerDetails = lastConnectedLoanServicerDetails;
  }

  User.fromJson(dynamic json) {
    _isChampion = json['isChampion'];
    _isBorrower = json['isBorrower'];
    _isRegistered = json['isRegistered'];
    _id = json['_id'];
    _partnerId = json['partnerId'];
    _extUserId = json['extUserId'];
    if (json['studentLoanAccounts'] != null) {
      _studentLoanAccounts = [];
      json['studentLoanAccounts'].forEach((v) {
        _studentLoanAccounts?.add(StudentLoanAccounts.fromJson(v));
      });
    }
    _createdOn = json['createdOn'];
    _updatedOn = json['updatedOn'];
    _v = json['__v'];
    _studentLoanAccountSummary =
        json['summary'] != null ? StudentLoanAccountSummary.fromJson(json['summary']) : null;
    _lastConnectedLoanServicerDetails = json['lastConnectedLoanServicerDetails'] != null
        ? LastConnectedLoanServicerDetails.fromJson(json['lastConnectedLoanServicerDetails'])
        : null;
  }

  bool? _isChampion;
  bool? _isBorrower;
  bool? _isRegistered;
  String? _id;
  String? _partnerId;
  String? _extUserId;
  List<StudentLoanAccounts>? _studentLoanAccounts;
  num? _createdOn;
  num? _updatedOn;
  num? _v;
  StudentLoanAccountSummary? _studentLoanAccountSummary;
  LastConnectedLoanServicerDetails? _lastConnectedLoanServicerDetails;

  bool? get isChampion => _isChampion;

  bool? get isBorrower => _isBorrower;

  bool? get isRegistered => _isRegistered;

  String get id => _id ?? '';

  String? get partnerId => _partnerId;

  String? get extUserId => _extUserId;

  List<StudentLoanAccounts> get studentLoanAccounts => _studentLoanAccounts ?? [];

  num? get createdOn => _createdOn;

  num? get updatedOn => _updatedOn;

  num? get v => _v;

  StudentLoanAccountSummary? get studentLoanAccountSummary => _studentLoanAccountSummary;

  LastConnectedLoanServicerDetails? get lastConnectedLoanServicerDetails =>
      _lastConnectedLoanServicerDetails;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['isChampion'] = _isChampion;
    map['isBorrower'] = _isBorrower;
    map['isRegistered'] = _isRegistered;
    map['_id'] = _id;
    map['partnerId'] = _partnerId;
    map['extUserId'] = _extUserId;
    if (_studentLoanAccounts != null) {
      map['studentLoanAccounts'] = _studentLoanAccounts?.map((v) => v.toJson()).toList();
    }
    map['createdOn'] = _createdOn;
    map['updatedOn'] = _updatedOn;
    map['__v'] = _v;
    if (_studentLoanAccountSummary != null) {
      map['summary'] = _studentLoanAccountSummary?.toJson();
    }
    if (_lastConnectedLoanServicerDetails != null) {
      map['lastConnectedLoanServicerDetails'] = _lastConnectedLoanServicerDetails?.toJson();
    }
    return map;
  }
}
