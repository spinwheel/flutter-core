import 'loans.dart';
import 'payments.dart';
import 'loan_accountssummary.dart';

class LoanAccounts {
  LoanAccounts({
    List<Loans>? loans,
    List<Payments>? payments,
    LoanAccountssummary? loanAccountssummary,
    String? name,
    String? loanAccountId,
  }) {
    _loans = loans;
    _payments = payments;
    _loanAccountssummary = loanAccountssummary;
    _name = name;
    _loanAccountId = loanAccountId;
  }

  LoanAccounts.fromJson(dynamic json) {
    if (json['loans'] != null) {
      _loans = [];
      json['loans'].forEach((v) {
        _loans?.add(Loans.fromJson(v));
      });
    }
    if (json['payments'] != null) {
      _payments = [];
      json['payments'].forEach((v) {
        _payments?.add(Payments.fromJson(v));
      });
    }
    _loanAccountssummary = json['loanAccountssummary'] != null
        ? LoanAccountssummary.fromJson(json['loanAccountssummary'])
        : null;
    _name = json['name'];
    _loanAccountId = json['loanAccountId'];
  }

  List<Loans>? _loans;
  List<Payments>? _payments;
  LoanAccountssummary? _loanAccountssummary;
  String? _name;
  String? _loanAccountId;

  List<Loans>? get loans => _loans;

  List<Payments>? get payments => _payments;

  LoanAccountssummary? get loanAccountssummary => _loanAccountssummary;

  String? get name => _name;

  String? get loanAccountId => _loanAccountId;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_loans != null) {
      map['loans'] = _loans?.map((v) => v.toJson()).toList();
    }
    if (_payments != null) {
      map['payments'] = _payments?.map((v) => v.toJson()).toList();
    }
    if (_loanAccountssummary != null) {
      map['loanAccountssummary'] = _loanAccountssummary?.toJson();
    }
    map['name'] = _name;
    map['loanAccountId'] = _loanAccountId;
    return map;
  }
}
