class StatusChangeLogs {
  StatusChangeLogs({
    String? updatedStatus,
    num? statusUpdateTs,
  }) {
    _updatedStatus = updatedStatus;
    _statusUpdateTs = statusUpdateTs;
  }

  StatusChangeLogs.fromJson(dynamic json) {
    _updatedStatus = json['updatedStatus'];
    _statusUpdateTs = json['statusUpdateTs'];
  }

  String? _updatedStatus;
  num? _statusUpdateTs;

  String? get updatedStatus => _updatedStatus;

  num? get statusUpdateTs => _statusUpdateTs;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['updatedStatus'] = _updatedStatus;
    map['statusUpdateTs'] = _statusUpdateTs;
    return map;
  }
}
