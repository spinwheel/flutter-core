class Payments {
  Payments({
    String? paymentDate,
    num? paymentAmount,
    num? appliedPrincipal,
    num? appliedInterest,
    num? appliedFees,
    String? paymentType,
  }) {
    _paymentDate = paymentDate;
    _paymentAmount = paymentAmount;
    _appliedPrincipal = appliedPrincipal;
    _appliedInterest = appliedInterest;
    _appliedFees = appliedFees;
    _paymentType = paymentType;
  }

  Payments.fromJson(dynamic json) {
    _paymentDate = json['paymentDate'];
    _paymentAmount = json['paymentAmount'];
    _appliedPrincipal = json['appliedPrincipal'];
    _appliedInterest = json['appliedInterest'];
    _appliedFees = json['appliedFees'];
    _paymentType = json['paymentType'];
  }

  String? _paymentDate;
  num? _paymentAmount;
  num? _appliedPrincipal;
  num? _appliedInterest;
  num? _appliedFees;
  String? _paymentType;

  String? get paymentDate => _paymentDate;

  num? get paymentAmount => _paymentAmount;

  num? get appliedPrincipal => _appliedPrincipal;

  num? get appliedInterest => _appliedInterest;

  num? get appliedFees => _appliedFees;

  String? get paymentType => _paymentType;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['paymentDate'] = _paymentDate;
    map['paymentAmount'] = _paymentAmount;
    map['appliedPrincipal'] = _appliedPrincipal;
    map['appliedInterest'] = _appliedInterest;
    map['appliedFees'] = _appliedFees;
    map['paymentType'] = _paymentType;
    return map;
  }
}
