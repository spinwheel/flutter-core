import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:get/route_manager.dart';

BuildContext get buildContext => Get.context!;

ThemeData get theme => Get.theme;

ColorScheme get colorScheme => theme.colorScheme;

bool get isDark => SchedulerBinding.instance!.window.platformBrightness == Brightness.dark;
