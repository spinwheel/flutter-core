import 'package:logger/logger.dart';
import 'package:sw_core/interface/i_analytics.dart';
import '../interface/i_logger.dart';
import 'inject.dart';

class SWLogger extends Logger implements ILogger {
  SWLogger()
      : super(
          printer: PrettyPrinter(
            methodCount: 0,
            errorMethodCount: 12,
            lineLength: 77,
            colors: true,
            printTime: false,
            noBoxingByDefault: false,
          ),
        ) {
    replace<ILogger>(() => this);
  }
}

bool get _logEnabled => exists<ILogger>();

bool get _analyticsEnabled => exists<IAnalytics>();

ILogger? get _logger => getSafe<ILogger>();

verbose(dynamic message, [dynamic error, StackTrace? stackTrace]) {
  if (_logEnabled) {
    _logger?.v(message, error, stackTrace);
    _track(message, error, stackTrace);
  }
}

debug(dynamic message, [dynamic error, StackTrace? stackTrace]) {
  if (_logEnabled) {
    _logger?.d(message, error, stackTrace);
    _track(message, error, stackTrace);
  }
}

info(dynamic message, [dynamic error, StackTrace? stackTrace]) {
  if (_logEnabled) {
    _logger?.i(message, error, stackTrace);
    _track(message, error, stackTrace);
  }
}

warning(dynamic message, [dynamic error, StackTrace? stackTrace]) {
  if (_logEnabled) {
    _logger?.w(message, error, stackTrace);
    _track(message, error, stackTrace);
  }
}

error(dynamic message, [dynamic error, StackTrace? stackTrace]) {
  if (_logEnabled) {
    _logger?.e(message, error, stackTrace);
    _track(message, error, stackTrace);
  }
}

errorWithStack(Object error, StackTrace stackTrace) {
  if (_logEnabled) {
    _logger?.e(error.toString(), error, stackTrace);
    _track(error, error, stackTrace);
  }
}

wtf(dynamic message, [dynamic error, StackTrace? stackTrace]) {
  if (_logEnabled) {
    _logger?.wtf(message, error, stackTrace);
    _track(message, error, stackTrace);
  }
}

_track(dynamic message, [dynamic error, StackTrace? stackTrace]) {
  if (_analyticsEnabled) {
    get<IAnalytics>().track(
      message.toString(),
      properties: (error != null || stackTrace != null)
          ? {
              'error': error,
              'stackTrace': stackTrace,
            }
          : {},
    );
  }
}
