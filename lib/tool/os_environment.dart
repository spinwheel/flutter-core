import 'dart:io';

import 'package:flutter/foundation.dart';

enum OS {
  /// iOS: <https://www.apple.com/ios/>
  ios,

  /// Windows: <https://www.windows.com>
  windows,

  /// macOS: <https://www.apple.com/macos>
  macos,

  /// Linux: <https://www.linux.org>
  linux,

  /// Fuchsia: <https://fuchsia.dev/fuchsia-src/concepts>
  fuchsia,

  /// Android: <https://www.android.com/>
  android,
}

extension ExtOS on OS {
  String get name => describeEnum(this);

  static OS currentOS() {
    final os = Platform.operatingSystem;

    if (os == OS.ios.name) {
      return OS.ios;
    } else if (os == OS.windows.name) {
      return OS.windows;
    } else if (os == OS.macos.name) {
      return OS.macos;
    } else if (os == OS.linux.name) {
      return OS.linux;
    } else if (os == OS.fuchsia.name) {
      return OS.fuchsia;
    } else {
      return OS.android;
    }
  }
}
