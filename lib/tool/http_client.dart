import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:dio_cache_interceptor/dio_cache_interceptor.dart';
import 'package:sw_core/interface/service/i_service_token.dart';
import 'inject.dart';
import 'log.dart';

const _xAuth = 'x-auth-user';
const _appJSON = 'application/json';

Dio defaultHTTPClient() => basicHTTPClient([httpInterceptors(), httpCache()]);

Dio httpClient(List<Interceptor> interceptors) {
  final dio = basicHTTPClient(interceptors);
  dio.interceptors.add(_tokenInterceptor(dio));
  return dio;
}

Dio basicHTTPClient(List<Interceptor> interceptors) {
  Dio dio = Dio(BaseOptions(contentType: _appJSON));
  dio.interceptors.addAll(interceptors);
  return dio;
}

InterceptorsWrapper httpInterceptors({
  InterceptorSendCallback? onRequest,
  InterceptorSuccessCallback? onResponse,
  InterceptorErrorCallback? onError,
}) =>
    InterceptorsWrapper(
      onRequest: onRequest ?? _onRequest,
      onResponse: onResponse ?? _onResponse,
      onError: onError ?? _onError,
    );

DioCacheInterceptor httpCache([int minutes = 1]) => DioCacheInterceptor(
      options: CacheOptions(
        store: MemCacheStore(),
        policy: CachePolicy.request,
        hitCacheOnErrorExcept: [401, 403],
        maxStale: Duration(minutes: minutes),
        priority: CachePriority.normal,
      ),
    );

_tokenInterceptor(Dio dio) => InterceptorsWrapper(
      onRequest: _onTokenRequest,
      onError: (
        DioError error,
        ErrorInterceptorHandler handler,
      ) async {
        _isTokenError(error) ? _onInvalidToken(handler, error, dio) : handler.next(error);
      },
    );

_onTokenRequest(RequestOptions options, RequestInterceptorHandler handler) {
  options.headers[_xAuth] = get<IServiceToken>().token;
  handler.next(options);
}

bool _isTokenError(DioError e) {
  final response = e.response;
  final invalid = 'Invalid Token';
  final code = 500;

  if (response != null) {
    error(response.data);
    final status = response.data['status'];
    return (response.statusCode == code && response.statusMessage == invalid) ||
        (status['code'] == code && status['desc'] == invalid);
  }
  return false;
}

_onInvalidToken(ErrorInterceptorHandler handler, DioError error, Dio dio) {
  final service = getSafe<IServiceToken>();
  if (service != null && service.handleToken) {
    service.getAuthToken().then(
      (value) async {
        final options = error.requestOptions;
        options.headers[_xAuth] = get<IServiceToken>().token;
        options.path = options.path.contains('http')
            ? options.path
            : get<IServiceToken>().baseURL + options.path;
        _repeatRequest(handler, dio, options);
      },
    );
  }
}

_repeatRequest(ErrorInterceptorHandler handler, Dio dio, RequestOptions opt) async {
  handler.resolve(
    await dio.request(
      opt.path,
      data: opt.data,
      queryParameters: opt.queryParameters,
      cancelToken: opt.cancelToken,
      options: Options(
        method: opt.method,
        headers: opt.headers,
        sendTimeout: opt.sendTimeout,
        receiveTimeout: opt.receiveTimeout,
        extra: opt.extra,
        responseType: opt.responseType,
        contentType: opt.contentType,
        validateStatus: opt.validateStatus,
        receiveDataWhenStatusError: opt.receiveDataWhenStatusError,
        followRedirects: opt.followRedirects,
        maxRedirects: opt.maxRedirects,
        requestEncoder: opt.requestEncoder,
        responseDecoder: opt.responseDecoder,
        listFormat: opt.listFormat,
      ),
    ),
  );
}

_onRequest(RequestOptions options, RequestInterceptorHandler handler) {
  info(
    '\n${_logFormat(options.method.toUpperCase(), options.baseUrl + options.path)}\n' +
        _logFormat('token', get<IServiceToken>().token) + "\n" +
        _logFormat('data', options.data) +
        _logFormat('queryParameters', options.queryParameters) +
        _logFormat('extra', options.extra) +
        _logFormat('headers', options.headers) +
        _logFormat('responseType', options.responseType) +
        _logFormat('contentType', options.contentType) +
        _logFormat('validateStatus', options.validateStatus),
  );
  handler.next(options);
}

_onResponse(Response response, ResponseInterceptorHandler handler) {
  try {
    info(
      '\nRESPONSE:\n\n' +
          const JsonEncoder.withIndent('  ').convert(json.decode(response.toString())) +
          '\n',
    );
  } catch (e) {
    error(e);
    info(response);
  }
  handler.next(response);
}

_onError(DioError dioError, ErrorInterceptorHandler handler) {
  error(dioError.message, dioError.error, dioError.stackTrace);
  handler.next(dioError);
}

_logFormat(String header, Object? field) => "$header: $field\n";
