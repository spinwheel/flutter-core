import 'package:get/get.dart';

/// *
/// A wrapper on [Bindings] to be referenced instead of directly referencing GET package

abstract class BaseBindings extends Bindings {
  BaseBindings() {
    dependencies();
  }

  bool isInitialized = false;

  init();

  @override
  dependencies() {
    if (!isInitialized) {
      isInitialized = true;
      return init();
    }
  }
}
