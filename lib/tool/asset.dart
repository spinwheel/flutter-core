import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:sw_core/tool/log.dart';

Future<Map<String, dynamic>> loadJson(String? name, {String path = 'assets/'}) async {
  if (name != null) {
    WidgetsFlutterBinding.ensureInitialized();
    try {
      final decoded = jsonDecode(await rootBundle.loadString('$path$name.json'));
      info("Decoded JSON file $path$name:\n\n\n$decoded");
      return decoded;
    } catch (e) {
      error("Unable to load asset: $path$name.json\n\n\n$e\n");
    }
  }
  return {};
}
