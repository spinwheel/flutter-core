//region ENDPOINTS
const base_SW_URL = 'https://swsl-api.spinwheel.io';
const base_SW_URL_SANDBOX = 'https://sandbox-swsl-api.spinwheel.io';
const login = '/v1/users/auth';
const calculator = '/v1/impacts/calculator';
const user = '/v1/users/';
const servicer = '/v1/md/loanServicers/';
const polled_login = '/v1/pollingTasks/';
//endregion

//region PATHs
const servicerID = 'servicerID';
const userID = 'userID';
const transactions = '/transactions';
//endregion

//region HEADERS
const xAuthUser = 'x-auth-user';
//endregion

//region QUERIES
const extUserId = 'extUserId';
//endregion
