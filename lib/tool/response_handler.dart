import 'package:dio/dio.dart';

import 'log.dart';

mixin ResponseHandler {
  handle<T>({
    required Future<T> Function() fetch,
    required Function(T t) onValue,
    Function(Object error)? onError,
  }) async {
    try {
      onValue(await fetch());
    } catch (error) {
      onResponseError(error);
      onError?.call(error);
    }
  }

  onResponseError(Object? object) {
    switch (object?.runtimeType) {
      case DioError:
        final errorResponse = (object as DioError).response;
        error("ERROR:\n\n ${errorResponse?.statusCode} -> ${errorResponse?.statusMessage}\n\n");
        break;
      default:
    }
  }
}
