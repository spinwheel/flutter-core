import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:injector/injector.dart';

/// The main purpose of these functions is to wrap and funnel down any and all calls to Injector
/// so that any maintenance, replacement or interception on these calls is centered here on CORE
/// instead of spread out amongst other packages.
///
/// This also allows us to have CORE handle more dependencies, thus any dependant package will
/// have less dependencies to handle on their own, simplifying and centralizing the whole process.
///

final _injector = Injector.appInstance;

T put<T>(T Function() any) {
  if (!exists<T>()) {
    _injector.registerSingleton<T>(any);
  }
  return get<T>();
}

T replace<T>(T Function() any) {
  if (exists<T>()) {
    delete<T>();
  }
  return put<T>(any);
}

T? getSafe<T>({T? Function()? onError}) {
  try {
    return get<T>();
  } catch (injectorError) {
    return onError?.call();
  }
}

T get<T>() => _injector.get<T>();

bool exists<T>() => _injector.exists<T>();

void create<T>(T Function() any) => _injector.register<T>(Factory.provider(any));
void delete<T>() => _injector.removeByKey<T>();
void clearAll() => _injector.clearAll();

extension StringExtension on String {
  /// Returns a `RxString` with [this] `String` as initial value.
  RxString get obs => RxString(this);
}

extension IntExtension on int {
  /// Returns a `RxInt` with [this] `int` as initial value.
  RxInt get obs => RxInt(this);
}

extension DoubleExtension on double {
  /// Returns a `RxDouble` with [this] `double` as initial value.
  RxDouble get obs => RxDouble(this);
}

extension BoolExtension on bool {
  /// Returns a `RxBool` with [this] `bool` as initial value.
  RxBool get obs => RxBool(this);
}

extension RxT<T> on T {
  /// Returns a `Rx` instance with [this] `T` as initial value.
  Rx<T> get obs => Rx<T>(this);
}

GlobalKey<NavigatorState> get navKey => Get.key;
