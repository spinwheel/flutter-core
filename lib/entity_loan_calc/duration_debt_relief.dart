class DurationDebtRelief {
  num? _years;
  num? _months;
  num? _days;
  num? _hours;
  String? _print;

  num? get years => _years;

  num? get months => _months;

  num? get days => _days;

  num? get hours => _hours;

  String? get print => _print;

  DurationDebtRelief({num? years, num? months, num? days, num? hours, String? print}) {
    _years = years;
    _months = months;
    _days = days;
    _hours = hours;
    _print = print;
  }

  DurationDebtRelief.fromJson(dynamic json) {
    _years = json['years'];
    _months = json['months'];
    _days = json['days'];
    _hours = json['hours'];
    _print = json['print'];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['years'] = _years;
    map['months'] = _months;
    map['days'] = _days;
    map['hours'] = _hours;
    map['print'] = _print;
    return map;
  }
}
