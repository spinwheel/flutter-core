class MonthlyBurndown {
  String? _date;
  num? _interestRate;
  num? _monthlyInstallment;
  num? _principalReduction;
  num? _interest;
  num? _remainingPrincipal;

  String? get date => _date;

  num? get interestRate => _interestRate;

  num? get monthlyInstallment => _monthlyInstallment;

  num? get principalReduction => _principalReduction;

  num? get interest => _interest;

  num? get remainingPrincipal => _remainingPrincipal;

  MonthlyBurndown(
      {String? date,
      num? interestRate,
      num? monthlyInstallment,
      num? principalReduction,
      num? interest,
      num? remainingPrincipal}) {
    _date = date;
    _interestRate = interestRate;
    _monthlyInstallment = monthlyInstallment;
    _principalReduction = principalReduction;
    _interest = interest;
    _remainingPrincipal = remainingPrincipal;
  }

  MonthlyBurndown.fromJson(dynamic json) {
    _date = json['date'];
    _interestRate = json['interestRate'];
    _monthlyInstallment = json['monthlyInstallment'];
    _principalReduction = json['principalReduction'];
    _interest = json['interest'];
    _remainingPrincipal = json['remainingPrincipal'];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['date'] = _date;
    map['interestRate'] = _interestRate;
    map['monthlyInstallment'] = _monthlyInstallment;
    map['principalReduction'] = _principalReduction;
    map['interest'] = _interest;
    map['remainingPrincipal'] = _remainingPrincipal;
    return map;
  }
}
