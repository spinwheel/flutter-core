import 'duration_debt_relief.dart';
import 'monthly_burndown.dart';

class LoanCalculated {
  num? _monthlyPayment;
  num? _monthlyPaymentRound;
  num? _principalSavings;
  num? _principalSavingsRound;
  num? _interestSavings;
  num? _interestSavingsRound;
  num? _totalSavings;
  num? _totalSavingsRound;
  num? _totalSavingsPercentage;
  DurationDebtRelief? _durationDebtRelief;
  num? _daysDebtFree;
  num? _originalMonthlyPayment;
  num? _originalMonthlyPaymentRound;
  num? _originalTotalInterestAmount;
  num? _originalTotalInterestAmountRound;
  num? _originalTotalPaymentAmount;
  num? _originalTotalPaymentAmountRound;
  num? _originalPaymentDays;
  List<MonthlyBurndown>? _monthlyBurndown;

  num? get monthlyPayment => _monthlyPayment;

  num? get monthlyPaymentRound => _monthlyPaymentRound;

  num? get principalSavings => _principalSavings;

  num? get principalSavingsRound => _principalSavingsRound;

  num? get interestSavings => _interestSavings;

  num? get interestSavingsRound => _interestSavingsRound;

  num get totalSavings => _totalSavings ?? 0;

  num? get totalSavingsRound => _totalSavingsRound;

  num? get totalSavingsPercentage => _totalSavingsPercentage;

  DurationDebtRelief? get durationDebtRelief => _durationDebtRelief;

  num get daysDebtFree => _daysDebtFree != null && _daysDebtFree! > 0 ? _daysDebtFree! : 1;

  num get monthsDebtFree => daysDebtFree / 30;

  num get yearsDebtFree => daysDebtFree / 365;

  bool get debtFreeAsMonth => monthsDebtFree < 18;

  num get debtFree => debtFreeAsMonth ? monthsDebtFree : yearsDebtFree;

  num? get originalMonthlyPayment => _originalMonthlyPayment;

  num? get originalMonthlyPaymentRound => _originalMonthlyPaymentRound;

  double get originalTotalInterestAmount => _originalTotalInterestAmount?.toDouble() ?? 1;

  num? get originalTotalInterestAmountRound => _originalTotalInterestAmountRound;

  double get originalTotalPaymentAmount => _originalTotalPaymentAmount?.toDouble() ?? 1;

  double get originalLoanBalance => originalTotalPaymentAmount - originalTotalInterestAmount;

  double get originalTotalPaymentAmountRound =>
      _originalTotalPaymentAmountRound?.toDouble() ?? 1;

  num? get originalPaymentDays => _originalPaymentDays;

  List<MonthlyBurndown>? get monthlyBurndown => _monthlyBurndown;

  LoanCalculated(
      {num? monthlyPayment,
      num? monthlyPaymentRound,
      num? principalSavings,
      num? principalSavingsRound,
      num? interestSavings,
      num? interestSavingsRound,
      num? totalSavings,
      num? totalSavingsRound,
      num? totalSavingsPercentage,
      DurationDebtRelief? durationDebtRelief,
      num? daysDebtFree,
      num? originalMonthlyPayment,
      num? originalMonthlyPaymentRound,
      num? originalTotalInterestAmount,
      num? originalTotalInterestAmountRound,
      num? originalTotalPaymentAmount,
      num? originalTotalPaymentAmountRound,
      num? originalPaymentDays,
      List<MonthlyBurndown>? monthlyBurndown}) {
    _monthlyPayment = monthlyPayment;
    _monthlyPaymentRound = monthlyPaymentRound;
    _principalSavings = principalSavings;
    _principalSavingsRound = principalSavingsRound;
    _interestSavings = interestSavings;
    _interestSavingsRound = interestSavingsRound;
    _totalSavings = totalSavings;
    _totalSavingsRound = totalSavingsRound;
    _totalSavingsPercentage = totalSavingsPercentage;
    _durationDebtRelief = durationDebtRelief;
    _daysDebtFree = daysDebtFree;
    _originalMonthlyPayment = originalMonthlyPayment;
    _originalMonthlyPaymentRound = originalMonthlyPaymentRound;
    _originalTotalInterestAmount = originalTotalInterestAmount;
    _originalTotalInterestAmountRound = originalTotalInterestAmountRound;
    _originalTotalPaymentAmount = originalTotalPaymentAmount;
    _originalTotalPaymentAmountRound = originalTotalPaymentAmountRound;
    _originalPaymentDays = originalPaymentDays;
    _monthlyBurndown = monthlyBurndown;
  }

  LoanCalculated.fromJson(dynamic json) {
    _monthlyPayment = json['monthlyPayment'];
    _monthlyPaymentRound = json['monthlyPaymentRound'];
    _principalSavings = json['principalSavings'];
    _principalSavingsRound = json['principalSavingsRound'];
    _interestSavings = json['interestSavings'];
    _interestSavingsRound = json['interestSavingsRound'];
    _totalSavings = json['totalSavings'];
    _totalSavingsRound = json['totalSavingsRound'];
    _totalSavingsPercentage = json['totalSavingsPercentage'];
    _durationDebtRelief = json['durationDebtRelief'] != null
        ? DurationDebtRelief.fromJson(json['durationDebtRelief'])
        : null;
    _daysDebtFree = json['daysDebtFree'];
    _originalMonthlyPayment = json['originalMonthlyPayment'];
    _originalMonthlyPaymentRound = json['originalMonthlyPaymentRound'];
    _originalTotalInterestAmount = json['originalTotalInterestAmount'];
    _originalTotalInterestAmountRound = json['originalTotalInterestAmountRound'];
    _originalTotalPaymentAmount = json['originalTotalPaymentAmount'];
    _originalTotalPaymentAmountRound = json['originalTotalPaymentAmountRound'];
    _originalPaymentDays = json['originalPaymentDays'];
    if (json['monthlyBurndown'] != null) {
      _monthlyBurndown = [];
      json['monthlyBurndown'].forEach((v) {
        _monthlyBurndown?.add(MonthlyBurndown.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['monthlyPayment'] = _monthlyPayment;
    map['monthlyPaymentRound'] = _monthlyPaymentRound;
    map['principalSavings'] = _principalSavings;
    map['principalSavingsRound'] = _principalSavingsRound;
    map['interestSavings'] = _interestSavings;
    map['interestSavingsRound'] = _interestSavingsRound;
    map['totalSavings'] = _totalSavings;
    map['totalSavingsRound'] = _totalSavingsRound;
    map['totalSavingsPercentage'] = _totalSavingsPercentage;
    if (_durationDebtRelief != null) {
      map['durationDebtRelief'] = _durationDebtRelief?.toJson();
    }
    map['daysDebtFree'] = _daysDebtFree;
    map['originalMonthlyPayment'] = _originalMonthlyPayment;
    map['originalMonthlyPaymentRound'] = _originalMonthlyPaymentRound;
    map['originalTotalInterestAmount'] = _originalTotalInterestAmount;
    map['originalTotalInterestAmountRound'] = _originalTotalInterestAmountRound;
    map['originalTotalPaymentAmount'] = _originalTotalPaymentAmount;
    map['originalTotalPaymentAmountRound'] = _originalTotalPaymentAmountRound;
    map['originalPaymentDays'] = _originalPaymentDays;
    if (_monthlyBurndown != null) {
      map['monthlyBurndown'] = _monthlyBurndown?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}
