import 'package:sw_core/tool/data_path.dart';
import 'package:sw_core/tool/log.dart';

import '../tool/inject.dart';

/// Enumeration to identify if the app is running using a
/// development environment or a production one
enum SWEnvironment { DEV, PROD, HOM }

SWEnvironment get _env {
  final environment = get<SWEnvironment>();
  wtf("Current environment:\n $environment");
  return environment;
}

extension SWEnv on SWEnvironment {
  bool get isDev => _env == SWEnvironment.DEV;

  bool get isProd => _env == SWEnvironment.PROD;

  bool get isHom => _env == SWEnvironment.HOM;

  String get url {
    switch (_env) {
      case SWEnvironment.PROD:
        return base_SW_URL;
      case SWEnvironment.HOM:
        return base_SW_URL_SANDBOX;
      case SWEnvironment.DEV:
      default:
        return base_SW_URL_SANDBOX;
    }
  }

  static void onEnvironment(
    Function onDev,
    Function onProd,
    Function onHom,
  ) {
    switch (_env) {
      case SWEnvironment.DEV:
        onDev.call();
        break;
      case SWEnvironment.PROD:
        onProd.call();
        break;
      case SWEnvironment.HOM:
        onHom.call();
        break;
    }
  }
}
