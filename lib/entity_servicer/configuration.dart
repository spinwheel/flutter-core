import 'payment.dart';
import 'payoff.dart';

class Configuration {
  Payment? _payment;
  Payoff? _payoff;
  bool? _isLoanForgivenessAvailable;

  Payment? get payment => _payment;

  Payoff? get payoff => _payoff;

  bool? get isLoanForgivenessAvailable => _isLoanForgivenessAvailable;

  Configuration({Payment? payment, Payoff? payoff, bool? isLoanForgivenessAvailable}) {
    _payment = payment;
    _payoff = payoff;
    _isLoanForgivenessAvailable = isLoanForgivenessAvailable;
  }

  Configuration.fromJson(dynamic json) {
    _payment = json['payment'] != null ? Payment.fromJson(json['payment']) : null;
    _payoff = json['payoff'] != null ? Payoff.fromJson(json['payoff']) : null;
    _isLoanForgivenessAvailable = json['isLoanForgivenessAvailable'];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (_payment != null) {
      map['payment'] = _payment?.toJson();
    }
    if (_payoff != null) {
      map['payoff'] = _payoff?.toJson();
    }
    map['isLoanForgivenessAvailable'] = _isLoanForgivenessAvailable;
    return map;
  }
}
