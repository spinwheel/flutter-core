class AuthParams {
  String? _id;
  String? _label;
  String? _type;
  String? _group;

  String? get id => _id;
  String? get label => _label;
  String? get type => _type;
  String get group => _group ?? '';

  AuthParams({String? id, String? label, String? type, String? group}) {
    _id = id;
    _label = label;
    _type = type;
    _group = group;
  }

  AuthParams.fromJson(dynamic json) {
    _id = json['id'];
    _label = json['label'];
    _type = json['type'];
    _group = json['group'];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['id'] = _id;
    map['label'] = _label;
    map['type'] = _type;
    map['group'] = _group;
    return map;
  }
}
