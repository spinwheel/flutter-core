class TaskId {
  TaskId({
    String? taskId,
    String? id,
    String? type,
    String? partnerId,
    String? loanServicerId,
    int? startTs,
    String? status,
    int? createdOn,
    int? updatedOn,
  });

  String? _taskId;
  String? _id;
  String? _type;
  String? _partnerId;
  String? _loanServicerId;
  int? _startTs;
  String? _status;
  int? _createdOn;
  int? _updatedOn;

  TaskId.fromJson(Map<String, dynamic> json) {
    _taskId = json['taskId'];
    _id = json['_id'];
    _type = json['type'];
    _partnerId = json['partnerId'];
    _loanServicerId = json['loanServicerId'];
    _startTs = json['startTs'];
    _status = json['status'];
    _createdOn = json['createdOn'];
    _updatedOn = json['updatedOn'];
  }

  String? get taskId => _taskId;

  Map<String, dynamic> toJson() {
    Map<String, dynamic> map = {};
    map['taskId'] = _taskId;
    map['_id'] = _id;
    map['type'] = type;
    map['partnerId'] = _partnerId;
    map['loanServicerId'] - _loanServicerId;
    map['startTs'] = _startTs;
    map['status'] = _status;
    map['createdOn'] = _createdOn;
    map['updatedOn'] = _updatedOn;
    return map;
  }

  int? get updatedOn => _updatedOn;

  int? get createdOn => _createdOn;

  String? get status => _status;

  int? get startTs => _startTs;

  String? get loanServicerId => _loanServicerId;

  String? get partnerId => _partnerId;

  String? get type => _type;

  String? get id => _id;


}
