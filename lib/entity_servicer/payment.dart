class Payment {
  num? _minAmount;
  num? _transactionFee;
  String? _accountPaySupport;
  String? _groupPaySupport;
  String? _precisionPaySupport;
  String? _advanceDueDateSupport;

  num? get minAmount => _minAmount;

  num? get transactionFee => _transactionFee;

  String? get accountPaySupport => _accountPaySupport;

  String? get groupPaySupport => _groupPaySupport;

  String? get precisionPaySupport => _precisionPaySupport;

  String? get advanceDueDateSupport => _advanceDueDateSupport;

  Payment(
      {num? minAmount,
      num? transactionFee,
      String? accountPaySupport,
      String? groupPaySupport,
      String? precisionPaySupport,
      String? advanceDueDateSupport}) {
    _minAmount = minAmount;
    _transactionFee = transactionFee;
    _accountPaySupport = accountPaySupport;
    _groupPaySupport = groupPaySupport;
    _precisionPaySupport = precisionPaySupport;
    _advanceDueDateSupport = advanceDueDateSupport;
  }

  Payment.fromJson(dynamic json) {
    _minAmount = json['minAmount'];
    _transactionFee = json['transactionFee'];
    _accountPaySupport = json['accountPaySupport'];
    _groupPaySupport = json['groupPaySupport'];
    _precisionPaySupport = json['precisionPaySupport'];
    _advanceDueDateSupport = json['advanceDueDateSupport'];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['minAmount'] = _minAmount;
    map['transactionFee'] = _transactionFee;
    map['accountPaySupport'] = _accountPaySupport;
    map['groupPaySupport'] = _groupPaySupport;
    map['precisionPaySupport'] = _precisionPaySupport;
    map['advanceDueDateSupport'] = _advanceDueDateSupport;
    return map;
  }
}
