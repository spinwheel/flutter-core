class ServicerAddress {
  String? _identifier;
  String? _address;

  String? get identifier => _identifier;
  String? get address => _address;

  ServicerAddress({String? identifier, String? address}) {
    _identifier = identifier;
    _address = address;
  }

  ServicerAddress.fromJson(dynamic json) {
    _identifier = json['identifier'];
    _address = json['address'];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['identifier'] = _identifier;
    map['address'] = _address;
    return map;
  }
}
