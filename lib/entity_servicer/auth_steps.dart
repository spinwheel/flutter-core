import 'auth_params.dart';

class AuthSteps {
  String? _stepName;
  List<AuthParams>? _authParams;

  String? get stepName => _stepName;
  List<AuthParams> get authParams => _authParams ?? [];

  AuthSteps({String? stepName, List<AuthParams>? authParams}) {
    _stepName = stepName;
    _authParams = authParams;
  }

  AuthSteps.fromJson(dynamic json) {
    _stepName = json['stepName'];
    if (json['authParams'] != null) {
      _authParams = [];
      json['authParams'].forEach((v) {
        _authParams?.add(AuthParams.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['stepName'] = _stepName;
    if (_authParams != null) {
      map['authParams'] = _authParams?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}
