import 'auth_steps.dart';
import 'configuration.dart';
import 'servicer_address.dart';

class Servicer {
  bool? _isIntegrated;
  dynamic _servicerAddressType;
  dynamic _liabilityType;
  String? _processorName;
  String? _id;
  List<AuthSteps>? _authSteps;
  String? _forgotPasswordUrl;
  String? _logoUrl;
  String? _loginUrl;
  String? _name;
  Configuration? _configuration;
  num? _rating;
  List<ServicerAddress>? _servicerAddress;

  bool get isIntegrated => _isIntegrated ?? false;

  dynamic get servicerAddressType => _servicerAddressType;

  dynamic get liabilityType => _liabilityType;

  String? get processorName => _processorName;

  String? get id => _id;

  List<AuthSteps> get authSteps => _authSteps ?? [];

  String get forgotPasswordUrl => _forgotPasswordUrl ?? '';

  String get logoUrl => _logoUrl ?? 'logo_spinwheel.png';

  String? get loginUrl => _loginUrl;

  String get name => _name ?? "";

  Configuration? get configuration => _configuration;

  num? get rating => _rating;

  List<ServicerAddress>? get servicerAddress => _servicerAddress;

  Servicer(
      {bool? isIntegrated,
      String? servicerAddressType,
      List<String>? liabilityType,
      String? processorName,
      String? id,
      List<AuthSteps>? authSteps,
      String? forgotPasswordUrl,
      String? logoUrl,
      String? loginUrl,
      String? name,
      Configuration? configuration,
      num? rating,
      List<ServicerAddress>? servicerAddress}) {
    _isIntegrated = isIntegrated;
    _servicerAddressType = servicerAddressType;
    _liabilityType = liabilityType;
    _processorName = processorName;
    _id = id;
    _authSteps = authSteps;
    _forgotPasswordUrl = forgotPasswordUrl;
    _logoUrl = logoUrl;
    _loginUrl = loginUrl;
    _name = name;
    _configuration = configuration;
    _rating = rating;
    _servicerAddress = servicerAddress;
  }

  Servicer.fromJson(dynamic json) {
    _isIntegrated = json['isIntegrated'];
    _servicerAddressType = json['servicerAddressType'];
    _liabilityType = json['liabilityType'];
    _processorName = json['processorName'];
    _id = json['_id'];
    if (json['authSteps'] != null) {
      _authSteps = [];
      json['authSteps'].forEach((v) {
        _authSteps?.add(AuthSteps.fromJson(v));
      });
    }
    _forgotPasswordUrl = json['forgotPasswordUrl'];
    _logoUrl = json['logoUrl'];
    _loginUrl = json['loginUrl'];
    _name = json['name'];
    _configuration =
        json['configuration'] != null ? Configuration.fromJson(json['configuration']) : null;
    _rating = json['rating'];
    if (json['servicerAddress'] != null) {
      _servicerAddress = [];
      json['servicerAddress'].forEach((v) {
        _servicerAddress?.add(ServicerAddress.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['isIntegrated'] = _isIntegrated;
    map['servicerAddressType'] = _servicerAddressType;
    map['liabilityType'] = _liabilityType;
    map['processorName'] = _processorName;
    map['_id'] = _id;
    if (_authSteps != null) {
      map['authSteps'] = _authSteps?.map((v) => v.toJson()).toList();
    }
    map['forgotPasswordUrl'] = _forgotPasswordUrl;
    map['logoUrl'] = _logoUrl;
    map['loginUrl'] = _loginUrl;
    map['name'] = _name;
    if (_configuration != null) {
      map['configuration'] = _configuration?.toJson();
    }
    map['rating'] = _rating;
    if (_servicerAddress != null) {
      map['servicerAddress'] = _servicerAddress?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}
