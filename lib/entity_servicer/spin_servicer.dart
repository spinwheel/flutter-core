// ignore_for_file: non_constant_identifier_names

class SpinServicer {
  static final NAVIENT_LOANS = "559a17a3-1e98-4429-808e-a393b759eb46";
  static final AIDVANTAGE = "2e8f52ce-d950-4410-aab1-d02698ccf46b";
  static final GREAT_LAKES = "4bc688c9-ba51-4028-baae-c51fff07044e";
  static final GRANITE_STATE_MANAGEMENT_E_RESOURCES = "aa095033-f975-44a5-b99f-6c3abb8ddcdd";
  static final FIRSTMARK_SERVICES = "e32be248-d43f-4dd4-9e34-05c7b8ab1888";
  static final FEDLOAN_SERVICING = "097ac8fa-6b11-49c6-8889-6dec25f83f55";
  static final HEARTLAND_ECSI = "2ca12f1e-2632-40d9-9bcc-82cde093fdf9";
  static final OKLAHOMA_STUDENT_LOAN_AUTHORITY_DIRECT_LOAN = "358eb528-6c0f-40f6-b91d-31142acc1785";
  static final CORNERSTONE_EDUCATION_LOAN_SERVICES = "dfd3532e-b8de-4658-affb-0e39b75cdfd6";
  static final NELNET_EDUCATION_FINANCING = "bb3e39e7-9dd6-461e-a4f4-c95a1a1e80ef";
  static final EARNEST = "129acf96-2ee4-4f25-a054-ba1df200e17c";
  static final EDFINANCIAL_SERVICES_FEDERAL_DIRECT_LOAN_PROGRAM =
      "3139117c-6496-4afe-8688-a2efe27e6ab4";
  static final MOHELA_EDUCATION_LOAN_FINANCE = "c1c06f2a-6d0d-4b27-b6ad-198c9bfeddbd";
  static final MOHELA_SOFI_SERVICING = "603bbecc-86f9-4c53-9c0e-dac5a667cc02";
  static final AMERICAN_EDUCATION_SERVICES = "9422554f-0170-4afe-b8df-12aaecc3fdef";
  static final WELLS_FARGO = "1a6182ae-da97-43dd-baf6-3d64fffaf0f8";
  static final UNIVERSITY_ACCOUNTING_SERVICE_LLC = "8238add6-b7d0-11eb-8529-0242ac130003";
  static final COMMONBOND_FIRSTMARK_SERVICES = "6bb42b3a-7bde-4841-8d40-4d5393e0194c";
  static final SALLIE_MAE = "09945206-21fc-4e8b-9779-be328c0926d5";
  static final SUNTRUST = "e35a6454-ca66-4b35-a7e7-afb25d03c424";
  static final VERMONT_STUDENT_ASSISTANCE_CORPORATION = "149acf96-2ee4-4f25-a054-ba1df200e17c";
  static final SOFI = "139acf96-2ee4-4f25-a054-ba1df200e17c";
  static final PNC = "51f92e9d-a4b4-480a-a393-00849e1c4d15";
  static final NAVY_FEDERAL_CREDIT_UNION = "7d234707-f2f6-4299-97cc-8f4b2e258394";
  static final LENDKEY = "6daee756-fc64-4494-890f-d427fd648416";
  static final MOHELA_LAUREL_ROAD = "ef4f460f-cdf8-4ad5-a235-ec8873ec1c6b";
  static final COLLEGE_FOUNDATION_OF_NORTH_CAROLINA = "a8f031b0-246f-4fd1-8ff7-2451cbe4e0f7";
  static final NAVIREFI = "1b4d6919-6ffe-4da5-bbb6-98853cbe412c";
  static final STUB_LOAN_SERVICER_SINGLE_STEP = "77a549e1-d67d-45f2-94a4-e3ef7041af92";
  static final STUB_LOAN_SERVICER_ADDITION_INFO = "ca3e3b3c-602a-4c33-890f-8af6911bd3f4";
  static final STUB_LOAN_SERVICER_MULTI_STEP = "f0f3de8c-9a9c-4d0c-8416-e71a97b81ebc";
}
