class Payoff {
  bool? _isIntegrated;
  num? _maxPayoffDateLimitInDays;

  bool? get isIntegrated => _isIntegrated;

  num? get maxPayoffDateLimitInDays => _maxPayoffDateLimitInDays;

  Payoff({bool? isIntegrated, num? maxPayoffDateLimitInDays}) {
    _isIntegrated = isIntegrated;
    _maxPayoffDateLimitInDays = maxPayoffDateLimitInDays;
  }

  Payoff.fromJson(dynamic json) {
    _isIntegrated = json['isIntegrated'];
    _maxPayoffDateLimitInDays = json['maxPayoffDateLimitInDays'];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['isIntegrated'] = _isIntegrated;
    map['maxPayoffDateLimitInDays'] = _maxPayoffDateLimitInDays;
    return map;
  }
}
