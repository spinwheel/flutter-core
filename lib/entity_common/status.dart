class Status {
  dynamic _code;
  dynamic _desc;

  dynamic get code => _code is num ? _code : _code.toString();

  String get desc => _desc is String ? _desc : _desc.toString();

  Status({dynamic code, dynamic desc}) {
    _code = code;
    _desc = desc;
  }

  Status.fromJson(dynamic json) {
    _code = json['code'];
    _desc = json['desc'];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['code'] = _code;
    map['desc'] = _desc;
    return map;
  }
}
