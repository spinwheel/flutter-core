class RequestAuthToken {
  RequestAuthToken({
    String? endpoint,
    String? extUserId,
    String? userId,
    int? tokenExpiryInSec,
  }) {
    _endpoint = endpoint;
    _extUserId = extUserId;
    _userId = userId;
    _tokenExpiryInSec =
        tokenExpiryInSec == null || tokenExpiryInSec < 600 ? 3600 : tokenExpiryInSec;
  }

  RequestAuthToken.pos([
    String? endpoint,
    String? extUserId,
    String? userId,
    int? tokenExpiryInSec,
  ]) {
    _endpoint = endpoint;
    _extUserId = extUserId;
    _userId = userId;
    _tokenExpiryInSec =
        tokenExpiryInSec == null || tokenExpiryInSec < 600 ? 3600 : tokenExpiryInSec;
  }

  RequestAuthToken.fromJson(dynamic json) {
    _endpoint = json['endpoint'];
    _extUserId = json['extUserId'];
    _userId = json['userId'];
    _tokenExpiryInSec = json['tokenExpiryInSec'];
  }

  String? _endpoint;
  String? _extUserId;
  String? _userId;
  int? _tokenExpiryInSec;

  String get endpoint => _endpoint ?? 'http://localhost:8081/';

  String? get extUserId => _extUserId;

  String? get userId => _userId;

  int get tokenExpiryInSec => _tokenExpiryInSec ?? 3600;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['endpoint'] = _endpoint;
    map['extUserId'] = _extUserId;
    map['userId'] = _userId;
    map['tokenExpiryInSec'] = _tokenExpiryInSec;
    return map;
  }
}
