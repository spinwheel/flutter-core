import 'package:sw_core/entity_auth/user_pass.dart';

class RequestAuthPasswordStep {
  RequestAuthPasswordStep({
      UserPass? loginDetails,
      String? authStep, 
      String? servicerId,}){
    _loginDetails = loginDetails;
    _authStep = authStep;
    _servicerId = servicerId;
}

  RequestAuthPasswordStep.fromJson(dynamic json) {
    _loginDetails = json['loginDetails'] != null ? UserPass.fromJson(json['loginDetails']) : null;
    _authStep = json['authStep'];
    _servicerId = json['servicerId'];
  }
  UserPass? _loginDetails;
  String? _authStep;
  String? _servicerId;

  UserPass? get loginDetails => _loginDetails;
  String? get authStep => _authStep;
  String? get servicerId => _servicerId;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_loginDetails != null) {
      map['loginDetails'] = _loginDetails?.toJson();
    }
    map['authStep'] = _authStep;
    map['servicerId'] = _servicerId;
    return map;
  }

}