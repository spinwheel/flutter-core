import 'package:sw_core/entity_auth/communication_details.dart';
import 'package:sw_core/entity_auth/user_pass.dart';

class RequestAuth {
  RequestAuth({
    CommunicationDetails? email,
    UserPass? loginDetails,
    String? loanServicerName,
    int? time,
    String? liabilityType,
    String? loanServicerId,
    String? extUserID,
    String? authStep,
  }) {
    _email = email;
    _loginDetails = loginDetails;
    _loanServicerName = loanServicerName;
    _time = time;
    _liabilityType = liabilityType;
    _loanServicerId = loanServicerId;
    _extUserId = extUserID;
    _authStep = authStep;
  }

  RequestAuth.fromJson(dynamic json) {
    _email = json['email'] != null ? CommunicationDetails.fromJson(json['email']) : null;
    _loginDetails = json['loginDetails'] != null ? UserPass.fromJson(json['loginDetails']) : null;
    _loanServicerName = json['loanServicerName'];
    _time = json['time'];
    _liabilityType = json['liabilityType'];
    _loanServicerId = json['loanServicerId'];
    _extUserId = json['extUserId'];
    _authStep = json['authStep'];
  }
  CommunicationDetails? _email;
  UserPass? _loginDetails;
  String? _loanServicerName;
  int? _time;
  String? _liabilityType;
  String? _loanServicerId;
  String? _extUserId;
  String? _authStep;

  CommunicationDetails? get email => _email;
  UserPass? get loginDetails => _loginDetails;
  String? get loanServicerName => _loanServicerName;
  int? get time => _time;
  String? get liabilityType => _liabilityType;
  String? get loanServicerId => _loanServicerId;
  String? get extUserId => _extUserId;
  String? get authStep => _authStep;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_email != null) {
      map['communicationdetails'] = _email?.toJson();
    }
    if (_loginDetails != null) {
      map['loginDetails'] = _loginDetails?.toJson();
    }
    map['loanServicerName'] = _loanServicerName;
    map['time'] = _time;
    map['liabilityType'] = _liabilityType;
    map['loanServicerId'] = _loanServicerId;
    map['extUserId'] = _extUserId;
    map['authStep'] = _authStep;
    return map;
  }
}
