class RequestLoanCalc {
  num? _loanBalance;
  num? _interestRate;
  num? _monthlyPayment;
  num? _extraPayment;
  bool? _isRecurring;

  num? get loanBalance => _loanBalance;

  num? get interestRate => _interestRate;

  num? get monthlyPayment => _monthlyPayment;

  num? get extraPayment => _extraPayment;

  bool? get isRecurring => _isRecurring;

  RequestLoanCalc(
      {num? loanBalance,
      num? interestRate,
      num? monthlyPayment,
      num? extraPayment,
      bool? isRecurring}) {
    _loanBalance = loanBalance;
    _interestRate = interestRate;
    _monthlyPayment = monthlyPayment;
    _extraPayment = extraPayment;
    _isRecurring = isRecurring;
  }

  RequestLoanCalc.fromJson(dynamic json) {
    _loanBalance = json['loanBalance'];
    _interestRate = json['interestRate'];
    _monthlyPayment = json['monthlyPayment'];
    _extraPayment = json['extraPayment'];
    _isRecurring = json['isRecurring'];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['loanBalance'] = _loanBalance;
    map['interestRate'] = _interestRate;
    map['monthlyPayment'] = _monthlyPayment;
    map['extraPayment'] = _extraPayment;
    map['isRecurring'] = _isRecurring;
    return map;
  }
}
