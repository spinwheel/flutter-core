class RequestAuthRefreshToken {
  RequestAuthRefreshToken({
    String? refreshToken,
  }) {
    _refreshToken = refreshToken;
  }

  RequestAuthRefreshToken.fromJson(dynamic json) {
    _refreshToken = json['refreshToken'];
  }

  String? _refreshToken;

  String? get refreshToken => _refreshToken;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['refreshToken'] = _refreshToken;
    return map;
  }
}
