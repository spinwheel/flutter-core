import 'package:sw_core/entity_auth/auth_step_params.dart';
import 'package:sw_core/entity_auth/login_security_details.dart';

class RequestAuthAddSteps {
  RequestAuthAddSteps({
    String? loanServicerName,
    String? userId,
    String? authStep,
    AuthStepParams? authStepParams,
    String? loanServicerId,
    LoginSecurityDetails? loginDetails,
    int? time,
    String? liabilityType,
  }) {
    _loanServicerName = loanServicerName;
    _userId = userId;
    _authStep = authStep;
    _authStepParams = authStepParams;
    _loanServicerId = loanServicerId;
    _loginDetails = loginDetails;
    _time = time;
    _liabilityType = liabilityType;
  }

  RequestAuthAddSteps.fromJson(dynamic json) {
    _loanServicerName = json['loanServicerName'];
    _userId = json['userId'];
    _authStep = json['authStep'];
    _authStepParams =
        json['authStepParams'] != null ? AuthStepParams.fromJson(json['authStepParams']) : null;
    _loanServicerId = json['loanServicerId'];
    _loginDetails =
        json['loginDetails'] != null ? LoginSecurityDetails.fromJson(json['loginDetails']) : null;
    _time = json['time'];
    _liabilityType = json['liabilityType'];
  }

  String? _loanServicerName;
  String? _userId;
  String? _authStep;
  AuthStepParams? _authStepParams;
  String? _loanServicerId;
  LoginSecurityDetails? _loginDetails;
  int? _time;
  String? _liabilityType;

  String? get loanServicerName => _loanServicerName;
  String? get userId => _userId;
  String? get authStep => _authStep;
  AuthStepParams? get authStepParams => _authStepParams;
  String? get loanServicerId => _loanServicerId;
  LoginSecurityDetails? get loginDetails => _loginDetails;
  int? get time => _time;
  String? get liabilityType => _liabilityType;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['loanServicerName'] = _loanServicerName;
    map['userId'] = _userId;
    map['authStep'] = _authStep;
    if (_authStepParams != null) {
      map['authStepParams'] = _authStepParams?.toJson();
    }
    map['loanServicerId'] = _loanServicerId;
    if (_loginDetails != null) {
      map['loginDetails'] = _loginDetails?.toJson();
    }
    map['time'] = _time;
    map['liabilityType'] = _liabilityType;
    return map;
  }
}
