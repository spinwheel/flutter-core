import 'package:sw_core/entity_auth/auth_step_params.dart';
import 'package:sw_core/entity_auth/login_security_details.dart';

class RequestAuthSecurityStep {
  RequestAuthSecurityStep({
    String? userId,
    String? authStep,
    AuthStepParams? authStepParams,
    String? loanServicerId,
    LoginSecurityDetails? loginDetails,
  }) {
    _userId = userId;
    _authStep = authStep;
    _authStepParams = authStepParams;
    _loanServicerId = loanServicerId;
    _loginDetails = loginDetails;
  }

  RequestAuthSecurityStep.fromJson(dynamic json) {
    _userId = json['userId'];
    _authStep = json['authStep'];
    _authStepParams =
        json['authStepParams'] != null ? AuthStepParams.fromJson(json['authStepParams']) : null;
    _loanServicerId = json['loanServicerId'];
    _loginDetails =
        json['loginDetails'] != null ? LoginSecurityDetails.fromJson(json['loginDetails']) : null;
  }

  String? _userId;
  String? _authStep;
  AuthStepParams? _authStepParams;
  String? _loanServicerId;
  LoginSecurityDetails? _loginDetails;

  String? get userId => _userId;

  String? get authStep => _authStep;

  AuthStepParams? get authStepParams => _authStepParams;

  String? get loanServicerId => _loanServicerId;

  LoginSecurityDetails? get loginDetails => _loginDetails;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['userId'] = _userId;
    map['authStep'] = _authStep;
    if (_authStepParams != null) {
      map['authStepParams'] = _authStepParams?.toJson();
    }
    map['loanServicerId'] = _loanServicerId;
    if (_loginDetails != null) {
      map['loginDetails'] = _loginDetails?.toJson();
    }
    return map;
  }
}
