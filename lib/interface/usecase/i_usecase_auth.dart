import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:sw_core/entity_auth/password_step.dart';
import 'package:sw_core/entity_user/unintegrated_user.dart';
import 'package:sw_core/entity_user/user.dart';
import 'package:sw_core/request/request_auth.dart';
import 'package:sw_core/request/request_auth_add_steps.dart';

abstract class IUseCaseAuth {
  abstract Rx<AuthStep> authStep;
  abstract Rx<User> user;
  abstract Rx<SecurityStep> security;
  abstract Rx<Object> error;
  abstract RxBool isPolledTask;
  abstract Rx<UnintegratedUser> unintegratedUser;

  requestAuth(RequestAuth requestAuth);

  requestAuthAddSteps(RequestAuthAddSteps requestAuth);

  getUser(String extUserID);
}

enum AuthStep { STEP_UNAUTHORIZED, STEP_SECURITY, STEP_AUTHORIZED, AUTH_ERROR }
