import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:sw_core/entity_servicer/servicer.dart';

abstract class IUseCaseAllServicers {
  abstract RxList<Servicer> allServicers;
  abstract Rx<Servicer> servicer;
  Future<Servicer> getServicer(String servicerID);
  Future<List<Servicer>> getAllServicers();
}
