abstract class IUseCasePolledAuth {
  abstract final authStep;
  abstract final user;
  abstract final error;
  abstract final isPolling;

  Future requestPolledAuth(String taskId);
}
