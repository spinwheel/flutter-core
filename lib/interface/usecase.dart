abstract class UseCase<T> {
  abstract final observable;
  Future<T> execute([dynamic object]);
  Future<T> update(Future<T> future) {
    future.then((value) => observable.value = value);
    return future;
  }
}
