abstract class IPartnerAuth {
  abstract final String authEndpoint;
  abstract final String extUserID;
  abstract final String userID;
}
