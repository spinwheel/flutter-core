
abstract class IAnalytics {
  void track(String eventName, {Map<String, dynamic>? properties});
}