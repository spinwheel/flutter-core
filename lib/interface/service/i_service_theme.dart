
import 'package:flutter/material.dart';
import 'package:sw_core/entity_theme/response_theme.dart';

abstract class IServiceTheme {

  abstract SWTheme _lightTheme;
  abstract SWTheme _darkTheme;

  ThemeData get theme;

  ThemeData get darkTheme;

  ColorScheme get colorSchemeLight;

  ColorScheme get colorSchemeDark;

  _buildThemeData(SWTheme swTheme, ColorScheme colorScheme, {TextTheme? textTheme});

  _buildColorScheme(SWTheme swTheme, Brightness brightness);

}