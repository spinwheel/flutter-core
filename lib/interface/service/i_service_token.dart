import 'package:get/get_rx/get_rx.dart';
import 'package:sw_core/request/request_auth_token.dart';

abstract class IServiceToken {
  bool get handleToken => true;
  abstract RxString token;
  abstract String refreshToken;
  abstract String baseURL;
  abstract RequestAuthToken? requestToken;
  String clientId = '';
  getAuthToken();
  refreshAuthToken();
}
