import 'i_partner_auth.dart';

class PartnerAuth extends IPartnerAuth {
  PartnerAuth({
    required this.authEndpoint,
    required this.extUserID,
    required this.userID,
  });

  @override
  final String authEndpoint;

  @override
  final String extUserID;

  @override
  final String userID;
}
