import 'package:sw_core/entity_common/status.dart';
import 'package:sw_core/entity_user/user.dart';

class ResponseAuthUser {
  ResponseAuthUser({
      Status? status, 
      User? data,}){
    _status = status;
    _data = data;
}

  ResponseAuthUser.fromJson(dynamic json) {
    _status = json['status'] != null ? Status.fromJson(json['status']) : null;
    _data = json['data'] != null ? User.fromJson(json['data']) : null;
  }
  Status? _status;
  User? _data;

  Status? get status => _status;
  User? get data => _data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_status != null) {
      map['status'] = _status?.toJson();
    }
    if (_data != null) {
      map['data'] = _data?.toJson();
    }
    return map;
  }

}