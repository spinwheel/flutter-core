import 'package:sw_core/entity_common/status.dart';
import 'package:sw_core/entity_loan_transaction/loan_transaction.dart';

class ResponseTransactions {
  Status? _status;
  List<LoanTransaction>? _loanTransactions;

  Status? get status => _status;

  List<LoanTransaction>? get loanTransactions => _loanTransactions;

  ResponseTransactions({Status? status, List<LoanTransaction>? data}) {
    _status = status;
    _loanTransactions = data;
  }

  ResponseTransactions.fromJson(dynamic json) {
    _status = json['status'] != null ? Status.fromJson(json['status']) : null;
    if (json['data'] != null) {
      _loanTransactions = [];
      json['data'].forEach((v) {
        _loanTransactions?.add(LoanTransaction.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (_status != null) {
      map['status'] = _status?.toJson();
    }
    if (_loanTransactions != null) {
      map['data'] = _loanTransactions?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}
