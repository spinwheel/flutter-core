import 'package:sw_core/entity_common/status.dart';
import 'package:sw_core/entity_servicer/servicer.dart';

class ResponseServicer {
  Status? _status;
  Servicer? _servicer;

  Status? get status => _status;

  Servicer get servicer => _servicer ?? Servicer();

  ResponseServicer({Status? status, Servicer? data}) {
    _status = status;
    _servicer = data;
  }

  ResponseServicer.fromJson(dynamic json) {
    _status = json['status'] != null ? Status.fromJson(json['status']) : null;
    _servicer = json['data'] != null ? Servicer.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (_status != null) {
      map['status'] = _status?.toJson();
    }
    if (_servicer != null) {
      map['data'] = _servicer?.toJson();
    }
    return map;
  }
}
