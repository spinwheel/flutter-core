import 'package:sw_core/entity_auth/token_data.dart';
import 'package:sw_core/entity_common/status.dart';

class ResponseAuthToken {
  ResponseAuthToken({
    Status? status,
    TokenData? tokenData,
  }) {
    _status = status;
    _tokenData = tokenData;
  }

  ResponseAuthToken.fromJson(dynamic json) {
    _status = json['status'] != null ? Status.fromJson(json['status']) : null;
    _tokenData = json['data'] != null ? TokenData.fromJson(json['data']) : null;
  }

  Status? _status;
  TokenData? _tokenData;

  Status? get status => _status;

  TokenData get tokenData => _tokenData ?? TokenData();

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_status != null) {
      map['status'] = _status?.toJson();
    }
    if (_tokenData != null) {
      map['data'] = _tokenData?.toJson();
    }
    return map;
  }
}
