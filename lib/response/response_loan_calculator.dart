import 'package:sw_core/entity_common/status.dart';
import 'package:sw_core/entity_loan_calc/loan_calculated.dart';

class ResponseLoanCalculator {
  Status? _status;
  LoanCalculated? _loanCalc;

  Status? get status => _status;

  LoanCalculated? get loanCalc => _loanCalc;

  ResponseLoanCalculator({Status? status, LoanCalculated? data}) {
    _status = status;
    _loanCalc = data;
  }

  ResponseLoanCalculator.fromJson(dynamic json) {
    _status = json['status'] != null ? Status.fromJson(json['status']) : null;
    _loanCalc = json['data'] != null ? LoanCalculated.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (_status != null) {
      map['status'] = _status?.toJson();
    }
    if (_loanCalc != null) {
      map['data'] = _loanCalc?.toJson();
    }
    return map;
  }
}
