import 'package:sw_core/entity_common/status.dart';
import 'package:sw_core/entity_servicer/servicer.dart';

class ResponseAllServicers {
  Status? _status;
  List<Servicer>? _allServicers;

  Status? get status => _status;

  List<Servicer> get allServicers => _allServicers ?? [];

  ResponseAllServicers({Status? status, List<Servicer>? data}) {
    _status = status;
    _allServicers = data;
  }

  ResponseAllServicers.fromJson(dynamic json) {
    _status = json['status'] != null ? Status.fromJson(json['status']) : null;
    if (json['data'] != null) {
      _allServicers = [];
      json['data'].forEach((v) {
        _allServicers?.add(Servicer.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (_status != null) {
      map['status'] = _status?.toJson();
    }
    if (_allServicers != null) {
      map['data'] = _allServicers?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}
