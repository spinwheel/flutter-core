import 'package:sw_core/entity_common/status.dart';
import 'package:sw_core/entity_auth/password_step.dart';

class ResponseAuthPasswordStep {
  ResponseAuthPasswordStep({
    Status? status,
    SecurityStep? data,
  }) {
    _status = status;
    _passwordStep = data;
  }

  ResponseAuthPasswordStep.fromJson(dynamic json) {
    _status = json['status'] != null ? Status.fromJson(json['status']) : null;
    _passwordStep = json['data'] != null ? SecurityStep.fromJson(json['data']) : null;
  }

  Status? _status;
  SecurityStep? _passwordStep;

  Status? get status => _status;

  SecurityStep? get passwordStep => _passwordStep;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_status != null) {
      map['status'] = _status?.toJson();
    }
    if (_passwordStep != null) {
      map['data'] = _passwordStep?.toJson();
    }
    return map;
  }
}
