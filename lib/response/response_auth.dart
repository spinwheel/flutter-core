import 'package:sw_core/entity_auth/password_step.dart';
import 'package:sw_core/entity_common/status.dart';
import 'package:sw_core/entity_servicer/taskId.dart';
import 'package:sw_core/entity_user/user.dart';
import 'package:sw_core/entity_auth/error.dart';

class ResponseAuth {
  ResponseAuth({
    Status? status,
    SecurityStep? securityStep,
    User? user,
    TaskId? taskId,
    Error? error,
  }) {
    _status = status;
    _user = user;
    _security = securityStep;
    _taskId = taskId;
    _error = error;
  }

  ResponseAuth.fromJson(dynamic json) {
    // _status = json['status'] != null ? Status.fromJson(json['status']) : null;
    _user = json['data']['response'] != null
        ? User.fromJson(json['data']['response'])
        : json['data'] != null
            ? User.fromJson(json['data'])
            : null;
    _security = json['data'] != null ? SecurityStep.fromJson(json['data']) : null;
    _taskId = json['data'] != null ? TaskId.fromJson(json['data']) : null;
    _error = json['data']['error'] != null ? Error.fromJson(json['data']['error']) : null;
  }

  Status? _status;
  User? _user;
  SecurityStep? _security;
  TaskId? _taskId;
  Error? _error;

  Error? get error => _error == null ? null : _error;

  TaskId? get taskId => _taskId == null ? null : _taskId;

  Status get status => _status ?? Status(code: -1, desc: 'Unknown Error');

  User? get user =>
      _user == null || _user?.isChampion == null || _user?.id == null || _user?.partnerId == null
          ? null
          : _user;

  SecurityStep? get security =>
      _security == null || _security?.securityDetails == null || _security?.userId == null
          ? null
          : _security;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_status != null) {
      map['status'] = _status?.toJson();
    }
    if (_user != null) {
      map['data'] = _user?.toJson();
    }
    if (_taskId != null) {
      map['data'] = taskId?.toJson();
    }
    return map;
  }
}
