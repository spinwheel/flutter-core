class ParamsLoanPal {
  String name;
  String transactionHistoryPath;
  String loanListNavigationText;
  String extraPayNavigationText;
  String roundUpNavigationText;
  String oneTimePayNavigationText;
  String collaboratorNavigationPath;
  String collaboratorNavigationText;

  bool hideTransactionHistoryNavigation;
  bool hideLoanListNavigation;
  bool hideExtraPayNavigation;
  bool hideRoundUpNavigation;
  bool showOneTimePayNavigation;
  bool hideCollaboratorNavigation;
  bool isNested;

  ParamsLoanPal({
    this.name = "",
    this.transactionHistoryPath = "",
    this.loanListNavigationText = "",
    this.extraPayNavigationText = "Extra Pay",
    this.roundUpNavigationText = "Round-Ups",
    this.oneTimePayNavigationText = "Make Payment",
    this.collaboratorNavigationPath = "",
    this.collaboratorNavigationText = "Community Pay",
    this.hideTransactionHistoryNavigation = false,
    this.hideLoanListNavigation = false,
    this.hideExtraPayNavigation = false,
    this.hideRoundUpNavigation = false,
    this.showOneTimePayNavigation = false,
    this.hideCollaboratorNavigation = false,
    this.isNested = true,
  });
}