import 'package:mixpanel_flutter/mixpanel_flutter.dart';
import 'package:sw_core/environment/sw_env.dart';

import '../interface/i_analytics.dart';
import '../tool/inject.dart';

const _sand = '0f39e35caa5482a0561a682618fa8a54';
const _prod = '7fe2700b39a268128c42afab4534610c';

String get _token => getSafe<SWEnvironment>()?.isProd == true ? _prod : _sand;

class Analytics implements IAnalytics {
  late Mixpanel _mixpanel;

  Analytics({bool optOutTrackingDefault = false, Map<String, dynamic>? superProperties}) {
    Mixpanel.init(
      _token,
      optOutTrackingDefault: optOutTrackingDefault,
      superProperties: superProperties,
    ).then((value) {
      _mixpanel = value;
      replace<IAnalytics>(() => this);
    });
  }

  @override
  void track(String eventName, {Map<String, dynamic>? properties}) async {
    _mixpanel.track(eventName, properties: properties);
  }
}
