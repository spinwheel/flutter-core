class AnalyticsPage {
  static const String PAGE_LOAN_SERVICERS_LOGIN = "LoanServicersLogin";
  static const String PAGE_LOAN_SERVICERS_LOGIN_HOME_PAGE = "Home Page";
}

class AnalyticsDesc {
  static const String DESC_SERVICER_LIST = "ServicerList";
  static const String DESC_SERVICER_CARD = "Servicer Card";
  static const String DESC_PRIVACY = "Privacy";
  static const String DESC_END_USER_PRIVACY_POLICY = "End User Privacy Policy";
  static const String DESC_CONTINUE = "Continue";
  static const String DESC_SERVICER_LOGIN = "ServicerLogin";
  static const String DESC_ADD_ANOTHER_ACCOUNT = "AddAnotherAccount";
  static const String DESC_CONNECT_LOAN = "Connect Loan";
  static const String DESC_CONNECT_LOAN_BUTTON = "Connect Loan Button";
  static const String DESC_CONNECT_PASSWORD = "Forgot Password";
  static const String DESC_DIM = "Dim";
}

class AnalyticsAct {
  static const String ACT_LOAN_SERVICERS_PAGE = "Page";
  static const String ACT_LOAN_SERVICERS_CLICKED = "Clicked";
  static const String ACT_LOAN_SERVICERS_SEARCHED = "Searched";
  static const String ACT_LOAN_SERVICERS_SUCCESS = "Success";
  static const String ACT_LOAN_SERVICERS_FAILED = "Failed";
  static const String ACT_LOAN_SERVICERS_LOADED = "Loaded";
}
