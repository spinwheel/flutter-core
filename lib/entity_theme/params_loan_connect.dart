import 'package:flutter/material.dart';
import 'package:sw_core/entity_theme/enum/transition.dart';
import 'package:sw_core/interface/layout.dart';
import 'package:sw_core/tool/inject.dart';

import '../environment/sw_env.dart';

class ParamsLoanConnect {
  ParamsLoanConnect({
    required String partnerName,
    bool isNested = false,
    bool hasExitButton = true,
    Layout layout = Layout.DEFAULT,
    Widget Function(BuildContext)? build,
    SpinTransition? pageTransition,
    Widget? logo,
    Widget? servicerNotFoundIcon,
    Widget? title,
    Widget? iconText0,
    Widget? iconText1,
    Widget? linkIcon,
    Widget? disclaimerText,
    Widget? navButton,
    bool skipLandingPage = false,
    String? skipToServicerID,
    SWEnvironment? swEnv,
  })  : this.partnerName = partnerName,
        this.isNested = isNested,
        this.hasExitButton = hasExitButton,
        this.layout = layout,
        this.build = build,
        this.pageTransition = pageTransition ?? SpinTransition.native,
        this.logo = logo ?? _alert,
        this.servicerNotFoundIcon = servicerNotFoundIcon ?? _alert,
        this.title = title,
        this.iconText0 = iconText0,
        this.iconText1 = iconText1,
        this.linkIcon = linkIcon,
        this.disclaimerText = disclaimerText,
        this.navButton = navButton,
        this.skipToServicerID = skipToServicerID,
        this.skipLandingPage = skipLandingPage,
        this.swEnv = replace<SWEnvironment>(() => swEnv ?? SWEnvironment.DEV);

  String partnerName;
  bool isNested;
  bool hasExitButton;
  Layout layout;
  Widget Function(BuildContext)? build;
  SpinTransition pageTransition;
  Widget logo;
  Widget servicerNotFoundIcon;
  Widget? title;
  Widget? iconText0;
  Widget? iconText1;
  Widget? linkIcon;
  Widget? disclaimerText;
  Widget? navButton;
  bool skipLandingPage;
  String? skipToServicerID;
  SWEnvironment swEnv;
}

Widget get _alert => const Icon(Icons.report_problem_outlined, color: Colors.purple);
