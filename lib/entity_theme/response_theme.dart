import 'package:sw_core/entity_common/status.dart';
import 'package:sw_core/tool/asset.dart';
import 'package:sw_core/tool/log.dart';

class ResponseTheme {
  ResponseTheme({
    Status? status,
    SWTheme? swTheme,
  }) {
    _status = status;
    _swTheme = swTheme;
  }

  ResponseTheme.fromJson(dynamic json) {
    _status = json['status'] != null ? Status.fromJson(json['status']) : null;
    _swTheme = json['data'] != null ? SWTheme.fromJson(json['data']) : null;
  }

  Status? _status;
  SWTheme? _swTheme;

  Status? get status => _status;

  SWTheme get swTheme => _swTheme ?? SWTheme();

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_status != null) {
      map['status'] = _status?.toJson();
    }
    if (_swTheme != null) {
      map['data'] = _swTheme?.toJson();
    }
    return map;
  }
}

const RED = "FF0000";

class SWTheme {
  SWTheme({
    String? name,
    Style? style,
    List<DimTemplates>? dimTemplates,
    DropinConfig? dropinConfig,
  }) {
    _name = name;
    _style = style;
    _dimTemplates = dimTemplates;
    _dropinConfig = dropinConfig;
  }

  SWTheme.fromJson(dynamic json) {
    _name = json['name'];
    _style = json['style'] != null ? Style.fromJson(json['style']) : null;
    if (json['dimTemplates'] != null) {
      _dimTemplates = [];
      json['dimTemplates'].forEach((v) {
        _dimTemplates?.add(DimTemplates.fromJson(v));
      });
    }
    _dropinConfig =
    json['dropinConfig'] != null ? DropinConfig.fromJson(json['dropinConfig']) : null;
  }

  String? _name;
  Style? _style;
  List<DimTemplates>? _dimTemplates;
  DropinConfig? _dropinConfig;

  String? get name => _name;

  Style get style => _style ?? Style();

  List<DimTemplates>? get dimTemplates => _dimTemplates;

  DropinConfig? get dropinConfig => _dropinConfig;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['name'] = _name;
    if (_style != null) {
      map['style'] = _style?.toJson();
    }
    if (_dimTemplates != null) {
      map['dimTemplates'] = _dimTemplates?.map((v) => v.toJson()).toList();
    }
    if (_dropinConfig != null) {
      map['dropinConfig'] = _dropinConfig?.toJson();
    }
    return map;
  }
}

class DropinConfig {
  DropinConfig({
    String? imageUrl,
    String? transparentImageUrl,
  }) {
    _imageUrl = imageUrl;
    _transparentImageUrl = transparentImageUrl;
  }

  DropinConfig.fromJson(dynamic json) {
    _imageUrl = json['imageUrl'];
    _transparentImageUrl = json['transparentImageUrl'];
  }

  String? _imageUrl;
  String? _transparentImageUrl;

  String? get imageUrl => _imageUrl;

  String? get transparentImageUrl => _transparentImageUrl;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['imageUrl'] = _imageUrl;
    map['transparentImageUrl'] = _transparentImageUrl;
    return map;
  }
}

class DimTemplates {
  DimTemplates({
    List<Dims>? dims,
    String? name,
  }) {
    _dims = dims;
    _name = name;
  }

  DimTemplates.fromJson(dynamic json) {
    if (json['dims'] != null) {
      _dims = [];
      json['dims'].forEach((v) {
        _dims?.add(Dims.fromJson(v));
      });
    }
    _name = json['name'];
  }

  List<Dims>? _dims;
  String? _name;

  List<Dims>? get dims => _dims;

  String? get name => _name;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_dims != null) {
      map['dims'] = _dims?.map((v) => v.toJson()).toList();
    }
    map['name'] = _name;
    return map;
  }
}

class Dims {
  Dims({
    bool? showHeader,
    bool? showConnectLoanText,
    String? championName,
    String? landingUrl,
    String? updateCredentials,
    String? loanServicerId,
    String? loanAccountId,
    String? platformBenefitText1,
    String? platformBenefitText2,
    String? platformBenefitText3,
    String? primaryColor3,
    String? transparentImageUrl,
    String? module,
    String? moduleId,
  }) {
    _showHeader = showHeader;
    _showConnectLoanText = showConnectLoanText;
    _championName = championName;
    _landingUrl = landingUrl;
    _updateCredentials = updateCredentials;
    _loanServicerId = loanServicerId;
    _loanAccountId = loanAccountId;
    _platformBenefitText1 = platformBenefitText1;
    _platformBenefitText2 = platformBenefitText2;
    _platformBenefitText3 = platformBenefitText3;
    _primaryColor3 = primaryColor3;
    _transparentImageUrl = transparentImageUrl;
    _module = module;
    _moduleId = moduleId;
  }

  Dims.fromJson(dynamic json) {
    _showHeader = json['showHeader'];
    _showConnectLoanText = json['showConnectLoanText'];
    _championName = json['championName'];
    _landingUrl = json['landingUrl'];
    _updateCredentials = json['updateCredentials'];
    _loanServicerId = json['loanServicerId'];
    _loanAccountId = json['loanAccountId'];
    _platformBenefitText1 = json['platformBenefitText1'];
    _platformBenefitText2 = json['platformBenefitText2'];
    _platformBenefitText3 = json['platformBenefitText3'];
    _primaryColor3 = json['primary-color3'];
    _transparentImageUrl = json['transparentImageUrl'];
    _module = json['module'];
    _moduleId = json['moduleId'];
  }

  bool? _showHeader;
  bool? _showConnectLoanText;
  String? _championName;
  String? _landingUrl;
  String? _updateCredentials;
  String? _loanServicerId;
  String? _loanAccountId;
  String? _platformBenefitText1;
  String? _platformBenefitText2;
  String? _platformBenefitText3;
  String? _primaryColor3;
  String? _transparentImageUrl;
  String? _module;
  String? _moduleId;

  bool? get showHeader => _showHeader;

  bool? get showConnectLoanText => _showConnectLoanText;

  String? get championName => _championName;

  String? get landingUrl => _landingUrl;

  String? get updateCredentials => _updateCredentials;

  String? get loanServicerId => _loanServicerId;

  String? get loanAccountId => _loanAccountId;

  String? get platformBenefitText1 => _platformBenefitText1;

  String? get platformBenefitText2 => _platformBenefitText2;

  String? get platformBenefitText3 => _platformBenefitText3;

  String? get primaryColor3 => _primaryColor3;

  String? get transparentImageUrl => _transparentImageUrl;

  String? get module => _module;

  String? get moduleId => _moduleId;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['showHeader'] = _showHeader;
    map['showConnectLoanText'] = _showConnectLoanText;
    map['championName'] = _championName;
    map['landingUrl'] = _landingUrl;
    map['updateCredentials'] = _updateCredentials;
    map['loanServicerId'] = _loanServicerId;
    map['loanAccountId'] = _loanAccountId;
    map['platformBenefitText1'] = _platformBenefitText1;
    map['platformBenefitText2'] = _platformBenefitText2;
    map['platformBenefitText3'] = _platformBenefitText3;
    map['primary-color3'] = _primaryColor3;
    map['transparentImageUrl'] = _transparentImageUrl;
    map['module'] = _module;
    map['moduleId'] = _moduleId;
    return map;
  }
}

class Style {
  Style({
    String? primaryColor1,
    String? primaryColor2,
    String? primaryColor3,
    String? secondaryColor1,
    String? secondaryColor2,
    String? secondaryColor3,
    String? backgroundColor,
    String? fontFamily,
    int? fontSize,
    Anchor? anchor,
    Box? box,
    Button? button,
    Card? card,
    Divider? divider,
    DotLoader? dotLoader,
    FormControl? formControl,
    Header? header,
    Icons? icons,
    InputGroup? inputGroup,
    InputSlider? inputSlider,
    Label? label,
    PieChart? pieChart,
    ProgressBar? progressBar,
    Svgs? svgs,
    Text? text,
    Toggle? toggle,
    Tooltip? tooltip,
  }) {
    _primaryColor1 = primaryColor1;
    _primaryColor2 = primaryColor2;
    _primaryColor3 = primaryColor3;
    _secondaryColor1 = secondaryColor1;
    _secondaryColor2 = secondaryColor2;
    _secondaryColor3 = secondaryColor3;
    _backgroundColor = backgroundColor;
    _fontFamily = fontFamily;
    _fontSize = fontSize;
    _anchor = anchor;
    _box = box;
    _button = button;
    _card = card;
    _divider = divider;
    _dotLoader = dotLoader;
    _formControl = formControl;
    _header = header;
    _icons = icons;
    _inputGroup = inputGroup;
    _inputSlider = inputSlider;
    _label = label;
    _pieChart = pieChart;
    _progressBar = progressBar;
    _svgs = svgs;
    _text = text;
    _toggle = toggle;
    _tooltip = tooltip;
  }

  Style.fromJson(dynamic json) {
    _primaryColor1 = json['primary-color1'];
    _primaryColor2 = json['primary-color2'];
    _primaryColor3 = json['primary-color3'];
    _secondaryColor1 = json['secondary-color1'];
    _secondaryColor2 = json['secondary-color2'];
    _secondaryColor3 = json['secondary-color3'];
    _backgroundColor = json['background-color'];
    _fontFamily = json['font-family'];
    _fontSize = json['font-size'];
    _anchor = json['anchor'] != null ? Anchor.fromJson(json['anchor']) : null;
    _box = json['box'] != null ? Box.fromJson(json['box']) : null;
    _button = json['button'] != null ? Button.fromJson(json['button']) : null;
    _card = json['card'] != null ? Card.fromJson(json['card']) : null;
    _divider = json['divider'] != null ? Divider.fromJson(json['divider']) : null;
    _dotLoader = json['dotLoader'] != null ? DotLoader.fromJson(json['dotLoader']) : null;
    _formControl = json['formControl'] != null ? FormControl.fromJson(json['formControl']) : null;
    _header = json['header'] != null ? Header.fromJson(json['header']) : null;
    _icons = json['icons'] != null ? Icons.fromJson(json['icons']) : null;
    _inputGroup = json['inputGroup'] != null ? InputGroup.fromJson(json['inputGroup']) : null;
    _inputSlider = json['inputSlider'] != null ? InputSlider.fromJson(json['inputSlider']) : null;
    _label = json['label'] != null ? Label.fromJson(json['label']) : null;
    _pieChart = json['pieChart'] != null ? PieChart.fromJson(json['pieChart']) : null;
    _progressBar = json['progressBar'] != null ? ProgressBar.fromJson(json['progressBar']) : null;
    _svgs = json['svgs'] != null ? Svgs.fromJson(json['svgs']) : null;
    _text = json['text'] != null ? Text.fromJson(json['text']) : null;
    _toggle = json['toggle'] != null ? Toggle.fromJson(json['toggle']) : null;
    _tooltip = json['tooltip'] != null ? Tooltip.fromJson(json['tooltip']) : null;
  }

  String? _primaryColor1;
  String? _primaryColor2;
  String? _primaryColor3;
  String? _secondaryColor1;
  String? _secondaryColor2;
  String? _secondaryColor3;
  String? _backgroundColor;
  String? _fontFamily;
  int? _fontSize;
  Anchor? _anchor;
  Box? _box;
  Button? _button;
  Card? _card;
  Divider? _divider;
  DotLoader? _dotLoader;
  FormControl? _formControl;
  Header? _header;
  Icons? _icons;
  InputGroup? _inputGroup;
  InputSlider? _inputSlider;
  Label? _label;
  PieChart? _pieChart;
  ProgressBar? _progressBar;
  Svgs? _svgs;
  Text? _text;
  Toggle? _toggle;
  Tooltip? _tooltip;

  String get primaryColor1 => _primaryColor1 ?? RED;

  String get primaryColor2 => _primaryColor2 ?? RED;

  String get primaryColor3 => _primaryColor3 ?? RED;

  String get secondaryColor1 => _secondaryColor1 ?? RED;

  String get secondaryColor2 => _secondaryColor2 ?? RED;

  String get secondaryColor3 => _secondaryColor3 ?? RED;

  String get backgroundColor => _backgroundColor ?? RED;

  String? get fontFamily => _fontFamily;

  int? get fontSize => _fontSize;

  Anchor? get anchor => _anchor;

  Box? get box => _box;

  Button? get button => _button;

  Card get card => _card ?? Card();

  Divider? get divider => _divider;

  DotLoader? get dotLoader => _dotLoader;

  FormControl? get formControl => _formControl;

  Header? get header => _header;

  Icons? get icons => _icons;

  InputGroup? get inputGroup => _inputGroup;

  InputSlider? get inputSlider => _inputSlider;

  Label? get label => _label;

  PieChart? get pieChart => _pieChart;

  ProgressBar? get progressBar => _progressBar;

  Svgs? get svgs => _svgs;

  Text? get text => _text;

  Toggle? get toggle => _toggle;

  Tooltip? get tooltip => _tooltip;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['primary-color1'] = _primaryColor1;
    map['primary-color2'] = _primaryColor2;
    map['primary-color3'] = _primaryColor3;
    map['secondary-color1'] = _secondaryColor1;
    map['secondary-color2'] = _secondaryColor2;
    map['secondary-color3'] = _secondaryColor3;
    map['background-color'] = _backgroundColor;
    map['font-family'] = _fontFamily;
    map['font-size'] = _fontSize;
    if (_anchor != null) {
      map['anchor'] = _anchor?.toJson();
    }
    if (_box != null) {
      map['box'] = _box?.toJson();
    }
    if (_button != null) {
      map['button'] = _button?.toJson();
    }
    if (_card != null) {
      map['card'] = _card?.toJson();
    }
    if (_divider != null) {
      map['divider'] = _divider?.toJson();
    }
    if (_dotLoader != null) {
      map['dotLoader'] = _dotLoader?.toJson();
    }
    if (_formControl != null) {
      map['formControl'] = _formControl?.toJson();
    }
    if (_header != null) {
      map['header'] = _header?.toJson();
    }
    if (_icons != null) {
      map['icons'] = _icons?.toJson();
    }
    if (_inputGroup != null) {
      map['inputGroup'] = _inputGroup?.toJson();
    }
    if (_inputSlider != null) {
      map['inputSlider'] = _inputSlider?.toJson();
    }
    if (_label != null) {
      map['label'] = _label?.toJson();
    }
    if (_pieChart != null) {
      map['pieChart'] = _pieChart?.toJson();
    }
    if (_progressBar != null) {
      map['progressBar'] = _progressBar?.toJson();
    }
    if (_svgs != null) {
      map['svgs'] = _svgs?.toJson();
    }
    if (_text != null) {
      map['text'] = _text?.toJson();
    }
    if (_toggle != null) {
      map['toggle'] = _toggle?.toJson();
    }
    if (_tooltip != null) {
      map['tooltip'] = _tooltip?.toJson();
    }
    return map;
  }
}

class Tooltip {
  Tooltip({
    DefaultTooltip? defaultTooltip,
  }) {
    _defaultTooltip = defaultTooltip;
  }

  Tooltip.fromJson(dynamic json) {
    _defaultTooltip =
    json['default_tooltip'] != null ? DefaultTooltip.fromJson(json['defaultTooltip']) : null;
  }

  DefaultTooltip? _defaultTooltip;

  DefaultTooltip? get defaultTooltip => _defaultTooltip;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_defaultTooltip != null) {
      map['default_tooltip'] = _defaultTooltip?.toJson();
    }
    return map;
  }
}

class DefaultTooltip {
  DefaultTooltip({
    String? placement,
    String? bgColor,
    int? bgColorOpacity,
    int? borderRadius,
    Border? border,
  }) {
    _placement = placement;
    _bgColor = bgColor;
    _bgColorOpacity = bgColorOpacity;
    _borderRadius = borderRadius;
    _border = border;
  }

  DefaultTooltip.fromJson(dynamic json) {
    _placement = json['placement'];
    _bgColor = json['bgColor'];
    _bgColorOpacity = json['bgColorOpacity'];
    _borderRadius = json['borderRadius'];
    _border = json['border'] != null ? Border.fromJson(json['border']) : null;
  }

  String? _placement;
  String? _bgColor;
  int? _bgColorOpacity;
  int? _borderRadius;
  Border? _border;

  String? get placement => _placement;

  String? get bgColor => _bgColor;

  int? get bgColorOpacity => _bgColorOpacity;

  int? get borderRadius => _borderRadius;

  Border? get border => _border;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['placement'] = _placement;
    map['bgColor'] = _bgColor;
    map['bgColorOpacity'] = _bgColorOpacity;
    map['borderRadius'] = _borderRadius;
    if (_border != null) {
      map['border'] = _border?.toJson();
    }
    return map;
  }
}

class Border {
  Border({
    String? type,
    String? color,
    int? opacity,
    int? size,
  }) {
    _type = type;
    _color = color;
    _opacity = opacity;
    _size = size;
  }

  Border.fromJson(dynamic json) {
    _type = json['type'];
    _color = json['color'];
    _opacity = json['opacity'];
    _size = json['size'];
  }

  String? _type;
  String? _color;
  int? _opacity;
  int? _size;

  String? get type => _type;

  String? get color => _color;

  int? get opacity => _opacity;

  int? get size => _size;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['type'] = _type;
    map['color'] = _color;
    map['opacity'] = _opacity;
    map['size'] = _size;
    return map;
  }
}

class Toggle {
  Toggle({
    DefaultToggle? defaultToggle,
  }) {
    _defaultToggle = defaultToggle;
  }

  Toggle.fromJson(dynamic json) {
    _defaultToggle =
    json['default_toggle'] != null ? DefaultToggle.fromJson(json['defaultToggle']) : null;
  }

  DefaultToggle? _defaultToggle;

  DefaultToggle? get defaultToggle => _defaultToggle;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_defaultToggle != null) {
      map['default_toggle'] = _defaultToggle?.toJson();
    }
    return map;
  }
}

class DefaultToggle {
  DefaultToggle({
    String? bgColor,
    int? bgColorOpacity,
    String? activeBgColor,
    int? activeBgColorOpacity,
    Title? title,
    SubTitle? subTitle,
  }) {
    _bgColor = bgColor;
    _bgColorOpacity = bgColorOpacity;
    _activeBgColor = activeBgColor;
    _activeBgColorOpacity = activeBgColorOpacity;
    _title = title;
    _subTitle = subTitle;
  }

  DefaultToggle.fromJson(dynamic json) {
    _bgColor = json['bgColor'];
    _bgColorOpacity = json['bgColorOpacity'];
    _activeBgColor = json['activeBgColor'];
    _activeBgColorOpacity = json['activeBgColorOpacity'];
    _title = json['title'] != null ? Title.fromJson(json['title']) : null;
    _subTitle = json['subTitle'] != null ? SubTitle.fromJson(json['subTitle']) : null;
  }

  String? _bgColor;
  int? _bgColorOpacity;
  String? _activeBgColor;
  int? _activeBgColorOpacity;
  Title? _title;
  SubTitle? _subTitle;

  String? get bgColor => _bgColor;

  int? get bgColorOpacity => _bgColorOpacity;

  String? get activeBgColor => _activeBgColor;

  int? get activeBgColorOpacity => _activeBgColorOpacity;

  Title? get title => _title;

  SubTitle? get subTitle => _subTitle;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['bgColor'] = _bgColor;
    map['bgColorOpacity'] = _bgColorOpacity;
    map['activeBgColor'] = _activeBgColor;
    map['activeBgColorOpacity'] = _activeBgColorOpacity;
    if (_title != null) {
      map['title'] = _title?.toJson();
    }
    if (_subTitle != null) {
      map['subTitle'] = _subTitle?.toJson();
    }
    return map;
  }
}

class SubTitle {
  SubTitle({
    String? fontFamily,
    int? fontWeight,
    String? color,
    String? activeColor,
    String? hoverColor,
    int? fontSize,
  }) {
    _fontFamily = fontFamily;
    _fontWeight = fontWeight;
    _color = color;
    _activeColor = activeColor;
    _fontSize = fontSize;
  }

  SubTitle.fromJson(dynamic json) {
    _fontFamily = json['fontFamily'];
    _fontWeight = json['fontWeight'];
    _color = json['color'];
    _activeColor = json['activeColor'];
    _hoverColor = json['hoverColor'];
    _fontSize = json['fontSize'];
  }

  String? _fontFamily;
  int? _fontWeight;
  String? _color;
  String? _activeColor;
  String? _hoverColor;
  int? _fontSize;

  String? get fontFamily => _fontFamily;

  int? get fontWeight => _fontWeight;

  String? get color => _color;

  String? get activeColor => _activeColor;

  String? get hoverColor => _hoverColor;

  int? get fontSize => _fontSize;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['fontFamily'] = _fontFamily;
    map['fontWeight'] = _fontWeight;
    map['color'] = _color;
    map['activeColor'] = _activeColor;
    map['hoverColor'] = _hoverColor;
    map['fontSize'] = _fontSize;
    return map;
  }
}

class Title {
  Title({
    String? fontFamily,
    int? fontWeight,
    String? color,
    String? activeColor,
    String? hoverColor,
    int? fontSize,
  }) {
    _fontFamily = fontFamily;
    _fontWeight = fontWeight;
    _color = color;
    _activeColor = activeColor;
    _hoverColor = hoverColor;
    _fontSize = fontSize;
  }

  Title.fromJson(dynamic json) {
    _fontFamily = json['fontFamily'];
    _fontWeight = json['fontWeight'];
    _color = json['color'];
    _activeColor = json['activeColor'];
    _hoverColor = json['hoverColor'];
    _fontSize = json['fontSize'];
  }

  String? _fontFamily;
  int? _fontWeight;
  String? _color;
  String? _activeColor;
  String? _hoverColor;
  int? _fontSize;

  String? get fontFamily => _fontFamily;

  int? get fontWeight => _fontWeight;

  String? get color => _color;

  String? get activeColor => _activeColor;

  String? get hoverColor => _hoverColor;

  int? get fontSize => _fontSize;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['fontFamily'] = _fontFamily;
    map['fontWeight'] = _fontWeight;
    map['color'] = _color;
    map['activeColor'] = _activeColor;
    map['hoverColor'] = _hoverColor;
    map['fontSize'] = _fontSize;
    return map;
  }
}

class Text {
  Text({
    LargerDark? largerDark,
    LargeDark? largeDark,
    MediumDark? mediumDark,
    MediumLight? mediumLight,
    SmallLight? smallLight,
    CardHeader? cardHeader,
    InfoBox? infoBox,
  }) {
    _largerDark = largerDark;
    _largeDark = largeDark;
    _mediumDark = mediumDark;
    _mediumLight = mediumLight;
    _smallLight = smallLight;
    _cardHeader = cardHeader;
    _infoBox = infoBox;
  }

  Text.fromJson(dynamic json) {
    _largerDark = json['largerDark'] != null ? LargerDark.fromJson(json['largerDark']) : null;
    _largeDark = json['largeDark'] != null ? LargeDark.fromJson(json['largeDark']) : null;
    _mediumDark = json['mediumDark'] != null ? MediumDark.fromJson(json['mediumDark']) : null;
    _mediumLight = json['mediumLight'] != null ? MediumLight.fromJson(json['mediumLight']) : null;
    _smallLight = json['smallLight'] != null ? SmallLight.fromJson(json['smallLight']) : null;
    _cardHeader = json['cardHeader'] != null ? CardHeader.fromJson(json['cardHeader']) : null;
    _infoBox = json['infoBox'] != null ? InfoBox.fromJson(json['infoBox']) : null;
  }

  LargerDark? _largerDark;
  LargeDark? _largeDark;
  MediumDark? _mediumDark;
  MediumLight? _mediumLight;
  SmallLight? _smallLight;
  CardHeader? _cardHeader;
  InfoBox? _infoBox;

  LargerDark? get largerDark => _largerDark;

  LargeDark? get largeDark => _largeDark;

  MediumDark? get mediumDark => _mediumDark;

  MediumLight? get mediumLight => _mediumLight;

  SmallLight? get smallLight => _smallLight;

  CardHeader? get cardHeader => _cardHeader;

  InfoBox? get infoBox => _infoBox;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_largerDark != null) {
      map['largerDark'] = _largerDark?.toJson();
    }
    if (_largeDark != null) {
      map['largeDark'] = _largeDark?.toJson();
    }
    if (_mediumDark != null) {
      map['mediumDark'] = _mediumDark?.toJson();
    }
    if (_mediumLight != null) {
      map['mediumLight'] = _mediumLight?.toJson();
    }
    if (_smallLight != null) {
      map['smallLight'] = _smallLight?.toJson();
    }
    if (_cardHeader != null) {
      map['cardHeader'] = _cardHeader?.toJson();
    }
    if (_infoBox != null) {
      map['infoBox'] = _infoBox?.toJson();
    }
    return map;
  }
}

class InfoBox {
  InfoBox({
    String? fontFamily,
    String? color,
    int? fontSize,
    int? fontWeight,
    int? opacity,
  }) {
    _fontFamily = fontFamily;
    _color = color;
    _fontSize = fontSize;
    _fontWeight = fontWeight;
    _opacity = opacity;
  }

  InfoBox.fromJson(dynamic json) {
    _fontFamily = json['fontFamily'];
    _color = json['color'];
    _fontSize = json['fontSize'];
    _fontWeight = json['fontWeight'];
    _opacity = json['opacity'];
  }

  String? _fontFamily;
  String? _color;
  int? _fontSize;
  int? _fontWeight;
  int? _opacity;

  String? get fontFamily => _fontFamily;

  String? get color => _color;

  int? get fontSize => _fontSize;

  int? get fontWeight => _fontWeight;

  int? get opacity => _opacity;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['fontFamily'] = _fontFamily;
    map['color'] = _color;
    map['fontSize'] = _fontSize;
    map['fontWeight'] = _fontWeight;
    map['opacity'] = _opacity;
    return map;
  }
}

class CardHeader {
  CardHeader({
    String? color,
    String? fontFamily,
    int? fontSize,
    int? fontWeight,
    int? opacity,
  }) {
    _color = color;
    _fontFamily = fontFamily;
    _fontSize = fontSize;
    _fontWeight = fontWeight;
    _opacity = opacity;
  }

  CardHeader.fromJson(dynamic json) {
    _color = json['color'];
    _fontFamily = json['fontFamily'];
    _fontSize = json['fontSize'];
    _fontWeight = json['fontWeight'];
    _opacity = json['opacity'];
  }

  String? _color;
  String? _fontFamily;
  int? _fontSize;
  int? _fontWeight;
  int? _opacity;

  String? get color => _color;

  String? get fontFamily => _fontFamily;

  int? get fontSize => _fontSize;

  int? get fontWeight => _fontWeight;

  int? get opacity => _opacity;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['color'] = _color;
    map['fontFamily'] = _fontFamily;
    map['fontSize'] = _fontSize;
    map['fontWeight'] = _fontWeight;
    map['opacity'] = _opacity;
    return map;
  }
}

class SmallLight {
  SmallLight({
    String? color,
    String? fontFamily,
    int? fontWeight,
    int? fontSize,
    int? opacity,
  }) {
    _color = color;
    _fontFamily = fontFamily;
    _fontWeight = fontWeight;
    _fontSize = fontSize;
    _opacity = opacity;
  }

  SmallLight.fromJson(dynamic json) {
    _color = json['color'];
    _fontFamily = json['fontFamily'];
    _fontWeight = json['fontWeight'];
    _fontSize = json['fontSize'];
    _opacity = json['opacity'];
  }

  String? _color;
  String? _fontFamily;
  int? _fontWeight;
  int? _fontSize;
  int? _opacity;

  String? get color => _color;

  String? get fontFamily => _fontFamily;

  int? get fontWeight => _fontWeight;

  int? get fontSize => _fontSize;

  int? get opacity => _opacity;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['color'] = _color;
    map['fontFamily'] = _fontFamily;
    map['fontWeight'] = _fontWeight;
    map['fontSize'] = _fontSize;
    map['opacity'] = _opacity;
    return map;
  }
}

class MediumLight {
  MediumLight({
    String? color,
    String? fontFamily,
    int? fontWeight,
    int? fontSize,
    int? opacity,
  }) {
    _color = color;
    _fontFamily = fontFamily;
    _fontWeight = fontWeight;
    _fontSize = fontSize;
    _opacity = opacity;
  }

  MediumLight.fromJson(dynamic json) {
    _color = json['color'];
    _fontFamily = json['fontFamily'];
    _fontWeight = json['fontWeight'];
    _fontSize = json['fontSize'];
    _opacity = json['opacity'];
  }

  String? _color;
  String? _fontFamily;
  int? _fontWeight;
  int? _fontSize;
  int? _opacity;

  String? get color => _color;

  String? get fontFamily => _fontFamily;

  int? get fontWeight => _fontWeight;

  int? get fontSize => _fontSize;

  int? get opacity => _opacity;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['color'] = _color;
    map['fontFamily'] = _fontFamily;
    map['fontWeight'] = _fontWeight;
    map['fontSize'] = _fontSize;
    map['opacity'] = _opacity;
    return map;
  }
}

class MediumDark {
  MediumDark({
    String? color,
    String? fontFamily,
    int? fontWeight,
    int? fontSize,
    int? opacity,
  }) {
    _color = color;
    _fontFamily = fontFamily;
    _fontWeight = fontWeight;
    _fontSize = fontSize;
    _opacity = opacity;
  }

  MediumDark.fromJson(dynamic json) {
    _color = json['color'];
    _fontFamily = json['fontFamily'];
    _fontWeight = json['fontWeight'];
    _fontSize = json['fontSize'];
    _opacity = json['opacity'];
  }

  String? _color;
  String? _fontFamily;
  int? _fontWeight;
  int? _fontSize;
  int? _opacity;

  String? get color => _color;

  String? get fontFamily => _fontFamily;

  int? get fontWeight => _fontWeight;

  int? get fontSize => _fontSize;

  int? get opacity => _opacity;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['color'] = _color;
    map['fontFamily'] = _fontFamily;
    map['fontWeight'] = _fontWeight;
    map['fontSize'] = _fontSize;
    map['opacity'] = _opacity;
    return map;
  }
}

class LargeDark {
  LargeDark({
    String? color,
    String? fontFamily,
    int? fontWeight,
    int? fontSize,
    int? opacity,
  }) {
    _color = color;
    _fontFamily = fontFamily;
    _fontWeight = fontWeight;
    _fontSize = fontSize;
    _opacity = opacity;
  }

  LargeDark.fromJson(dynamic json) {
    _color = json['color'];
    _fontFamily = json['fontFamily'];
    _fontWeight = json['fontWeight'];
    _fontSize = json['fontSize'];
    _opacity = json['opacity'];
  }

  String? _color;
  String? _fontFamily;
  int? _fontWeight;
  int? _fontSize;
  int? _opacity;

  String? get color => _color;

  String? get fontFamily => _fontFamily;

  int? get fontWeight => _fontWeight;

  int? get fontSize => _fontSize;

  int? get opacity => _opacity;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['color'] = _color;
    map['fontFamily'] = _fontFamily;
    map['fontWeight'] = _fontWeight;
    map['fontSize'] = _fontSize;
    map['opacity'] = _opacity;
    return map;
  }
}

class LargerDark {
  LargerDark({
    String? color,
    String? fontFamily,
    int? fontWeight,
    int? fontSize,
    int? opacity,
  }) {
    _color = color;
    _fontFamily = fontFamily;
    _fontWeight = fontWeight;
    _fontSize = fontSize;
    _opacity = opacity;
  }

  LargerDark.fromJson(dynamic json) {
    _color = json['color'];
    _fontFamily = json['fontFamily'];
    _fontWeight = json['fontWeight'];
    _fontSize = json['fontSize'];
    _opacity = json['opacity'];
  }

  String? _color;
  String? _fontFamily;
  int? _fontWeight;
  int? _fontSize;
  int? _opacity;

  String? get color => _color;

  String? get fontFamily => _fontFamily;

  int? get fontWeight => _fontWeight;

  int? get fontSize => _fontSize;

  int? get opacity => _opacity;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['color'] = _color;
    map['fontFamily'] = _fontFamily;
    map['fontWeight'] = _fontWeight;
    map['fontSize'] = _fontSize;
    map['opacity'] = _opacity;
    return map;
  }
}

class Svgs {
  Svgs({
    DoubleDollar? doubleDollar,
    DoubleArrow? doubleArrow,
    PayDownArrow? payDownArrow,
    Calendar? calendar,
    PayHand? payHand,
    KeepItUp? keepItUp,
    CrossClose? crossClose,
    ConnectBank? connectBank,
    ConnectingDotsLeft? connectingDotsLeft,
    ConnectingDotsRight? connectingDotsRight,
    InputCircle? inputCircle,
    InputCircleChecked? inputCircleChecked,
  }) {
    _doubleDollar = doubleDollar;
    _doubleArrow = doubleArrow;
    _payDownArrow = payDownArrow;
    _calendar = calendar;
    _payHand = payHand;
    _keepItUp = keepItUp;
    _crossClose = crossClose;
    _connectBank = connectBank;
    _connectingDotsLeft = connectingDotsLeft;
    _connectingDotsRight = connectingDotsRight;
    _inputCircle = inputCircle;
    _inputCircleChecked = inputCircleChecked;
  }

  Svgs.fromJson(dynamic json) {
    _doubleDollar =
    json['doubleDollar'] != null ? DoubleDollar.fromJson(json['doubleDollar']) : null;
    _doubleArrow = json['doubleArrow'] != null ? DoubleArrow.fromJson(json['doubleArrow']) : null;
    _payDownArrow =
    json['payDownArrow'] != null ? PayDownArrow.fromJson(json['payDownArrow']) : null;
    _calendar = json['calendar'] != null ? Calendar.fromJson(json['calendar']) : null;
    _payHand = json['payHand'] != null ? PayHand.fromJson(json['payHand']) : null;
    _keepItUp = json['keepItUp'] != null ? KeepItUp.fromJson(json['keepItUp']) : null;
    _crossClose = json['crossClose'] != null ? CrossClose.fromJson(json['crossClose']) : null;
    _connectBank = json['connectBank'] != null ? ConnectBank.fromJson(json['connectBank']) : null;
    _connectingDotsLeft = json['connectingDotsLeft'] != null
        ? ConnectingDotsLeft.fromJson(json['connectingDotsLeft'])
        : null;
    _connectingDotsRight = json['connectingDotsRight'] != null
        ? ConnectingDotsRight.fromJson(json['connectingDotsRight'])
        : null;
    _inputCircle = json['inputCircle'] != null ? InputCircle.fromJson(json['inputCircle']) : null;
    _inputCircleChecked = json['inputCircleChecked'] != null
        ? InputCircleChecked.fromJson(json['inputCircleChecked'])
        : null;
  }

  DoubleDollar? _doubleDollar;
  DoubleArrow? _doubleArrow;
  PayDownArrow? _payDownArrow;
  Calendar? _calendar;
  PayHand? _payHand;
  KeepItUp? _keepItUp;
  CrossClose? _crossClose;
  ConnectBank? _connectBank;
  ConnectingDotsLeft? _connectingDotsLeft;
  ConnectingDotsRight? _connectingDotsRight;
  InputCircle? _inputCircle;
  InputCircleChecked? _inputCircleChecked;

  DoubleDollar? get doubleDollar => _doubleDollar;

  DoubleArrow? get doubleArrow => _doubleArrow;

  PayDownArrow? get payDownArrow => _payDownArrow;

  Calendar? get calendar => _calendar;

  PayHand? get payHand => _payHand;

  KeepItUp? get keepItUp => _keepItUp;

  CrossClose? get crossClose => _crossClose;

  ConnectBank? get connectBank => _connectBank;

  ConnectingDotsLeft? get connectingDotsLeft => _connectingDotsLeft;

  ConnectingDotsRight? get connectingDotsRight => _connectingDotsRight;

  InputCircle? get inputCircle => _inputCircle;

  InputCircleChecked? get inputCircleChecked => _inputCircleChecked;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_doubleDollar != null) {
      map['doubleDollar'] = _doubleDollar?.toJson();
    }
    if (_doubleArrow != null) {
      map['doubleArrow'] = _doubleArrow?.toJson();
    }
    if (_payDownArrow != null) {
      map['payDownArrow'] = _payDownArrow?.toJson();
    }
    if (_calendar != null) {
      map['calendar'] = _calendar?.toJson();
    }
    if (_payHand != null) {
      map['payHand'] = _payHand?.toJson();
    }
    if (_keepItUp != null) {
      map['keepItUp'] = _keepItUp?.toJson();
    }
    if (_crossClose != null) {
      map['crossClose'] = _crossClose?.toJson();
    }
    if (_connectBank != null) {
      map['connectBank'] = _connectBank?.toJson();
    }
    if (_connectingDotsLeft != null) {
      map['connectingDotsLeft'] = _connectingDotsLeft?.toJson();
    }
    if (_connectingDotsRight != null) {
      map['connectingDotsRight'] = _connectingDotsRight?.toJson();
    }
    if (_inputCircle != null) {
      map['inputCircle'] = _inputCircle?.toJson();
    }
    if (_inputCircleChecked != null) {
      map['inputCircleChecked'] = _inputCircleChecked?.toJson();
    }
    return map;
  }
}

class InputCircleChecked {
  InputCircleChecked({
    String? circleColor,
    String? tickColor,
  }) {
    _circleColor = circleColor;
    _tickColor = tickColor;
  }

  InputCircleChecked.fromJson(dynamic json) {
    _circleColor = json['circleColor'];
    _tickColor = json['tickColor'];
  }

  String? _circleColor;
  String? _tickColor;

  String? get circleColor => _circleColor;

  String? get tickColor => _tickColor;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['circleColor'] = _circleColor;
    map['tickColor'] = _tickColor;
    return map;
  }
}

class InputCircle {
  InputCircle({
    String? circleColor,
  }) {
    _circleColor = circleColor;
  }

  InputCircle.fromJson(dynamic json) {
    _circleColor = json['circleColor'];
  }

  String? _circleColor;

  String? get circleColor => _circleColor;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['circleColor'] = _circleColor;
    return map;
  }
}

class ConnectingDotsRight {
  ConnectingDotsRight({
    String? firstDotColor,
    String? secondDotColor,
  }) {
    _firstDotColor = firstDotColor;
    _secondDotColor = secondDotColor;
  }

  ConnectingDotsRight.fromJson(dynamic json) {
    _firstDotColor = json['firstDotColor'];
    _secondDotColor = json['secondDotColor'];
  }

  String? _firstDotColor;
  String? _secondDotColor;

  String? get firstDotColor => _firstDotColor;

  String? get secondDotColor => _secondDotColor;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['firstDotColor'] = _firstDotColor;
    map['secondDotColor'] = _secondDotColor;
    return map;
  }
}

class ConnectingDotsLeft {
  ConnectingDotsLeft({
    String? firstDotColor,
    String? secondDotColor,
  }) {
    _firstDotColor = firstDotColor;
    _secondDotColor = secondDotColor;
  }

  ConnectingDotsLeft.fromJson(dynamic json) {
    _firstDotColor = json['firstDotColor'];
    _secondDotColor = json['secondDotColor'];
  }

  String? _firstDotColor;
  String? _secondDotColor;

  String? get firstDotColor => _firstDotColor;

  String? get secondDotColor => _secondDotColor;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['firstDotColor'] = _firstDotColor;
    map['secondDotColor'] = _secondDotColor;
    return map;
  }
}

class ConnectBank {
  ConnectBank({
    String? topColor,
    String? pillarColor,
  }) {
    _topColor = topColor;
    _pillarColor = pillarColor;
  }

  ConnectBank.fromJson(dynamic json) {
    _topColor = json['topColor'];
    _pillarColor = json['pillarColor'];
  }

  String? _topColor;
  String? _pillarColor;

  String? get topColor => _topColor;

  String? get pillarColor => _pillarColor;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['topColor'] = _topColor;
    map['pillarColor'] = _pillarColor;
    return map;
  }
}

class CrossClose {
  CrossClose({
    String? circleColor,
    String? crossColor,
  }) {
    _circleColor = circleColor;
    _crossColor = crossColor;
  }

  CrossClose.fromJson(dynamic json) {
    _circleColor = json['circleColor'];
    _crossColor = json['crossColor'];
  }

  String? _circleColor;
  String? _crossColor;

  String? get circleColor => _circleColor;

  String? get crossColor => _crossColor;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['circleColor'] = _circleColor;
    map['crossColor'] = _crossColor;
    return map;
  }
}

class KeepItUp {
  KeepItUp({
    String? color1,
    String? color2,
  }) {
    _color1 = color1;
    _color2 = color2;
  }

  KeepItUp.fromJson(dynamic json) {
    _color1 = json['color1'];
    _color2 = json['color2'];
  }

  String? _color1;
  String? _color2;

  String? get color1 => _color1;

  String? get color2 => _color2;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['color1'] = _color1;
    map['color2'] = _color2;
    return map;
  }
}

class PayHand {
  PayHand({
    String? dollarColor,
    String? handColor,
  }) {
    _dollarColor = dollarColor;
    _handColor = handColor;
  }

  PayHand.fromJson(dynamic json) {
    _dollarColor = json['dollarColor'];
    _handColor = json['handColor'];
  }

  String? _dollarColor;
  String? _handColor;

  String? get dollarColor => _dollarColor;

  String? get handColor => _handColor;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['dollarColor'] = _dollarColor;
    map['handColor'] = _handColor;
    return map;
  }
}

class PayDownArrow {
  PayDownArrow({
    String? topColor,
    String? bottomColor,
  }) {
    _topColor = topColor;
    _bottomColor = bottomColor;
  }

  PayDownArrow.fromJson(dynamic json) {
    _topColor = json['topColor'];
    _bottomColor = json['bottomColor'];
  }

  String? _topColor;
  String? _bottomColor;

  String? get topColor => _topColor;

  String? get bottomColor => _bottomColor;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['topColor'] = _topColor;
    map['bottomColor'] = _bottomColor;
    return map;
  }
}

class DoubleArrow {
  DoubleArrow({
    String? arrowColor1,
    String? arrowColor2,
  }) {
    _arrowColor1 = arrowColor1;
    _arrowColor2 = arrowColor2;
  }

  DoubleArrow.fromJson(dynamic json) {
    _arrowColor1 = json['arrowColor1'];
    _arrowColor2 = json['arrowColor2'];
  }

  String? _arrowColor1;
  String? _arrowColor2;

  String? get arrowColor1 => _arrowColor1;

  String? get arrowColor2 => _arrowColor2;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['arrowColor1'] = _arrowColor1;
    map['arrowColor2'] = _arrowColor2;
    return map;
  }
}

class DoubleDollar {
  DoubleDollar({
    String? dollarColor1,
    String? dollarColor2,
  }) {
    _dollarColor1 = dollarColor1;
    _dollarColor2 = dollarColor2;
  }

  DoubleDollar.fromJson(dynamic json) {
    _dollarColor1 = json['dollarColor1'];
    _dollarColor2 = json['dollarColor2'];
  }

  String? _dollarColor1;
  String? _dollarColor2;

  String? get dollarColor1 => _dollarColor1;

  String? get dollarColor2 => _dollarColor2;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['dollarColor1'] = _dollarColor1;
    map['dollarColor2'] = _dollarColor2;
    return map;
  }
}

class ProgressBar {
  ProgressBar({
    DefaultProgressBar? defaultProgressBar,
  }) {
    _defaultProgressBar = defaultProgressBar;
  }

  ProgressBar.fromJson(dynamic json) {
    _defaultProgressBar = json['default_progressBar'] != null
        ? DefaultProgressBar.fromJson(json['defaultProgressBar'])
        : null;
  }

  DefaultProgressBar? _defaultProgressBar;

  DefaultProgressBar? get defaultProgressBar => _defaultProgressBar;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_defaultProgressBar != null) {
      map['default_progressBar'] = _defaultProgressBar?.toJson();
    }
    return map;
  }
}

class DefaultProgressBar {
  DefaultProgressBar({
    String? bgColor,
    int? bgColorOpacity,
    Border? border,
    int? borderRadius,
    int? height,
  }) {
    _bgColor = bgColor;
    _bgColorOpacity = bgColorOpacity;
    _border = border;
    _borderRadius = borderRadius;
    _height = height;
  }

  DefaultProgressBar.fromJson(dynamic json) {
    _bgColor = json['bgColor'];
    _bgColorOpacity = json['bgColorOpacity'];
    _border = json['border'] != null ? Border.fromJson(json['border']) : null;
    _borderRadius = json['borderRadius'];
    _height = json['height'];
  }

  String? _bgColor;
  int? _bgColorOpacity;
  Border? _border;
  int? _borderRadius;
  int? _height;

  String? get bgColor => _bgColor;

  int? get bgColorOpacity => _bgColorOpacity;

  Border? get border => _border;

  int? get borderRadius => _borderRadius;

  int? get height => _height;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['bgColor'] = _bgColor;
    map['bgColorOpacity'] = _bgColorOpacity;
    if (_border != null) {
      map['border'] = _border?.toJson();
    }
    map['borderRadius'] = _borderRadius;
    map['height'] = _height;
    return map;
  }
}

class PieChart {
  PieChart({
    DefaultPieChart? defaultPieChart,
  }) {
    _defaultPieChart = defaultPieChart;
  }

  PieChart.fromJson(dynamic json) {
    _defaultPieChart = json['default'] != null ? DefaultPieChart.fromJson(json['default']) : null;
  }

  DefaultPieChart? _defaultPieChart;

  DefaultPieChart? get defaultPieChart => _defaultPieChart;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_defaultPieChart != null) {
      map['default'] = _defaultPieChart?.toJson();
    }
    return map;
  }
}

class DefaultPieChart {
  DefaultPieChart({
    String? primaryColor,
    String? secondaryColor,
    int? spaceBtnRadius,
  }) {
    _primaryColor = primaryColor;
    _secondaryColor = secondaryColor;
    _spaceBtnRadius = spaceBtnRadius;
  }

  DefaultPieChart.fromJson(dynamic json) {
    _primaryColor = json['primaryColor'];
    _secondaryColor = json['secondaryColor'];
    _spaceBtnRadius = json['spaceBtnRadius'];
  }

  String? _primaryColor;
  String? _secondaryColor;
  int? _spaceBtnRadius;

  String? get primaryColor => _primaryColor;

  String? get secondaryColor => _secondaryColor;

  int? get spaceBtnRadius => _spaceBtnRadius;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['primaryColor'] = _primaryColor;
    map['secondaryColor'] = _secondaryColor;
    map['spaceBtnRadius'] = _spaceBtnRadius;
    return map;
  }
}

class Label {
  Label({
    L1? l1,
    L2? l2,
    L3? l3,
  }) {
    _l1 = l1;
    _l2 = l2;
    _l3 = l3;
  }

  Label.fromJson(dynamic json) {
    _l1 = json['l1'] != null ? L1.fromJson(json['l1']) : null;
    _l2 = json['l2'] != null ? L2.fromJson(json['l2']) : null;
    _l3 = json['l3'] != null ? L3.fromJson(json['l3']) : null;
  }

  L1? _l1;
  L2? _l2;
  L3? _l3;

  L1? get l1 => _l1;

  L2? get l2 => _l2;

  L3? get l3 => _l3;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_l1 != null) {
      map['l1'] = _l1?.toJson();
    }
    if (_l2 != null) {
      map['l2'] = _l2?.toJson();
    }
    if (_l3 != null) {
      map['l3'] = _l3?.toJson();
    }
    return map;
  }
}

class L3 {
  L3({
    String? fontFamily,
    String? color,
    int? fontSize,
    int? fontWeight,
    int? opacity,
  }) {
    _fontFamily = fontFamily;
    _color = color;
    _fontSize = fontSize;
    _fontWeight = fontWeight;
    _opacity = opacity;
  }

  L3.fromJson(dynamic json) {
    _fontFamily = json['fontFamily'];
    _color = json['color'];
    _fontSize = json['fontSize'];
    _fontWeight = json['fontWeight'];
    _opacity = json['opacity'];
  }

  String? _fontFamily;
  String? _color;
  int? _fontSize;
  int? _fontWeight;
  int? _opacity;

  String? get fontFamily => _fontFamily;

  String? get color => _color;

  int? get fontSize => _fontSize;

  int? get fontWeight => _fontWeight;

  int? get opacity => _opacity;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['fontFamily'] = _fontFamily;
    map['color'] = _color;
    map['fontSize'] = _fontSize;
    map['fontWeight'] = _fontWeight;
    map['opacity'] = _opacity;
    return map;
  }
}

class L2 {
  L2({
    String? fontFamily,
    String? color,
    int? fontSize,
    int? fontWeight,
    int? opacity,
  }) {
    _fontFamily = fontFamily;
    _color = color;
    _fontSize = fontSize;
    _fontWeight = fontWeight;
    _opacity = opacity;
  }

  L2.fromJson(dynamic json) {
    _fontFamily = json['fontFamily'];
    _color = json['color'];
    _fontSize = json['fontSize'];
    _fontWeight = json['fontWeight'];
    _opacity = json['opacity'];
  }

  String? _fontFamily;
  String? _color;
  int? _fontSize;
  int? _fontWeight;
  int? _opacity;

  String? get fontFamily => _fontFamily;

  String? get color => _color;

  int? get fontSize => _fontSize;

  int? get fontWeight => _fontWeight;

  int? get opacity => _opacity;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['fontFamily'] = _fontFamily;
    map['color'] = _color;
    map['fontSize'] = _fontSize;
    map['fontWeight'] = _fontWeight;
    map['opacity'] = _opacity;
    return map;
  }
}

class L1 {
  L1({
    String? fontFamily,
    String? color,
    int? fontSize,
    int? fontWeight,
    int? opacity,
  }) {
    _fontFamily = fontFamily;
    _color = color;
    _fontSize = fontSize;
    _fontWeight = fontWeight;
    _opacity = opacity;
  }

  L1.fromJson(dynamic json) {
    _fontFamily = json['fontFamily'];
    _color = json['color'];
    _fontSize = json['fontSize'];
    _fontWeight = json['fontWeight'];
    _opacity = json['opacity'];
  }

  String? _fontFamily;
  String? _color;
  int? _fontSize;
  int? _fontWeight;
  int? _opacity;

  String? get fontFamily => _fontFamily;

  String? get color => _color;

  int? get fontSize => _fontSize;

  int? get fontWeight => _fontWeight;

  int? get opacity => _opacity;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['fontFamily'] = _fontFamily;
    map['color'] = _color;
    map['fontSize'] = _fontSize;
    map['fontWeight'] = _fontWeight;
    map['opacity'] = _opacity;
    return map;
  }
}

class InputSlider {
  InputSlider({
    DefaultInputSlider? defaultInputSlider,
  }) {
    _defaultInputSlider = defaultInputSlider;
  }

  InputSlider.fromJson(dynamic json) {
    _defaultInputSlider = json['default_inputSlider'] != null
        ? DefaultInputSlider.fromJson(json['defaultInputSlider'])
        : null;
  }

  DefaultInputSlider? _defaultInputSlider;

  DefaultInputSlider? get defaultInputSlider => _defaultInputSlider;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_defaultInputSlider != null) {
      map['default_inputSlider'] = _defaultInputSlider?.toJson();
    }
    return map;
  }
}

class DefaultInputSlider {
  DefaultInputSlider({
    String? sliderColor,
  }) {
    _sliderColor = sliderColor;
  }

  DefaultInputSlider.fromJson(dynamic json) {
    _sliderColor = json['sliderColor'];
  }

  String? _sliderColor;

  String? get sliderColor => _sliderColor;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['sliderColor'] = _sliderColor;
    return map;
  }
}

class InputGroup {
  InputGroup({
    String? bgColor,
    int? bgColorOpacity,
    Border? border,
    BorderBottom? borderBottom,
    int? borderRadius,
  }) {
    _bgColor = bgColor;
    _bgColorOpacity = bgColorOpacity;
    _border = border;
    _borderBottom = borderBottom;
    _borderRadius = borderRadius;
  }

  InputGroup.fromJson(dynamic json) {
    _bgColor = json['bgColor'];
    _bgColorOpacity = json['bgColorOpacity'];
    _border = json['border'] != null ? Border.fromJson(json['border']) : null;
    _borderBottom =
    json['borderBottom'] != null ? BorderBottom.fromJson(json['borderBottom']) : null;
    _borderRadius = json['borderRadius'];
  }

  String? _bgColor;
  int? _bgColorOpacity;
  Border? _border;
  BorderBottom? _borderBottom;
  int? _borderRadius;

  String? get bgColor => _bgColor;

  int? get bgColorOpacity => _bgColorOpacity;

  Border? get border => _border;

  BorderBottom? get borderBottom => _borderBottom;

  int? get borderRadius => _borderRadius;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['bgColor'] = _bgColor;
    map['bgColorOpacity'] = _bgColorOpacity;
    if (_border != null) {
      map['border'] = _border?.toJson();
    }
    if (_borderBottom != null) {
      map['borderBottom'] = _borderBottom?.toJson();
    }
    map['borderRadius'] = _borderRadius;
    return map;
  }
}

class BorderBottom {
  BorderBottom({
    String? type,
    String? color,
    int? size,
    int? opacity,
  }) {
    _type = type;
    _color = color;
    _size = size;
    _opacity = opacity;
  }

  BorderBottom.fromJson(dynamic json) {
    _type = json['type'];
    _color = json['color'];
    _size = json['size'];
    _opacity = json['opacity'];
  }

  String? _type;
  String? _color;
  int? _size;
  int? _opacity;

  String? get type => _type;

  String? get color => _color;

  int? get size => _size;

  int? get opacity => _opacity;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['type'] = _type;
    map['color'] = _color;
    map['size'] = _size;
    map['opacity'] = _opacity;
    return map;
  }
}

class Icons {
  Icons({
    DefaultIcons? defaultIcons,
    Calendar? calendar,
    ArrowLeft? arrowLeft,
    ArrowLeftLarge? arrowLeftLarge,
    ArrowRight? arrowRight,
    IconInfo? info,
    Roundup? roundup,
    CheckCircle? checkCircle,
    Search? search,
  }) {
    _defaultIcons = defaultIcons;
    _calendar = calendar;
    _arrowLeft = arrowLeft;
    _arrowLeftLarge = arrowLeftLarge;
    _arrowRight = arrowRight;
    _info = info;
    _roundup = roundup;
    _checkCircle = checkCircle;
    _search = search;
  }

  Icons.fromJson(dynamic json) {
    _defaultIcons =
    json['default_icons'] != null ? DefaultIcons.fromJson(json['defaultIcons']) : null;
    _calendar = json['calendar'] != null ? Calendar.fromJson(json['calendar']) : null;
    _arrowLeft = json['arrowLeft'] != null ? ArrowLeft.fromJson(json['arrowLeft']) : null;
    _arrowLeftLarge =
    json['arrowLeftLarge'] != null ? ArrowLeftLarge.fromJson(json['arrowLeftLarge']) : null;
    _arrowRight = json['arrowRight'] != null ? ArrowRight.fromJson(json['arrowRight']) : null;
    _info = json['info'] != null ? IconInfo.fromJson(json['info']) : null;
    _roundup = json['roundup'] != null ? Roundup.fromJson(json['roundup']) : null;
    _checkCircle = json['checkCircle'] != null ? CheckCircle.fromJson(json['checkCircle']) : null;
    _search = json['search'] != null ? Search.fromJson(json['search']) : null;
  }

  DefaultIcons? _defaultIcons;
  Calendar? _calendar;
  ArrowLeft? _arrowLeft;
  ArrowLeftLarge? _arrowLeftLarge;
  ArrowRight? _arrowRight;
  IconInfo? _info;
  Roundup? _roundup;
  CheckCircle? _checkCircle;
  Search? _search;

  DefaultIcons? get defaultIcons => _defaultIcons;

  Calendar? get calendar => _calendar;

  ArrowLeft? get arrowLeft => _arrowLeft;

  ArrowLeftLarge? get arrowLeftLarge => _arrowLeftLarge;

  ArrowRight? get arrowRight => _arrowRight;

  IconInfo? get info => _info;

  Roundup? get roundup => _roundup;

  CheckCircle? get checkCircle => _checkCircle;

  Search? get search => _search;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_defaultIcons != null) {
      map['default_icons'] = _defaultIcons?.toJson();
    }
    if (_calendar != null) {
      map['calendar'] = _calendar?.toJson();
    }
    if (_arrowLeft != null) {
      map['arrowLeft'] = _arrowLeft?.toJson();
    }
    if (_arrowLeftLarge != null) {
      map['arrowLeftLarge'] = _arrowLeftLarge?.toJson();
    }
    if (_arrowRight != null) {
      map['arrowRight'] = _arrowRight?.toJson();
    }
    if (_info != null) {
      map['info'] = _info?.toJson();
    }
    if (_roundup != null) {
      map['roundup'] = _roundup?.toJson();
    }
    if (_checkCircle != null) {
      map['checkCircle'] = _checkCircle?.toJson();
    }
    if (_search != null) {
      map['search'] = _search?.toJson();
    }
    return map;
  }
}

class Search {
  Search({
    String? fill,
    String? icon,
    String? size,
  }) {
    _fill = fill;
    _icon = icon;
    _size = size;
  }

  Search.fromJson(dynamic json) {
    _fill = json['fill'];
    _icon = json['icon'];
    _size = json['size'];
  }

  String? _fill;
  String? _icon;
  String? _size;

  String? get fill => _fill;

  String? get icon => _icon;

  String? get size => _size;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['fill'] = _fill;
    map['icon'] = _icon;
    map['size'] = _size;
    return map;
  }
}

class CheckCircle {
  CheckCircle({
    String? fill,
    String? icon,
    String? size,
  }) {
    _fill = fill;
    _icon = icon;
    _size = size;
  }

  CheckCircle.fromJson(dynamic json) {
    _fill = json['fill'];
    _icon = json['icon'];
    _size = json['size'];
  }

  String? _fill;
  String? _icon;
  String? _size;

  String? get fill => _fill;

  String? get icon => _icon;

  String? get size => _size;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['fill'] = _fill;
    map['icon'] = _icon;
    map['size'] = _size;
    return map;
  }
}

class Roundup {
  Roundup({
    PlusCircle? plusCircle,
    MinusCircle? minusCircle,
  }) {
    _plusCircle = plusCircle;
    _minusCircle = minusCircle;
  }

  Roundup.fromJson(dynamic json) {
    _plusCircle = json['plusCircle'] != null ? PlusCircle.fromJson(json['plusCircle']) : null;
    _minusCircle = json['minusCircle'] != null ? MinusCircle.fromJson(json['minusCircle']) : null;
  }

  PlusCircle? _plusCircle;
  MinusCircle? _minusCircle;

  PlusCircle? get plusCircle => _plusCircle;

  MinusCircle? get minusCircle => _minusCircle;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_plusCircle != null) {
      map['plusCircle'] = _plusCircle?.toJson();
    }
    if (_minusCircle != null) {
      map['minusCircle'] = _minusCircle?.toJson();
    }
    return map;
  }
}

class MinusCircle {
  MinusCircle({
    String? fill,
    String? icon,
    String? size,
  }) {
    _fill = fill;
    _icon = icon;
    _size = size;
  }

  MinusCircle.fromJson(dynamic json) {
    _fill = json['fill'];
    _icon = json['icon'];
    _size = json['size'];
  }

  String? _fill;
  String? _icon;
  String? _size;

  String? get fill => _fill;

  String? get icon => _icon;

  String? get size => _size;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['fill'] = _fill;
    map['icon'] = _icon;
    map['size'] = _size;
    return map;
  }
}

class PlusCircle {
  PlusCircle({
    String? fill,
    String? icon,
    String? size,
  }) {
    _fill = fill;
    _icon = icon;
    _size = size;
  }

  PlusCircle.fromJson(dynamic json) {
    _fill = json['fill'];
    _icon = json['icon'];
    _size = json['size'];
  }

  String? _fill;
  String? _icon;
  String? _size;

  String? get fill => _fill;

  String? get icon => _icon;

  String? get size => _size;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['fill'] = _fill;
    map['icon'] = _icon;
    map['size'] = _size;
    return map;
  }
}

class IconInfo {
  IconInfo({
    String? fill,
    String? icon,
    String? size,
  }) {
    _fill = fill;
    _icon = icon;
    _size = size;
  }

  IconInfo.fromJson(dynamic json) {
    _fill = json['fill'];
    _icon = json['icon'];
    _size = json['size'];
  }

  String? _fill;
  String? _icon;
  String? _size;

  String? get fill => _fill;

  String? get icon => _icon;

  String? get size => _size;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['fill'] = _fill;
    map['icon'] = _icon;
    map['size'] = _size;
    return map;
  }
}

class ArrowRight {
  ArrowRight({
    String? fill,
    String? icon,
    String? size,
  }) {
    _fill = fill;
    _icon = icon;
    _size = size;
  }

  ArrowRight.fromJson(dynamic json) {
    _fill = json['fill'];
    _icon = json['icon'];
    _size = json['size'];
  }

  String? _fill;
  String? _icon;
  String? _size;

  String? get fill => _fill;

  String? get icon => _icon;

  String? get size => _size;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['fill'] = _fill;
    map['icon'] = _icon;
    map['size'] = _size;
    return map;
  }
}

class ArrowLeftLarge {
  ArrowLeftLarge({
    String? fill,
    String? icon,
    String? size,
  }) {
    _fill = fill;
    _icon = icon;
    _size = size;
  }

  ArrowLeftLarge.fromJson(dynamic json) {
    _fill = json['fill'];
    _icon = json['icon'];
    _size = json['size'];
  }

  String? _fill;
  String? _icon;
  String? _size;

  String? get fill => _fill;

  String? get icon => _icon;

  String? get size => _size;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['fill'] = _fill;
    map['icon'] = _icon;
    map['size'] = _size;
    return map;
  }
}

class ArrowLeft {
  ArrowLeft({
    String? fill,
    String? icon,
    String? size,
  }) {
    _fill = fill;
    _icon = icon;
    _size = size;
  }

  ArrowLeft.fromJson(dynamic json) {
    _fill = json['fill'];
    _icon = json['icon'];
    _size = json['size'];
  }

  String? _fill;
  String? _icon;
  String? _size;

  String? get fill => _fill;

  String? get icon => _icon;

  String? get size => _size;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['fill'] = _fill;
    map['icon'] = _icon;
    map['size'] = _size;
    return map;
  }
}

class Calendar {
  Calendar({
    String? color,
    String? icon,
    String? size,
  }) {
    _color = color;
    _icon = icon;
    _size = size;
  }

  Calendar.fromJson(dynamic json) {
    _color = json['color'] ?? json['fill'];
    _icon = json['icon'];
    _size = json['size'];
  }

  String? _color;
  String? _icon;
  String? _size;

  String? get color => _color;

  String? get icon => _icon;

  String? get size => _size;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['color'] = _color;
    map['fill'] = _color;
    map['icon'] = _icon;
    map['size'] = _size;
    return map;
  }
}

class DefaultIcons {
  DefaultIcons({
    String? fill,
    String? icon,
    String? size,
  }) {
    _fill = fill;
    _icon = icon;
    _size = size;
  }

  DefaultIcons.fromJson(dynamic json) {
    _fill = json['fill'];
    _icon = json['icon'];
    _size = json['size'];
  }

  String? _fill;
  String? _icon;
  String? _size;

  String? get fill => _fill;

  String? get icon => _icon;

  String? get size => _size;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['fill'] = _fill;
    map['icon'] = _icon;
    map['size'] = _size;
    return map;
  }
}

class Header {
  Header({
    H1? h1,
    H2? h2,
    H3? h3,
  }) {
    _h1 = h1;
    _h2 = h2;
    _h3 = h3;
  }

  Header.fromJson(dynamic json) {
    _h1 = json['h1'] != null ? H1.fromJson(json['h1']) : null;
    _h2 = json['h2'] != null ? H2.fromJson(json['h2']) : null;
    _h3 = json['h3'] != null ? H3.fromJson(json['h3']) : null;
  }

  H1? _h1;
  H2? _h2;
  H3? _h3;

  H1? get h1 => _h1;

  H2? get h2 => _h2;

  H3? get h3 => _h3;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_h1 != null) {
      map['h1'] = _h1?.toJson();
    }
    if (_h2 != null) {
      map['h2'] = _h2?.toJson();
    }
    if (_h3 != null) {
      map['h3'] = _h3?.toJson();
    }
    return map;
  }
}

class H3 {
  H3({
    String? fontFamily,
    String? color,
    int? fontSize,
    int? fontWeight,
    int? opacity,
  }) {
    _fontFamily = fontFamily;
    _color = color;
    _fontSize = fontSize;
    _fontWeight = fontWeight;
    _opacity = opacity;
  }

  H3.fromJson(dynamic json) {
    _fontFamily = json['fontFamily'];
    _color = json['color'];
    _fontSize = json['fontSize'];
    _fontWeight = json['fontWeight'];
    _opacity = json['opacity'];
  }

  String? _fontFamily;
  String? _color;
  int? _fontSize;
  int? _fontWeight;
  int? _opacity;

  String? get fontFamily => _fontFamily;

  String? get color => _color;

  int? get fontSize => _fontSize;

  int? get fontWeight => _fontWeight;

  int? get opacity => _opacity;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['fontFamily'] = _fontFamily;
    map['color'] = _color;
    map['fontSize'] = _fontSize;
    map['fontWeight'] = _fontWeight;
    map['opacity'] = _opacity;
    return map;
  }
}

class H2 {
  H2({
    String? fontFamily,
    String? color,
    int? fontSize,
    int? fontWeight,
    int? opacity,
  }) {
    _fontFamily = fontFamily;
    _color = color;
    _fontSize = fontSize;
    _fontWeight = fontWeight;
    _opacity = opacity;
  }

  H2.fromJson(dynamic json) {
    _fontFamily = json['fontFamily'];
    _color = json['color'];
    _fontSize = json['fontSize'];
    _fontWeight = json['fontWeight'];
    _opacity = json['opacity'];
  }

  String? _fontFamily;
  String? _color;
  int? _fontSize;
  int? _fontWeight;
  int? _opacity;

  String? get fontFamily => _fontFamily;

  String? get color => _color;

  int? get fontSize => _fontSize;

  int? get fontWeight => _fontWeight;

  int? get opacity => _opacity;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['fontFamily'] = _fontFamily;
    map['color'] = _color;
    map['fontSize'] = _fontSize;
    map['fontWeight'] = _fontWeight;
    map['opacity'] = _opacity;
    return map;
  }
}

class H1 {
  H1({
    String? fontFamily,
    String? color,
    int? fontSize,
    int? fontWeight,
    int? opacity,
  }) {
    _fontFamily = fontFamily;
    _color = color;
    _fontSize = fontSize;
    _fontWeight = fontWeight;
    _opacity = opacity;
  }

  H1.fromJson(dynamic json) {
    _fontFamily = json['fontFamily'];
    _color = json['color'];
    _fontSize = json['fontSize'];
    _fontWeight = json['fontWeight'];
    _opacity = json['opacity'];
  }

  String? _fontFamily;
  String? _color;
  int? _fontSize;
  int? _fontWeight;
  int? _opacity;

  String? get fontFamily => _fontFamily;

  String? get color => _color;

  int? get fontSize => _fontSize;

  int? get fontWeight => _fontWeight;

  int? get opacity => _opacity;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['fontFamily'] = _fontFamily;
    map['color'] = _color;
    map['fontSize'] = _fontSize;
    map['fontWeight'] = _fontWeight;
    map['opacity'] = _opacity;
    return map;
  }
}

class FormControl {
  FormControl({
    Border? border,
    int? borderRadius,
    int? fontSize,
    int? fontWeight,
    String? color,
    int? colorOpacity,
  }) {
    _border = border;
    _borderRadius = borderRadius;
    _fontSize = fontSize;
    _fontWeight = fontWeight;
    _color = color;
    _colorOpacity = colorOpacity;
  }

  FormControl.fromJson(dynamic json) {
    _border = json['border'] != null ? Border.fromJson(json['border']) : null;
    _borderRadius = json['borderRadius'];
    _fontSize = json['fontSize'];
    _fontWeight = json['fontWeight'];
    _color = json['color'];
    _colorOpacity = json['colorOpacity'];
  }

  Border? _border;
  int? _borderRadius;
  int? _fontSize;
  int? _fontWeight;
  String? _color;
  int? _colorOpacity;

  Border? get border => _border;

  int? get borderRadius => _borderRadius;

  int? get fontSize => _fontSize;

  int? get fontWeight => _fontWeight;

  String? get color => _color;

  int? get colorOpacity => _colorOpacity;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_border != null) {
      map['border'] = _border?.toJson();
    }
    map['borderRadius'] = _borderRadius;
    map['fontSize'] = _fontSize;
    map['fontWeight'] = _fontWeight;
    map['color'] = _color;
    map['colorOpacity'] = _colorOpacity;
    return map;
  }
}

class DotLoader {
  DotLoader({
    String? loaderColor,
  }) {
    _loaderColor = loaderColor;
  }

  DotLoader.fromJson(dynamic json) {
    _loaderColor = json['loaderColor'];
  }

  String? _loaderColor;

  String? get loaderColor => _loaderColor;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['loaderColor'] = _loaderColor;
    return map;
  }
}

class Divider {
  Divider({
    DefaultDivider? defaultDivider,
  }) {
    _defaultDivider = defaultDivider;
  }

  Divider.fromJson(dynamic json) {
    _defaultDivider =
    json['default_divider'] != null ? DefaultDivider.fromJson(json['defaultDivider']) : null;
  }

  DefaultDivider? _defaultDivider;

  DefaultDivider? get defaultDivider => _defaultDivider;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_defaultDivider != null) {
      map['default_divider'] = _defaultDivider?.toJson();
    }
    return map;
  }
}

class DefaultDivider {
  DefaultDivider({
    int? borderRadius,
    Border? border,
    String? bgColor,
    int? bgColorOpacity,
    int? height,
  }) {
    _borderRadius = borderRadius;
    _border = border;
    _bgColor = bgColor;
    _bgColorOpacity = bgColorOpacity;
    _height = height;
  }

  DefaultDivider.fromJson(dynamic json) {
    _borderRadius = json['borderRadius'];
    _border = json['border'] != null ? Border.fromJson(json['border']) : null;
    _bgColor = json['bgColor'];
    _bgColorOpacity = json['bgColorOpacity'];
    _height = json['height'];
  }

  int? _borderRadius;
  Border? _border;
  String? _bgColor;
  int? _bgColorOpacity;
  int? _height;

  int? get borderRadius => _borderRadius;

  Border? get border => _border;

  String? get bgColor => _bgColor;

  int? get bgColorOpacity => _bgColorOpacity;

  int? get height => _height;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['borderRadius'] = _borderRadius;
    if (_border != null) {
      map['border'] = _border?.toJson();
    }
    map['bgColor'] = _bgColor;
    map['bgColorOpacity'] = _bgColorOpacity;
    map['height'] = _height;
    return map;
  }
}

class Card {
  Card({
    DefaultCard? defaultCard,
  }) {
    _defaultCard = defaultCard;
  }

  Card.fromJson(dynamic json) {
    _defaultCard = json['default'] != null ? DefaultCard.fromJson(json['default']) : null;
  }

  DefaultCard? _defaultCard;

  DefaultCard get defaultCard => _defaultCard ?? DefaultCard();

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_defaultCard != null) {
      map['default_card'] = _defaultCard?.toJson();
    }
    return map;
  }
}

class DefaultCard {
  DefaultCard({
    String? bgColor,
    int? bgColorOpacity,
    Border? border,
    int? borderRadius,
  }) {
    _bgColor = bgColor;
    _bgColorOpacity = bgColorOpacity;
    _border = border;
    _borderRadius = borderRadius;
  }

  DefaultCard.fromJson(dynamic json) {
    _bgColor = json['bgColor'];
    _bgColorOpacity = json['bgColorOpacity'];
    _border = json['border'] != null ? Border.fromJson(json['border']) : null;
    _borderRadius = json['borderRadius'];
  }

  String? _bgColor;
  int? _bgColorOpacity;
  Border? _border;
  int? _borderRadius;

  String? get bgColor => _bgColor;

  double get bgColorOpacity => (_bgColorOpacity ?? 100) / 100;

  Border? get border => _border;

  int? get borderRadius => _borderRadius;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['bgColor'] = _bgColor;
    map['bgColorOpacity'] = _bgColorOpacity;
    if (_border != null) {
      map['border'] = _border?.toJson();
    }
    map['borderRadius'] = _borderRadius;
    return map;
  }
}

class Button {
  Button({
    DefaultButton? defaultButton,
    Outlined? outlined,
    Navigation? navigation,
  }) {
    _defaultButton = defaultButton;
    _outlined = outlined;
    _navigation = navigation;
  }

  Button.fromJson(dynamic json) {
    _defaultButton =
    json['default_button'] != null ? DefaultButton.fromJson(json['defaultButton']) : null;
    _outlined = json['outlined'] != null ? Outlined.fromJson(json['outlined']) : null;
    _navigation = json['navigation'] != null ? Navigation.fromJson(json['navigation']) : null;
  }

  DefaultButton? _defaultButton;
  Outlined? _outlined;
  Navigation? _navigation;

  DefaultButton? get defaultButton => _defaultButton;

  Outlined? get outlined => _outlined;

  Navigation? get navigation => _navigation;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_defaultButton != null) {
      map['default_button'] = _defaultButton?.toJson();
    }
    if (_outlined != null) {
      map['outlined'] = _outlined?.toJson();
    }
    if (_navigation != null) {
      map['navigation'] = _navigation?.toJson();
    }
    return map;
  }
}

class Navigation {
  Navigation({
    String? bgColor,
    int? bgColorOpacity,
    String? hoverBgColor,
    int? hoverBgColorOpacity,
    Title? title,
    SubTitle? subTitle,
    Icon? icon,
  }) {
    _bgColor = bgColor;
    _bgColorOpacity = bgColorOpacity;
    _hoverBgColor = hoverBgColor;
    _hoverBgColorOpacity = hoverBgColorOpacity;
    _title = title;
    _subTitle = subTitle;
    _icon = icon;
  }

  Navigation.fromJson(dynamic json) {
    _bgColor = json['bgColor'];
    _bgColorOpacity = json['bgColorOpacity'];
    _hoverBgColor = json['hoverBgColor'];
    _hoverBgColorOpacity = json['hoverBgColorOpacity'];
    _title = json['title'] != null ? Title.fromJson(json['title']) : null;
    _subTitle = json['subTitle'] != null ? SubTitle.fromJson(json['subTitle']) : null;
    _icon = json['icon'] != null ? Icon.fromJson(json['icon']) : null;
  }

  String? _bgColor;
  int? _bgColorOpacity;
  String? _hoverBgColor;
  int? _hoverBgColorOpacity;
  Title? _title;
  SubTitle? _subTitle;
  Icon? _icon;

  String? get bgColor => _bgColor;

  int? get bgColorOpacity => _bgColorOpacity;

  String? get hoverBgColor => _hoverBgColor;

  int? get hoverBgColorOpacity => _hoverBgColorOpacity;

  Title? get title => _title;

  SubTitle? get subTitle => _subTitle;

  Icon? get icon => _icon;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['bgColor'] = _bgColor;
    map['bgColorOpacity'] = _bgColorOpacity;
    map['hoverBgColor'] = _hoverBgColor;
    map['hoverBgColorOpacity'] = _hoverBgColorOpacity;
    if (_title != null) {
      map['title'] = _title?.toJson();
    }
    if (_subTitle != null) {
      map['subTitle'] = _subTitle?.toJson();
    }
    if (_icon != null) {
      map['icon'] = _icon?.toJson();
    }
    return map;
  }
}

class Icon {
  Icon({
    String? icon,
    String? size,
    String? fill,
  }) {
    _icon = icon;
    _size = size;
    _fill = fill;
  }

  Icon.fromJson(dynamic json) {
    _icon = json['icon'];
    _size = json['size'];
    _fill = json['fill'];
  }

  String? _icon;
  String? _size;
  String? _fill;

  String? get icon => _icon;

  String? get size => _size;

  String? get fill => _fill;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['icon'] = _icon;
    map['size'] = _size;
    map['fill'] = _fill;
    return map;
  }
}

class Outlined {
  Outlined({
    String? bgColor,
    int? bgColorOpacity,
    String? hoverBgColor,
    int? hoverBgColorOpacity,
    Border? border,
    int? borderRadius,
    Title? title,
  }) {
    _bgColor = bgColor;
    _bgColorOpacity = bgColorOpacity;
    _hoverBgColor = hoverBgColor;
    _hoverBgColorOpacity = hoverBgColorOpacity;
    _border = border;
    _borderRadius = borderRadius;
    _title = title;
  }

  Outlined.fromJson(dynamic json) {
    _bgColor = json['bgColor'];
    _bgColorOpacity = json['bgColorOpacity'];
    _hoverBgColor = json['hoverBgColor'];
    _hoverBgColorOpacity = json['hoverBgColorOpacity'];
    _border = json['border'] != null ? Border.fromJson(json['border']) : null;
    _borderRadius = json['borderRadius'];
    _title = json['title'] != null ? Title.fromJson(json['title']) : null;
  }

  String? _bgColor;
  int? _bgColorOpacity;
  String? _hoverBgColor;
  int? _hoverBgColorOpacity;
  Border? _border;
  int? _borderRadius;
  Title? _title;

  String? get bgColor => _bgColor;

  int? get bgColorOpacity => _bgColorOpacity;

  String? get hoverBgColor => _hoverBgColor;

  int? get hoverBgColorOpacity => _hoverBgColorOpacity;

  Border? get border => _border;

  int? get borderRadius => _borderRadius;

  Title? get title => _title;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['bgColor'] = _bgColor;
    map['bgColorOpacity'] = _bgColorOpacity;
    map['hoverBgColor'] = _hoverBgColor;
    map['hoverBgColorOpacity'] = _hoverBgColorOpacity;
    if (_border != null) {
      map['border'] = _border?.toJson();
    }
    map['borderRadius'] = _borderRadius;
    if (_title != null) {
      map['title'] = _title?.toJson();
    }
    return map;
  }
}

class DefaultButton {
  DefaultButton({
    String? bgColor,
    int? bgColorOpacity,
    String? hoverBgColor,
    int? hoverBgColorOpacity,
    Border? border,
    int? borderRadius,
    Title? title,
  }) {
    _bgColor = bgColor;
    _bgColorOpacity = bgColorOpacity;
    _hoverBgColor = hoverBgColor;
    _hoverBgColorOpacity = hoverBgColorOpacity;
    _border = border;
    _borderRadius = borderRadius;
    _title = title;
  }

  DefaultButton.fromJson(dynamic json) {
    _bgColor = json['bgColor'];
    _bgColorOpacity = json['bgColorOpacity'];
    _hoverBgColor = json['hoverBgColor'];
    _hoverBgColorOpacity = json['hoverBgColorOpacity'];
    _border = json['border'] != null ? Border.fromJson(json['border']) : null;
    _borderRadius = json['borderRadius'];
    _title = json['title'] != null ? Title.fromJson(json['title']) : null;
  }

  String? _bgColor;
  int? _bgColorOpacity;
  String? _hoverBgColor;
  int? _hoverBgColorOpacity;
  Border? _border;
  int? _borderRadius;
  Title? _title;

  String? get bgColor => _bgColor;

  int? get bgColorOpacity => _bgColorOpacity;

  String? get hoverBgColor => _hoverBgColor;

  int? get hoverBgColorOpacity => _hoverBgColorOpacity;

  Border? get border => _border;

  int? get borderRadius => _borderRadius;

  Title? get title => _title;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['bgColor'] = _bgColor;
    map['bgColorOpacity'] = _bgColorOpacity;
    map['hoverBgColor'] = _hoverBgColor;
    map['hoverBgColorOpacity'] = _hoverBgColorOpacity;
    if (_border != null) {
      map['border'] = _border?.toJson();
    }
    map['borderRadius'] = _borderRadius;
    if (_title != null) {
      map['title'] = _title?.toJson();
    }
    return map;
  }
}

class Box {
  Box({
    DefaultBox? defaultBox,
    IconInfo? info,
    Color? color,
  }) {
    _defaultBox = defaultBox;
    _info = info;
    _color = color;
  }

  Box.fromJson(dynamic json) {
    _defaultBox = json['default_box'] != null ? DefaultBox.fromJson(json['defaultBox']) : null;
    _info = json['info'] != null ? IconInfo.fromJson(json['info']) : null;
    _color = json['color'] != null ? Color.fromJson(json['color']) : null;
  }

  DefaultBox? _defaultBox;
  IconInfo? _info;
  Color? _color;

  DefaultBox? get defaultBox => _defaultBox;

  IconInfo? get info => _info;

  Color? get color => _color;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_defaultBox != null) {
      map['default_box'] = _defaultBox?.toJson();
    }
    if (_info != null) {
      map['info'] = _info?.toJson();
    }
    if (_color != null) {
      map['color'] = _color?.toJson();
    }
    return map;
  }
}

class Color {
  Color({
    String? bgColor,
    int? bgColorOpacity,
    Border? border,
    int? borderRadius,
    int? padding,
  }) {
    _bgColor = bgColor;
    _bgColorOpacity = bgColorOpacity;
    _border = border;
    _borderRadius = borderRadius;
    _padding = padding;
  }

  Color.fromJson(dynamic json) {
    _bgColor = json['bgColor'];
    _bgColorOpacity = json['bgColorOpacity'];
    _border = json['border'] != null ? Border.fromJson(json['border']) : null;
    _borderRadius = json['borderRadius'];
    _padding = json['padding'];
  }

  String? _bgColor;
  int? _bgColorOpacity;
  Border? _border;
  int? _borderRadius;
  int? _padding;

  String? get bgColor => _bgColor;

  int? get bgColorOpacity => _bgColorOpacity;

  Border? get border => _border;

  int? get borderRadius => _borderRadius;

  int? get padding => _padding;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['bgColor'] = _bgColor;
    map['bgColorOpacity'] = _bgColorOpacity;
    if (_border != null) {
      map['border'] = _border?.toJson();
    }
    map['borderRadius'] = _borderRadius;
    map['padding'] = _padding;
    return map;
  }
}

class Info {
  Info({
    String? bgColor,
    int? bgColorOpacity,
    Border? border,
    int? borderRadius,
    int? padding,
  }) {
    _bgColor = bgColor;
    _bgColorOpacity = bgColorOpacity;
    _border = border;
    _borderRadius = borderRadius;
    _padding = padding;
  }

  Info.fromJson(dynamic json) {
    _bgColor = json['bgColor'];
    _bgColorOpacity = json['bgColorOpacity'];
    _border = json['border'] != null ? Border.fromJson(json['border']) : null;
    _borderRadius = json['borderRadius'];
    _padding = json['padding'];
  }

  String? _bgColor;
  int? _bgColorOpacity;
  Border? _border;
  int? _borderRadius;
  int? _padding;

  String? get bgColor => _bgColor;

  int? get bgColorOpacity => _bgColorOpacity;

  Border? get border => _border;

  int? get borderRadius => _borderRadius;

  int? get padding => _padding;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['bgColor'] = _bgColor;
    map['bgColorOpacity'] = _bgColorOpacity;
    if (_border != null) {
      map['border'] = _border?.toJson();
    }
    map['borderRadius'] = _borderRadius;
    map['padding'] = _padding;
    return map;
  }
}

class DefaultBox {
  DefaultBox({
    String? bgColor,
    int? bgColorOpacity,
    Border? border,
    int? borderRadius,
    int? padding,
  }) {
    _bgColor = bgColor;
    _bgColorOpacity = bgColorOpacity;
    _border = border;
    _borderRadius = borderRadius;
    _padding = padding;
  }

  DefaultBox.fromJson(dynamic json) {
    _bgColor = json['bgColor'];
    _bgColorOpacity = json['bgColorOpacity'];
    _border = json['border'] != null ? Border.fromJson(json['border']) : null;
    _borderRadius = json['borderRadius'];
    _padding = json['padding'];
  }

  String? _bgColor;
  int? _bgColorOpacity;
  Border? _border;
  int? _borderRadius;
  int? _padding;

  String? get bgColor => _bgColor;

  int? get bgColorOpacity => _bgColorOpacity;

  Border? get border => _border;

  int? get borderRadius => _borderRadius;

  int? get padding => _padding;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['bgColor'] = _bgColor;
    map['bgColorOpacity'] = _bgColorOpacity;
    if (_border != null) {
      map['border'] = _border?.toJson();
    }
    map['borderRadius'] = _borderRadius;
    map['padding'] = _padding;
    return map;
  }
}

class Anchor {
  Anchor({
    DefaultAnchor? defaultAnchor,
  }) {
    _defaultAnchor = defaultAnchor;
  }

  Anchor.fromJson(dynamic json) {
    _defaultAnchor =
    json['default_anchor'] != null ? DefaultAnchor.fromJson(json['defaultAnchor']) : null;
  }

  DefaultAnchor? _defaultAnchor;

  DefaultAnchor? get defaultAnchor => _defaultAnchor;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_defaultAnchor != null) {
      map['default_anchor'] = _defaultAnchor?.toJson();
    }
    return map;
  }
}

class DefaultAnchor {
  DefaultAnchor({
    String? fontFamily,
    String? color,
    int? fontSize,
    int? fontWeight,
  }) {
    _fontFamily = fontFamily;
    _color = color;
    _fontSize = fontSize;
    _fontWeight = fontWeight;
  }

  DefaultAnchor.fromJson(dynamic json) {
    _fontFamily = json['fontFamily'];
    _color = json['color'];
    _fontSize = json['fontSize'];
    _fontWeight = json['fontWeight'];
  }

  String? _fontFamily;
  String? _color;
  int? _fontSize;
  int? _fontWeight;

  String? get fontFamily => _fontFamily;

  String? get color => _color;

  int? get fontSize => _fontSize;

  int? get fontWeight => _fontWeight;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['fontFamily'] = _fontFamily;
    map['color'] = _color;
    map['fontSize'] = _fontSize;
    map['fontWeight'] = _fontWeight;
    return map;
  }
}
