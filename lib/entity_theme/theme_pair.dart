import 'package:sw_core/tool/asset.dart';

class ThemePair {
  ThemePair(this.lightTheme, this.darkTheme);

  Map<String, dynamic> lightTheme;
  Map<String, dynamic> darkTheme;

  static ThemePair fromJson(Map<String, dynamic> lightTheme, Map<String, dynamic> darkTheme) =>
      ThemePair(lightTheme, darkTheme);

  static Future<ThemePair> fromAsset([String? lightThemeAsset, String? darkThemeAsset]) async =>
      ThemePair(
        await loadJson(lightThemeAsset),
        await loadJson(darkThemeAsset),
      );
}
