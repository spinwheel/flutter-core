export 'package:sw_core/navigation/transition/ext_spin_transition.dart';

enum SpinTransition {
  fade,
  rightToLeft,
  leftToRight,
  topToBottom,
  bottomToTop,
  rightToLeftWithFade,
  leftToRightWithFade,
  rightToLeftJoined,
  leftToRightJoined,
  scale,
  rotate,
  cupertino,
  material,
  fluent,
  size,
  native,
}

SpinTransition getTransition(SpinTransition? transition) {
  switch (transition) {
    case SpinTransition.cupertino:
      return SpinTransition.cupertino;
    case SpinTransition.material:
      return SpinTransition.material;
    case SpinTransition.fluent:
      return SpinTransition.fluent;
    case SpinTransition.fade:
      return SpinTransition.fade;
    case SpinTransition.rightToLeft:
      return SpinTransition.rightToLeft;
    case SpinTransition.leftToRight:
      return SpinTransition.leftToRight;
    case SpinTransition.topToBottom:
      return SpinTransition.topToBottom;
    case SpinTransition.bottomToTop:
      return SpinTransition.bottomToTop;
    case SpinTransition.rightToLeftWithFade:
      return SpinTransition.rightToLeftWithFade;
    case SpinTransition.leftToRightWithFade:
      return SpinTransition.leftToRightWithFade;
    case SpinTransition.rightToLeftJoined:
      return SpinTransition.rightToLeftJoined;
    case SpinTransition.leftToRightJoined:
      return SpinTransition.leftToRightJoined;
    case SpinTransition.scale:
      return SpinTransition.scale;
    case SpinTransition.rotate:
      return SpinTransition.rotate;
    case SpinTransition.size:
      return SpinTransition.size;
    case SpinTransition.native:
    case null:
    default:
      return SpinTransition.native;
  }
}
