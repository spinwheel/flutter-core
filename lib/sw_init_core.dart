import 'package:dio/dio.dart';
import 'package:dio_cache_interceptor/dio_cache_interceptor.dart';
import 'package:sw_core/analytics/analytics.dart';
import 'package:sw_core/tool/http_client.dart';
import 'package:sw_core/tool/inject.dart';
import 'package:sw_core/tool/log.dart';

initCore() async {
  Analytics();
  put<InterceptorsWrapper>(httpInterceptors);
  put<DioCacheInterceptor>(httpCache);
  put<Dio>(
    () => httpClient(
      [
        get<InterceptorsWrapper>(),
        get<DioCacheInterceptor>(),
      ],
    ),
  );
}
