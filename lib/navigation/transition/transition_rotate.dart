import 'package:flutter/material.dart';

class TransitionRotate<T> extends PageRouteBuilder<T> {
  TransitionRotate(
    Widget child, {
    RouteSettings? settings,
  }) : super(
          pageBuilder: (
            BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
          ) =>
              RotationTransition(
            turns: animation,
            child: ScaleTransition(
              scale: animation,
              child: FadeTransition(
                opacity: animation,
                child: child,
              ),
            ),
          ),
          settings: settings,
        );
}
