import 'package:flutter/material.dart';

class TransitionFluent<T> extends PageRouteBuilder<T> {
  TransitionFluent(
    Widget child, {
    RouteSettings? settings,
  }) : super(
          pageBuilder: (
            context,
            animation,
            secondaryAnimation,
          ) =>
              FadeTransition(
            opacity: animation,
            child: ScaleTransition(
              child: child,
              scale: Tween<double>(begin: 0.88, end: 1.0).animate(animation),
            ),
          ),
          settings: settings,
        );
}
