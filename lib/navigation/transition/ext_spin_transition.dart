import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sw_core/entity_theme/enum/transition.dart';
import 'package:sw_core/navigation/transition/transition_bottom_to_top.dart';
import 'package:sw_core/navigation/transition/transition_cupertino.dart';
import 'package:sw_core/navigation/transition/transition_fade.dart';
import 'package:sw_core/navigation/transition/transition_fluent.dart';
import 'package:sw_core/navigation/transition/transition_left_to_right.dart';
import 'package:sw_core/navigation/transition/transition_left_to_right_joined.dart';
import 'package:sw_core/navigation/transition/transition_left_to_right_w_fade.dart';
import 'package:sw_core/navigation/transition/transition_material.dart';
import 'package:sw_core/navigation/transition/transition_right_to_left.dart';
import 'package:sw_core/navigation/transition/transition_right_to_left_joined.dart';
import 'package:sw_core/navigation/transition/transition_right_to_left_w_fade.dart';
import 'package:sw_core/navigation/transition/transition_rotate.dart';
import 'package:sw_core/navigation/transition/transition_scale.dart';
import 'package:sw_core/navigation/transition/transition_size.dart';
import 'package:sw_core/navigation/transition/transition_top_to_bottom.dart';

import '../../tool/os_environment.dart';

extension ExtSpinTransition on SpinTransition {
  static PageRouteBuilder get(SpinTransition transition, Widget child, {RouteSettings? settings}) {
    switch (transition) {
      case SpinTransition.fade:
        return TransitionFade(child, settings: settings);
      case SpinTransition.rightToLeft:
        return TransitionRightToLeft(child, settings: settings);
      case SpinTransition.leftToRight:
        return TransitionLeftToRight(child, settings: settings);
      case SpinTransition.topToBottom:
        return TransitionTopToBottom(child, settings: settings);
      case SpinTransition.bottomToTop:
        return TransitionBottomToTop(child, settings: settings);
      case SpinTransition.rightToLeftWithFade:
        return TransitionRightToLeftWithFade(child, settings: settings);
      case SpinTransition.leftToRightWithFade:
        return TransitionLeftToRightWithFade(child, settings: settings);
      case SpinTransition.rightToLeftJoined:
        return TransitionRightToLeftJoined(child, child, settings: settings);
      case SpinTransition.leftToRightJoined:
        return TransitionLeftToRightJoined(child, child, settings: settings);
      case SpinTransition.scale:
        return TransitionScale(child, settings: settings);
      case SpinTransition.rotate:
        return TransitionRotate(child, settings: settings);
      case SpinTransition.cupertino:
        return TransitionCupertino(child, settings: settings);
      case SpinTransition.material:
        return TransitionMaterial(child, settings: settings);
      case SpinTransition.fluent:
        return TransitionFluent(child, settings: settings);
      case SpinTransition.size:
        return TransitionSize(child, settings: settings);
      case SpinTransition.native:
      default:
        return _caseNative(child, settings: settings);
    }
  }
}

PageRouteBuilder _caseNative(
  Widget child, {
  RouteSettings? settings,
}) {
  switch (ExtOS.currentOS()) {
    case OS.ios:
      return TransitionCupertino(child, settings: settings);
    case OS.windows:
      return TransitionFluent(child, settings: settings);
    case OS.macos:
      return TransitionCupertino(child, settings: settings);
    case OS.linux:
    case OS.fuchsia:
    case OS.android:
    default:
      return TransitionMaterial(child, settings: settings);
  }
}
