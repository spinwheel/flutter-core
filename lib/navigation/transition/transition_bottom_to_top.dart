import 'package:flutter/material.dart';

class TransitionBottomToTop<T> extends PageRouteBuilder<T> {
  TransitionBottomToTop(
    Widget child, {
    RouteSettings? settings,
  }) : super(
          pageBuilder: (
            BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
          ) =>
              SlideTransition(
            position: Tween<Offset>(
              begin: const Offset(0, 1),
              end: Offset.zero,
            ).animate(animation),
            child: child,
          ),
          settings: settings,
        );
}
