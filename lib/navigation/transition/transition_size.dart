import 'package:flutter/material.dart';

class TransitionSize<T> extends PageRouteBuilder<T> {
  TransitionSize(
    Widget child, {
    RouteSettings? settings,
  }) : super(
            pageBuilder: (
              BuildContext context,
              Animation<double> animation,
              Animation<double> secondaryAnimation,
            ) =>
                SizeTransition(
                  sizeFactor: CurvedAnimation(
                    parent: animation,
                    curve: Curves.linear,
                  ),
                  child: child,
                ),
            settings: settings);
}
