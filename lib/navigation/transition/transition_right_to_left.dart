import 'package:flutter/material.dart';

class TransitionRightToLeft<T> extends PageRouteBuilder<T> {
  TransitionRightToLeft(
    Widget child, {
    RouteSettings? settings,
  }) : super(
          pageBuilder: (
            BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
          ) =>
              SlideTransition(
            position: Tween<Offset>(
              begin: const Offset(1, 0),
              end: Offset.zero,
            ).animate(animation),
            child: child,
          ),
          settings: settings,
        );
}
