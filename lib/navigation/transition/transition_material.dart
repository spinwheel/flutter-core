import 'package:flutter/material.dart';

class TransitionMaterial<T> extends PageRouteBuilder<T> {
  TransitionMaterial(
    Widget child, {
    RouteSettings? settings,
  }) : super(
          pageBuilder: (
            context,
            animation,
            secondaryAnimation,
          ) =>
              SlideTransition(
            position: animation.drive(_bottomUpTween.chain(_fastOutSlowInTween)),
            child: FadeTransition(
              opacity: animation.drive(_easeInTween),
              child: child,
            ),
          ),
          settings: settings,
        );

  static final Tween<Offset> _bottomUpTween = Tween<Offset>(
    begin: const Offset(0.0, 0.25),
    end: Offset.zero,
  );
  static final Animatable<double> _fastOutSlowInTween = CurveTween(curve: Curves.fastOutSlowIn);
  static final Animatable<double> _easeInTween = CurveTween(curve: Curves.easeIn);
}
