import 'package:flutter/cupertino.dart';

class TransitionCupertino<T> extends PageRouteBuilder<T> {
  TransitionCupertino(
    Widget child, {
    RouteSettings? settings,
  }) : super(
          pageBuilder: (
            BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
          ) =>
              CupertinoPageTransition(
            primaryRouteAnimation: animation,
            secondaryRouteAnimation: secondaryAnimation,
            child: child,
            linearTransition: true,
          ),
          settings: settings,
        );
}
