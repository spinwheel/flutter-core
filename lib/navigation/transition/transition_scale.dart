import 'package:flutter/material.dart';

class TransitionScale<T> extends PageRouteBuilder<T> {
  TransitionScale(
    Widget child, {
    RouteSettings? settings,
  }) : super(
          pageBuilder: (
            BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
          ) =>
              ScaleTransition(
            scale: CurvedAnimation(
              parent: animation,
              curve: Interval(
                0.00,
                0.50,
                curve: Curves.linear,
              ),
            ),
            child: child,
          ),
          settings: settings,
        );
}
