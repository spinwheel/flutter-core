import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/route_manager.dart';
import 'package:sw_core/entity_theme/enum/transition.dart';
import 'package:sw_core/entity_theme/params_loan_connect.dart';
import 'package:sw_core/tool/inject.dart';
import 'package:url_launcher/url_launcher.dart';

import '../tool/log.dart';

void navigate(BuildContext? context, Widget page, [Object? args]) {
  if (context != null) {
    Navigator.of(context, rootNavigator: true).push(
      ExtSpinTransition.get(
        getSafe<ParamsLoanConnect>()?.pageTransition ?? SpinTransition.native,
        page,
        settings: RouteSettings(arguments: args),
      ),
    );
  }
}

void back(BuildContext? context) {
  if (context != null) {
    final nav = Navigator.of(context);
    nav.canPop() ? nav.pop(context) : Navigator.of(context, rootNavigator: true).pop();
  }
}

dynamic get args => Get.arguments;

launchURL(String url, [Function? onError]) async {
  info('Trying to launch url: $url');
  final encoded = Uri.encodeFull(url);
  final launchAble = await canLaunch(encoded);
  // launchAble ? await launch(encoded) : onError?.call(); // https://pub.dev/packages/url_launcher#android
  await launch(encoded);
  return launchAble;
}
