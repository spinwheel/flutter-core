@echo off

echo ________________________________      DELETE PUBSPEC.LOCK CORE
call del "pubspec.lock"
echo ________________________________      FLUTTER CLEAN CORE
call flutter clean
echo ________________________________      FLUTTER PUB UPGRADE CORE
call flutter pub upgrade
echo ________________________________      FLUTTER PUB GET CORE
call flutter pub get